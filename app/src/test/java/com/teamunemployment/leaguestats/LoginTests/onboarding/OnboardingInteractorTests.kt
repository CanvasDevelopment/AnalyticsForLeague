package com.teamunemployment.leaguestats.LoginTests.onboarding

import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import com.teamunemployment.leaguestats.io.service_layer.ServiceProducer
import com.teamunemployment.leaguestats.login_page.onboarding.OnboardingInteractor
import com.teamunemployment.leaguestats.login_page.onboarding.OnboardingPresenter
import com.teamunemployment.leaguestats.login_page.onboarding.OnboardingService
import org.junit.Test
import org.mockito.Mockito.*
import retrofit2.Call

/**
 * Created by Josiah Kendall
 */
class OnboardingInteractorTests {

    private val service = mock(OnboardingService::class.java)
    private val retrofitFactory = mock(RetrofitFactory::class.java)
    private val network = mock(Network::class.java)
    private val serviceProducer = mock(ServiceProducer::class.java)
    val interactor = OnboardingInteractor(retrofitFactory, network,serviceProducer)
    val presenter = mock(OnboardingPresenter::class.java)

    @Test
    fun `Make sure that we set true when returning 200`() {
        val call =mock(Call::class.java)

//        `when`(service.syncUserMatchList("-1")).thenReturn(call as Call<Result<String>>?)
//        interactor.requestUserRegistration(presenter)
//        verify(presenter, times(1)).handleSyncResult(false)
    }

    @Test
    fun `Make sure that we set false when we dont return 200`() {

    }
}