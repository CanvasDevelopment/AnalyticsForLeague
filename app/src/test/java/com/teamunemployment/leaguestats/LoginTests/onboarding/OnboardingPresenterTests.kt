package com.teamunemployment.leaguestats.LoginTests.onboarding

import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import com.teamunemployment.leaguestats.io.service_layer.ServiceProducer
import com.teamunemployment.leaguestats.login_page.onboarding.*
import com.teamunemployment.leaguestats.login_page.onboarding.model.SyncProgress
import org.junit.Test
import org.mockito.Mockito.*

/**
 * Created by Josiah Kendall
 */
class OnboardingPresenterTests {

    private val retrofitFactory = mock(RetrofitFactory::class.java)
    private val network = mock(Network::class.java)
    private val serviceProducer = mock(ServiceProducer::class.java)
    private val presenter = OnboardingPresenter(OnboardingInteractor(retrofitFactory,network,serviceProducer))
    private val view = mock(OnboardingContract.View::class.java)
    private val messages = OnboardingMessages()

    @Test
    fun `Make sure that we show the sync error message to the user if we fail the sync`() {
        presenter.setView(view)
        presenter.handleSyncResult(false, "")
        verify(view, times(1)).showMessage(messages.syncFailure())
    }

    @Test
    fun `Make sure the we launch the home screen on successfull sync`() {
        presenter.setView(view)
        presenter.handleSyncResult(true, "")

        // Todo rework this piece of code its messy and difficult to test
//        verify(view, times(1)).launchHomeScreen()
    }

    @Test
    fun `Make sure that we set the total matches when setting sync progress update`() {
        presenter.setView(view)
        val syncProgress = SyncProgress(121, 9)
        presenter.handleSyncProgressUpdate(syncProgress, "")
        verify(view, times(1)).setTotalMatches(syncProgress.total)
    }

    @Test
    fun `Make sure that we set the completed matches when setting sync progress update`() {
        presenter.setView(view)
        val syncProgress = SyncProgress(121, 9)
        presenter.handleSyncProgressUpdate(syncProgress, "")
        verify(view, times(1)).setSyncedMatches(syncProgress.completed)
    }

    @Test
    fun `Make sure that we return true when the completed matches are the same as the total matches`() {
        presenter.setView(view)
        val syncProgress = SyncProgress(121, 121)
        val result = presenter.handleSyncProgressUpdate(syncProgress, "")
        assert(result)
    }

    @Test
    fun `Make sure that we return false when the completed matches are not the same as the total matches`() {
        presenter.setView(view)
        val syncProgress = SyncProgress(121, 9)
        val result = presenter.handleSyncProgressUpdate(syncProgress, "")
        assert(!result)

    }

    @Test
    fun `Make sure that we update the loading text when we get a match update`() {
        presenter.setView(view)
        val syncProgress = SyncProgress(121, 9)
        val result = presenter.handleSyncProgressUpdate(syncProgress, "")
        verify(view, times(1)).setProgressTextMessage("Downloading matches from Riot servers - 9/121")

    }

    @Test
    fun `Make sure that set processing text when we reach the last one`() {
        presenter.setView(view)
        val syncProgress = SyncProgress(121, 121)
        val result = presenter.handleSyncProgressUpdate(syncProgress,"")
        verify(view, times(1)).setProgressTextMessage( "Downloading matches from Riot servers - 121/121")

    }
}