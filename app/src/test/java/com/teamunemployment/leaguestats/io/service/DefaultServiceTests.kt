package com.teamunemployment.leaguestats.io.service

import com.google.firebase.analytics.FirebaseAnalytics
import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.io.service_layer.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class DefaultServiceTests {

    val servicePresenter = mock(ServicePresenter::class.java)
    val serviceInteractor = mock(ServiceInteractor::class.java)
    private val serviceProducer = mock(ServiceProducer::class.java)
    lateinit var defaultServiceString : DefaultService<String>

    @Before
    fun preTestSetup() {
    }

    @Test
    fun `Ensure that we do not set the data to the presenter if it is the same as the cached data`() {
        val test : String = "12345"
        val test2 = "12345"
        val cachedData = Result(200, test)
        val serverData = Result(20, test2)

        `when`(serviceProducer.produceService<String>()).thenReturn(DefaultService(serviceInteractor, mock(FirebaseAnalytics::class.java)))
        defaultServiceString = serviceProducer.produceService<String>()


        defaultServiceString.setTestCallback {
            servicePresenter.handleDataResponse(it)
        }

        defaultServiceString.handleCacheResponse(cachedData)
        defaultServiceString.handleServerResponse(serverData)
        verify(servicePresenter, times(1)).handleDataResponse(cachedData)
        verify(servicePresenter, times(0)).handleDataResponse(serverData)
    }

    @Test
    fun `Ensure that we do set the data to the presenter if the data is different`() {
        val test : String = "12345"
        val test2 = "123456"
        val cachedData = Result(200, test)
        val serverData = Result(20, test2)
        `when`(serviceProducer.produceService<String>()).thenReturn(DefaultService(serviceInteractor, mock(FirebaseAnalytics::class.java)))
        defaultServiceString = serviceProducer.produceService<String>()

        defaultServiceString.setTestCallback {
            servicePresenter.handleDataResponse(it)
        }

        defaultServiceString.handleCacheResponse(cachedData)
        defaultServiceString.handleCacheResponse(serverData)
        verify(servicePresenter, times(1)).handleDataResponse(cachedData)
        verify(servicePresenter, times(1)).handleDataResponse(serverData)
    }

    @Test
    fun `Ensure that we do not set the data to the presenter if it is the same as the cached data (COMPLEX DATA)`() {
        val test  = HeadToHeadStat(12.0f, 12.0f)
        val test2 = HeadToHeadStat(12.0f, 12.0f)
        val cachedData = Result(200, test)
        val serverData = Result(201, test2)

        `when`(serviceProducer.produceService<HeadToHeadStat>()).thenReturn(DefaultService(serviceInteractor, mock(FirebaseAnalytics::class.java)))

        val defaultServiceComplex = serviceProducer.produceService<HeadToHeadStat>()

        defaultServiceComplex.setTestCallback {
            servicePresenter.handleDataResponse(it)
        }

        defaultServiceComplex.handleCacheResponse(cachedData)
        defaultServiceComplex.handleServerResponse(serverData)
        verify(servicePresenter, times(1)).handleDataResponse(cachedData)
        verify(servicePresenter, times(0)).handleDataResponse(serverData)
    }

    @Test
    fun `Ensure that we do set the data to the presenter if the data is different (COMPLEX DATA)`() {
        val test  = HeadToHeadStat(12.0f, 12.0f)
        val test2 = HeadToHeadStat(12.1f, 12.0f)
        val cachedData = Result(200, test)
        val serverData = Result(201, test2)

        `when`(serviceProducer.produceService<HeadToHeadStat>()).thenReturn(DefaultService(serviceInteractor, mock(FirebaseAnalytics::class.java)))

        val defaultServiceComplex = serviceProducer.produceService<HeadToHeadStat>()

        defaultServiceComplex.setTestCallback {
            servicePresenter.handleDataResponse(it)
        }

        defaultServiceComplex.handleCacheResponse(cachedData)
        defaultServiceComplex.handleServerResponse(serverData)
        verify(servicePresenter, times(1)).handleDataResponse(cachedData)
        verify(servicePresenter, times(1)).handleDataResponse(serverData)
    }

    @Test
    fun `Ensure that we show error if both server and cache are null`() {

    }

   @Test
   fun `Ensure that we dispatch when we have not found anything in the cache`() {
       val test  = HeadToHeadStat(12.0f, 12.0f)
       val test2 = HeadToHeadStat(12.1f, 12.0f)
       val cachedData = Result(200, test)
       val serverData = Result(201, test2)

       `when`(serviceProducer.produceService<HeadToHeadStat>()).thenReturn(DefaultService(serviceInteractor, mock(FirebaseAnalytics::class.java)))

       val defaultServiceComplex = serviceProducer.produceService<HeadToHeadStat>()

       defaultServiceComplex.setTestCallback {
           servicePresenter.handleDataResponse(it)
       }

       defaultServiceComplex.handleServerResponse(serverData)
       verify(servicePresenter, times(1)).handleDataResponse(serverData)
       verify(servicePresenter, times(0)).handleDataResponse(cachedData)
   }

    @Test
    fun `Make sure that we cache the result if the code is 200`() {
        val test  = HeadToHeadStat(12.0f, 12.0f)
        val test2 = HeadToHeadStat(12.1f, 12.0f)
        val cachedData = Result(200, test)
        val serverData = Result(200, test2)

        `when`(serviceProducer.produceService<HeadToHeadStat>()).thenReturn(DefaultService(serviceInteractor, mock(FirebaseAnalytics::class.java)))

        val defaultServiceComplex = serviceProducer.produceService<HeadToHeadStat>()

        defaultServiceComplex.setTestCallback {
            servicePresenter.handleDataResponse(it)
        }

        val url = "test"
        defaultServiceComplex.setTestUrl(url)

        defaultServiceComplex.handleCacheResponse(cachedData)
        defaultServiceComplex.handleServerResponse(serverData)
        verify(servicePresenter, times(1)).handleDataResponse(cachedData)
        verify(servicePresenter, times(1)).handleDataResponse(serverData)
        verify(serviceInteractor, times(1)).cacheData(serverData, url)
    }


}
