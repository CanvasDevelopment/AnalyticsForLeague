package com.teamunemployment.leaguestats.io.service

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.io.service_layer.ServiceInteractor
import com.teamunemployment.leaguestats.io.service_layer.cache.CacheDao
import com.teamunemployment.leaguestats.io.service_layer.cache.Request
import org.junit.Test
import org.mockito.Mockito.mock

class ServiceInteractorTests {

    val gson = Gson()

    @Test
    fun `Test that we can load from database using generic type`() {
        val stat = HeadToHeadStat(12.0f, 12.0f)
        val statString = gson.toJson(stat)
        val request = Request("1", statString)
//        val defaultService = DefaultService<ArrayList<HeadToHeadStat>>(mock(ServiceInteractor::class.java), mock(ServicePresenter::class.java))
        val interactor = ServiceInteractor(mock(CacheDao::class.java))
        val generic = interactor.createGenericObject<HeadToHeadStat>(request, HeadToHeadStat::class.java)
        assert(generic == stat)
    }

    @Test
    fun `Test that we can load from database using a more complex generic type`() {
        val stats = ArrayList<HeadToHeadStat>()
        stats.add(HeadToHeadStat(12.0f, 12.0f))
        val statString = gson.toJson(stats)
        val request = Request("1", statString)
//        val defaultService = DefaultService<HeadToHeadStat>(mock(ServiceInteractor::class.java), mock(ServicePresenter::class.java))
        val interactor = ServiceInteractor(mock(CacheDao::class.java))
        val generic = interactor.createGenericObject<ArrayList<HeadToHeadStat>>(request, object: TypeToken<ArrayList<HeadToHeadStat>>() {}.type)
        assert(generic == stats)
    }
}