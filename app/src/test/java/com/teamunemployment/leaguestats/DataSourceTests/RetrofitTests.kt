package com.teamunemployment.leaguestats.DataSourceTests

import com.teamunemployment.leaguestats.utils.getMatchHistoryCardData
import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.MatchHistoryService
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchHistoryCardData
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import com.teamunemployment.leaguestats.mock.MockHttpResponseInterceptor
import com.teamunemployment.leaguestats.mock.MockMatchHistoryServiceResponses
import org.junit.Test

import retrofit2.Response

/**
 * @author Josiah Kendall
 */

class RetrofitTests {

    private val mockMatchHistoryServiceResponses = MockMatchHistoryServiceResponses()

    @Test
    fun `Make sure that we can convert the mock match list id array`() {
        // getMatchIds array string
        val stringResponse = mockMatchHistoryServiceResponses.getMatchList()
        // make intercepter
        val interceptor = MockHttpResponseInterceptor(stringResponse, 200)
        // build retrofit mock
        val retrofitMock = RetrofitFactory().produceMockRetrofitInterface(MatchHistoryService::class.java, interceptor)
        // getMatchIds response
        val call = retrofitMock.fetchMatches(20, 23,"2")

        // todo
//        val response : Response<Result<ArrayList<MatchIdentifier>>> =  call.execute()
//
//        val result : Result<ArrayList<MatchIdentifier>> = response.getMatchIds()
//        assert(result.data.size == 41)
    }

    @Test
    fun `Make sure that we can convert the mock match summary to the correct object`() {
        // getMatchIds array string
        val stringResponse = mockMatchHistoryServiceResponses.getMatchSummary(1)
        // make intercepter
        val interceptor = MockHttpResponseInterceptor(stringResponse, 200)
        // build retrofit mock
        val retrofitMock = RetrofitFactory().produceMockRetrofitInterface(MatchHistoryService::class.java, interceptor)
        // getMatchIds response
        val call = retrofitMock.fetchMatchSummary(1,20, "23")

        val response : Response<Result<MatchHistoryCardData>> =  call.execute()

        val result : Result<MatchHistoryCardData> = response.getMatchHistoryCardData()
        assert(result.data.champId == 1.toLong())
    }
}
