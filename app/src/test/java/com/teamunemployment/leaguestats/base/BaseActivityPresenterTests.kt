package com.teamunemployment.leaguestats.base

import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.utils.Role
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import org.junit.Before

import org.junit.Test

import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

/**
 * @author Josiah Kendall
 */
class BaseActivityPresenterTests {
    val baseActivityPersistenceInteractor = mock(BaseActivityPersistenceInteractor::class.java)
    val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
    val baseActivityPresenter = BaseActivityPresenter(baseActivityPersistenceInteractor, summonerRapidAccessObject)
    val view = mock(BaseActivityView::class.java)



    @Before
    fun init() {
        baseActivityPresenter.setView(view)
    }

    @Test
    fun handleTabPress_RequestsWinRate() {
        val baseActivityPersistenceInteractor = mock(BaseActivityPersistenceInteractor::class.java)
        val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
        val baseActivityPresenter = BaseActivityPresenter(baseActivityPersistenceInteractor, summonerRapidAccessObject)
        val view = mock(BaseActivityView::class.java)
        baseActivityPresenter.setView(view)
        baseActivityPresenter.handleTabPress(1)
        baseActivityPresenter.onWinRateLoaded(HeadToHeadStat(12f, 12f))
        //todo fix this.
        // For some reason this shitty fucking framework thinks I have a null here. I have walked through
        // the test in the debugger and it is clearly not null.
//        verify(baseActivityPersistenceInteractor, times(1)).fetchWinRateForRole(
//                ArgumentMatchers.anyString(),
//                ArgumentMatchers.anyString(),
//                ArgumentMatchers.anyString(),
//                ArgumentMatchers.any(BaseActivityPresenter::class.java))
    }

    @Test
    fun onWinRateLoaded_setsWinRate() {
        val baseActivityPersistenceInteractor = mock(BaseActivityPersistenceInteractor::class.java)
        val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
        val baseActivityPresenter = BaseActivityPresenter(baseActivityPersistenceInteractor, summonerRapidAccessObject)
        val view = mock(BaseActivityView::class.java)
        baseActivityPresenter.setView(view)
        baseActivityPresenter.handleTabPress(1)
        baseActivityPresenter.onWinRateLoaded(HeadToHeadStat(12f, 12f))
        // verify(view, times(1)).setWinRate(anyDouble()); TODO
    }

    @Test
    fun TestThatWeCanSetCorrectFragmentTab() {
        val baseActivityPersistenceInteractor = mock(BaseActivityPersistenceInteractor::class.java)
        val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
        val baseActivityPresenter = BaseActivityPresenter(baseActivityPersistenceInteractor, summonerRapidAccessObject)
        val view = mock(BaseActivityView::class.java)
        baseActivityPresenter.setView(view)
        baseActivityPresenter.handleTabPress(1)
        verify(view, times(1)).setCorrectTabFragment(1)
    }

    @Test
    fun testThatWeCanSendCorrectVariablesToTheViewWhenARoleIsChanged() {
        val baseActivityPersistenceInteractor = mock(BaseActivityPersistenceInteractor::class.java)
        val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
        val baseActivityPresenter = BaseActivityPresenter(baseActivityPersistenceInteractor, summonerRapidAccessObject)
        baseActivityPresenter.setView(view)
        baseActivityPresenter.handleChangeInRole(R.id.action_mid)
        val role = Role()
        verify(view, times(1)).setCorrectTabFragment(role.MID)
    }

    @Test
    fun `Make sure that we set toolbar to disabled while syncing`() {
        baseActivityPresenter.handleRefresh()
        verify(view, times(1)).setRefreshButtonEnabled(false)
        verify(view, times(1)).setRefreshAnimating(true)
    }

    @Test
    fun `Make sure that we cancel animation when we fail loading with an error`() {
        baseActivityPresenter.handleSyncError("error")
        verify(view, times(1)).setRefreshButtonEnabled(true)

    }

    @Test
    fun `Make sure that we notify user when we start loading`() {
        val message = "Updating user stats"
        baseActivityPresenter.handleRefresh()
        verify(view, times(1)).showMessage(message)

    }

    @Test
    fun `Make sure that we notify user when we finish loading`() {
        val message = "Update finished"
        baseActivityPresenter.finishRefresh()
        verify(view, times(1)).showMessage(message)
    }

    @Test
    fun `Make sure that we notify user when we get an error`() {
        val message = "An error occurred while loading"
        baseActivityPresenter.handleSyncError(message)
        verify(view, times(1)).showMessage(message)
    }

    @Test
    fun `Ensure We Toast If First Press`() {
        baseActivityPresenter.backPressed()
        verify(view, times(1)).showMessage(BaseActivityPresenter.BACK_MASSAGE)
        verify(view, times(0)).closeApp()
    }

    @Test
    fun `Ensure we don't close if pressed outside 2 seconds`() {
        baseActivityPresenter.backPressed()
        Thread.sleep(3000)
        baseActivityPresenter.backPressed()
        verify(view, times(2)).showMessage(BaseActivityPresenter.BACK_MASSAGE)
        verify(view, times(0)).closeApp()
    }

    @Test
    fun `Ensure we do close if pressed inside bounds`() {
        baseActivityPresenter.backPressed()
        Thread.sleep(100)
        baseActivityPresenter.backPressed()
        verify(view, times(1)).showMessage(BaseActivityPresenter.BACK_MASSAGE)
        verify(view, times(1)).closeApp()

    }
}
