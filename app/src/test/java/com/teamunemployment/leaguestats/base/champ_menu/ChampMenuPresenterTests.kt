package com.teamunemployment.leaguestats.base.champ_menu

import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.data.model.Champ
import junit.framework.Assert

import org.junit.Before
import org.junit.Test

import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import java.util.*

/**
 * Created by Josiah Kendall
 */

class ChampMenuPresenterTests {

    lateinit var champMenuInteractor: ChampMenuInteractor
    lateinit var champMenuPresenter: ChampMenuPresenter
    lateinit var champMenuView: ChampMenuContract.View
    private val id = 121
    private val games = 21
    val summonerId = 21L
    val random = Random()
    val champ = Champ(random.nextInt().toString(),
            id.toString(),
            id.toString(),
            random.toString(),
            random.nextInt().toString(),
            random.nextInt().toString(),
            games,
            "summonerId")

    private fun createChamp() :Champ{
        return Champ(random.nextInt().toString(),
                id.toString(),
                id.toString(),
                random.toString(),
                random.nextInt().toString(),
                random.nextInt().toString(),
                games,
                summonerId.toString())
    }

    private fun createChamp(title : String) :Champ{
        return Champ(random.nextInt().toString(),
                id.toString(),
                id.toString(),
                random.toString(),
                title,
                random.nextInt().toString(),
                games,
                "summonerId")
    }

    @Before
    fun doSetUp() {
        champMenuInteractor = mock(ChampMenuInteractor::class.java)
        champMenuPresenter = ChampMenuPresenter(champMenuInteractor,mock(Network::class.java))
        champMenuView = mock(ChampMenuContract.View::class.java)
        champMenuPresenter.setSearchView(champMenuView)



        `when`<Champ>(champMenuInteractor.getCurrentSetChamp()).thenReturn(champ)
    }

    @Test
    fun testThatWeCanFilterChamps() {
        val champs = ArrayList<Champ>()
        // THe first champ is the offset champ
        champs.add(createChamp(""))
        champs.add(createChamp("vi"))
        champs.add(createChamp("hercules"))
        champs.add(createChamp("hero_guy"))
        champs.add(createChamp("dogmatix"))

        champMenuPresenter.setChampData(champs)

        Assert.assertTrue(champMenuPresenter.champs.size == 5)
        champMenuPresenter.filterChamps("her")
        // We get 3, not two, as we put in an offset champ.
        Assert.assertTrue(champMenuPresenter.champs.size == 3)
    }

    @Test
    fun testThatWeCanFilterChampsWithCaps() {
        val champs = ArrayList<Champ>()
        champs.add(createChamp("Shyvana"))
        champs.add(createChamp("Vi"))
        champs.add(createChamp("Hercules"))
        champs.add(createChamp("Hero_guy"))
        champs.add(createChamp("Dogmatix"))
        champMenuPresenter.setChampData(champs)

        Assert.assertTrue(champMenuPresenter.champs.size == 5)
        champMenuPresenter.filterChamps("her")
        // We get 3, not two, as we put in an offset champ.
        Assert.assertTrue(champMenuPresenter.champs.size == 3)
    }

    @Test
    fun `Ensure that we can clear filter`() {
        val champs = ArrayList<Champ>()
        // THe first champ is the offset champ
        champs.add(createChamp(""))
        champs.add(createChamp("vi"))
        champs.add(createChamp("hercules"))
        champs.add(createChamp("hero_guy"))
        champs.add(createChamp("dogmatix"))
        champMenuPresenter.setChampData(champs)

        Assert.assertTrue(champMenuPresenter.champs.size == 5)
        champMenuPresenter.filterChamps("her")
        // We get 3, not two, as we put in an offset champ.
        Assert.assertTrue(champMenuPresenter.champs.size == 3)
        champMenuPresenter.clearFilter()
        Assert.assertTrue(champMenuPresenter.champs.size == 5)
    }


    @Test
    fun EnsureWeCanDisplayOverlayWhenChangeFilteredChampButtonIsClicked() {

        champMenuPresenter.handleSearchFabClick()
        verify<ChampMenuContract.View>(
                champMenuView,
                times(1)).showOverlay()
    }

    @Test
    fun EnsureThatWeHideOverlayWhenCloseIsClicked() {
        `when`<Champ>(champMenuInteractor.getCurrentSetChamp()).thenReturn(champ)
        champMenuPresenter.handleSearchFabClick()
        verify<ChampMenuContract.View>(champMenuView, times(1)).showOverlay()
        champMenuPresenter.handleSearchFabClick()
        verify<ChampMenuContract.View>(champMenuView, times(1)).hideOverlay()
    }

    @Test
    fun EnsureThatWeSetCurrentChampIconWhenWeClose() {

        champMenuPresenter.handleSearchFabClick()
        champMenuPresenter.handleSearchFabClick()

        verify(champMenuView, times(1)).setChampFabIconAsSelectedChamp("null/champion/${champ.title}.png")
    }

    @Test
    fun EnsureWeSetSearchButtonToXButton() {

        champMenuPresenter.handleSearchFabClick()
        verify(champMenuView, times(1)).setChampFabIconAsACross()
    }

    @Test
    fun EnsureThatWeShowTheListOfChampions() {

        champMenuPresenter.handleSearchFabClick()

        verify(champMenuView, times(1)).showChampList()
    }

    @Test
    fun EnsureThatWeCloseTheChampList() {
        champMenuPresenter.handleSearchFabClick()
        champMenuPresenter.handleSearchFabClick()
        verify(champMenuView, times(1)).hideChampList()
    }

    @Test
    fun EnsureThatWeShowTheSearchBar() {
        champMenuPresenter.handleSearchFabClick()
        verify(champMenuView, times(1)).showSearchBar()
    }

    @Test
    fun EnsureThatWeCloseTheSearchBarOnSecondClick() {
        champMenuPresenter.handleSearchFabClick()
        champMenuPresenter.handleSearchFabClick()
        verify(champMenuView, times(1)).hideSearchBar()
    }



    @Test
    fun EnsureThatWeSetFavouriteChampsWhenWeFirstLoad() {
        champMenuPresenter.handleSearchFabClick()
        val champs = ArrayList<Champ>()
        val response =  mock(ChampLoadResponse::class.java)
        verify(champMenuInteractor, times(1)).getChamps(champMenuPresenter)
        champMenuPresenter.setChampRequestResponse(champs)
        verify(champMenuView, times(1)).setChampList(champs)
    }

    @Test
    fun EnsureThatWeSetMostApplicableChampsWhenWeSearch() {

    }

    @Test
    fun EnsureThatWeCloseOverlayWhenWeSetAChamp() {
        champMenuPresenter.handleChampClick(champ)
        verify(champMenuView, times(1)).hideOverlay()
    }

    @Test
    fun EnsureThatWeSetCorrectImageWhenWeSelectAChamp() {
        val champUrl = "/images/vi.jpg"
        champMenuPresenter.handleChampClick(champ)
        verify(champMenuView, times(1)).setChampFabIconAsSelectedChamp("null/champion/${champ.title}.png")
    }

    @Test
    fun EnsureThatWeCloseTheKeyboard() {
        champMenuPresenter.handleSearchFabClick()
        champMenuPresenter.handleSearchFabClick()
        verify(champMenuView, times(1)).ensureKeyboardIsHidden()
    }

    @Test
    fun EnsureThatWeClearChampSettingWhenWeSelectClearChampInAdapter() {
        champMenuPresenter.handleChampClick(champ)
        verify(champMenuInteractor, times(1)).setCurrentChamp(champ)
    }

    @Test
    fun EnsureThatWeClearTextWhenWeClickOnTheClearTextButtonForTheSearchBox() {
        champMenuPresenter.clearSearchText()
        verify(champMenuView, times(1)).clearSearchField()
    }

    @Test
    fun EnsureThatWeShow_ClearSearchInputButton_WhenWeAddText() {
        champMenuPresenter.searchForChamp("sfff")
        verify(champMenuView, times(1)).showClearSearchTextButton()
    }

    @Test
    fun EnsureThatWeHide_ClearSearchInputButton_WhenWeHaveNoTextLeft() {
        champMenuPresenter.searchForChamp("")
        verify(champMenuView, times(1)).hideClearSearchTextButton()
    }

    @Test
    fun `Ensure that we trigger champ update when we change champ`() {

        champMenuPresenter.handleChampClick(champ)
        verify(champMenuView, times(1)).refresh()
    }
}

