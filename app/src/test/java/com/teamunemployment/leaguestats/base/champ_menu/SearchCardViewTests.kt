package com.teamunemployment.leaguestats.base.champ_menu

import com.teamunemployment.leaguestats.data.model.Champ
import com.teamunemployment.leaguestats.data.model.OldChamp
import com.teamunemployment.leaguestats.base.champ_menu.Model.ChampSearchCardView
import com.teamunemployment.leaguestats.base.champ_menu.Model.OffSetViewHolder

import org.junit.Test
import org.mockito.Mockito.*

import java.util.*

/**
 * Created by Josiah Kendall
 */

class SearchCardViewTests {

    private val id = 121
    private val games = 21
    val summonerId = "21"
    val random = Random()
    val champ = createChamp()

    private fun createChamp() :Champ{
        return Champ(random.nextInt().toString(),
                id.toString(),
                id.toString(),
                random.toString(),
                random.nextInt().toString(),
                random.nextInt().toString(),
                games,
                summonerId)
    }

    private fun createChamp(title : String) :Champ{
        return Champ(random.nextInt().toString(),
                id.toString(),
                id.toString(),
                random.toString(),
                title,
                random.nextInt().toString(),
                games,
                "summonerId")
    }

    @Test
    fun EnsureThatWeSetImage() {
        val champUrl = "fhdskfgdlsk"
        val champMenuPresenter = mock(ChampMenuPresenter::class.java)
        val champMenuListAdapter = ChampMenuListAdapter(champMenuPresenter)
        val champsData = ArrayList<Champ>()
        champsData.add(champ)
        champsData.add(champ)
        champMenuListAdapter.setData(champsData)
        `when`(champMenuPresenter.champs).thenReturn(champsData)
        val searchCardView = mock(ChampSearchCardView::class.java)
        champMenuListAdapter.onBindViewHolder(searchCardView, 1)
        verify(searchCardView, times(1)).setChamp(champ)
    }

    @Test(expected = IndexOutOfBoundsException::class)
    fun EnsureThatWeThrowExceptionIfWeLoadWithoutSettingData() {
        val champUrl = "fhdskfgdlsk"
        val champMenuPresenter = mock(ChampMenuPresenter::class.java)
        val champMenuListAdapter = ChampMenuListAdapter(champMenuPresenter)
        val arrayList = ArrayList<OldChamp>()
        val searchCardView = mock(ChampSearchCardView::class.java)
        champMenuListAdapter.onBindViewHolder(searchCardView, 0)
    }

    @Test // Fails because layout inflater is not mocked
    fun EnsureWeOffSetChampSearchScrollSlightly() {
        //        ChampMenuPresenter champMenuPresenter = mock(ChampMenuPresenter.class);
        //        SearchListAdapter searchListAdapter = new SearchListAdapter(champMenuPresenter);
        //        RecyclerView.ViewHolder viewHolder = searchListAdapter.onCreateViewHolder(mock(ViewGroup.class), Constant.ViewType.OFFSET_VIEW);
        //        Assert.assertTrue(viewHolder instanceof OffSetViewHolder);
        // TODO
    }

    @Test
    fun EnsureWeDontSetChampToBlankItem() {
        val champMenuPresenter = mock(ChampMenuPresenter::class.java)
        val champMenuListAdapter = ChampMenuListAdapter(champMenuPresenter)
        champMenuListAdapter.onBindViewHolder(mock(OffSetViewHolder::class.java), 0)

    }


}
