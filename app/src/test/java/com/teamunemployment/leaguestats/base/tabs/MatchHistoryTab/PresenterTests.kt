package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab

import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.utils.Role
import com.teamunemployment.leaguestats.data.Statics
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.data.room.champ.SimpleChampDao
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.MatchHistoryCardViewContract
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchHistoryCardData
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchIdentifier
import com.teamunemployment.leaguestats.utils.champ.ChampPresenter

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import java.util.*

/**
 * @author Josiah Kendall
 */
class PresenterTests {

    internal var role = Role()
    val random = Random()
    val network = mock(Network::class.java)
    internal var matchHistoryInteractor = mock(MatchHistoryInteractor::class.java)
    internal var summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
    internal var matchHistoryRecycleViewAdapter = mock(MatchHistoryRecycleViewAdapter::class.java)
    val champHelper = mock(ChampPresenter::class.java)
    internal var presenter = MatchHistoryPresenter(matchHistoryInteractor, summonerRapidAccessObject, matchHistoryRecycleViewAdapter, champHelper)
    internal var matchHistoryCardViewContract = mock(MatchHistoryCardViewContract::class.java)
    internal var summonerId = "Josiahs_ID"
    internal var region = "OCE"
    internal var roleString = 1

    @Before
    fun setUp() {

        `when`(summonerRapidAccessObject.summonerId).thenReturn(summonerId)
        `when`(summonerRapidAccessObject.getRegion()).thenReturn(region)
        `when`(summonerRapidAccessObject.role).thenReturn(roleString)

    }

    // vnot going to work atm
    @Test
    fun LoadMatchList_LoadsMatchListFromNetwork() {

        presenter.setView(mock(MatchHistoryTabView::class.java))
        presenter.loadDataForRole(Statics.TOP, "-1")
        Mockito.`when`(matchHistoryRecycleViewAdapter.itemCount).thenReturn(0)
        // summonerId, role, matchHistoryRecycleViewAdapter.itemCount, this
        verify(matchHistoryInteractor, times(1)).loadMatches(
                "-1",
                Statics.TOP,
                0,
                presenter)
    }

    @Test
    fun LoadMatchSummary_LoadsMatchSummaryFromNetwork() {
        val matchHistoryInteractor = mock(MatchHistoryInteractor::class.java)
        val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
        presenter.setView(mock(MatchHistoryTabView::class.java))
        presenter.loadMatchSummary(123456)
        //        verify(matchHistoryInteractor, times(1)).LoadFreshMatchSummary(123456, presenter);
    }

    @Test
    fun LoadMatchList_AttemptsToLoadMatchListFromTheCache() {
        // todo
    }

    @Test
    fun LoadMatchSummary_AttemptsToLoadMatchFromCache() {
        val matchHistoryInteractor = mock(MatchHistoryInteractor::class.java)
        val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
        presenter.setView(mock(MatchHistoryTabView::class.java))
        presenter.loadMatchSummary(123456)
        //        verify(matchHistoryInteractor, times(1)).loadCachedMatchHistoryData(123456, presenter);
    }

    @Test
    fun `Test that we show placeholder when we have no matches`() {
        val view = mock(MatchHistoryTabView::class.java)
        presenter.setView(view)
        presenter.onMatchListLoadedSuccessfully(ArrayList(), 200)
        verify(view, times(1)).showPlaceholder()
        verify(view, times(0)).hidePlaceholder()
    }

    @Test
    fun `Test that we hide placeholder when we have matches`() {
        val view = mock(MatchHistoryTabView::class.java)
        val array = ArrayList<MatchIdentifier>()
        array.add(mock(MatchIdentifier::class.java))
        `when`(matchHistoryRecycleViewAdapter.itemCount).thenReturn(1)
        presenter.setView(view)
        presenter.onMatchListLoadedSuccessfully(array, 200)
        verify(view, times(0)).showPlaceholder()
        verify(view, times(1)).hidePlaceholder()
    }

    @Test
    fun LoadMatchList_DoesntHideLoadingSpinnerIfWeDontFindAnythingInTheCache() {

    }

    @Test
    fun OnMatchesLoaded_TriggersSetAdapterWhenLoadedSuccessfully() {
        val matchHistoryInteractor = mock(MatchHistoryInteractor::class.java)
        val tabView = mock(MatchHistoryTabView::class.java)
        presenter.setView(tabView)
        val matchIdWrappers = ArrayList<MatchIdentifier>()
        presenter.onMatchListLoadedSuccessfully(matchIdWrappers, 200)
        verify(matchHistoryRecycleViewAdapter, times(1)).addData(matchIdWrappers)
    }


    @Test
    fun MakeSureThatWeOnlySetResultsIfCodeIsIn200_300Group() {

    }

    @Test
    fun OnMatchesLoadError_TriggersErrorMessage() {
        val matchHistoryInteractor = mock(MatchHistoryInteractor::class.java)
        val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
        val tabView = mock(MatchHistoryTabView::class.java)
        presenter.setView(tabView)

        presenter.onError(mock(Throwable::class.java))
        verify(tabView, times(1)).showMessage("An error occurred. Please try again.")
    }

    @Test
    fun LoadCardData_SetsGraph123WithCorrectData() {
        val kills = 0
        val deaths = 0
        val matchHistoryInteractor = mock(MatchHistoryInteractor::class.java)
        val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
        val cardViewContract = mock(MatchHistoryCardViewContract::class.java)
        val champId = random.nextLong()
        val matchId = random.nextLong()
        val enemyChampId = random.nextLong()

        val h1 = produceRandomHeadToHeadStat()
        val h2 = produceRandomHeadToHeadStat()
        val h3 = produceRandomHeadToHeadStat()

        val won = random.nextBoolean()

        val matchHistoryCardData = MatchHistoryCardData(
                matchId,
                champId ,
                enemyChampId,
                h1,
                h2,
                h3,
                won,
                random.nextInt().toString(),
                summonerId,
                "",
                random.nextLong())
                presenter.setLoadedCardData(matchHistoryCardData, cardViewContract,random.nextLong())
                verify(cardViewContract, times(1)).setGraph1(h1)
                verify(cardViewContract, times(1)).setGraph2(h2)
                verify(cardViewContract, times(1)).setGraph3(h3)
    }

    //@PrimaryKey(autoGenerate = false) val matchId: Long,
    //                           val champId: Long,
    //                           val enemyChampId: Long,
    //                           @TypeConverters(HeadToHeadStatConverter::class) val earlyGame: HeadToHeadStat,
    //                           @TypeConverters(HeadToHeadStatConverter::class) val midGame: HeadToHeadStat,
    //                           @TypeConverters(HeadToHeadStatConverter::class) val lateGame: HeadToHeadStat,
    //                           val won : Boolean,
    //                           val detailsUrl : String,
    //                           val summonerId : String,
    //                           val heroChampName : String,
    //                           val dateTime : Long

    @Test
    fun LoadCardData_SetsGraph2WithDeaths() {
        val matchHistoryInteractor = mock(MatchHistoryInteractor::class.java)
        val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
        val cardViewContract = mock(MatchHistoryCardViewContract::class.java)

        //        MatchHistoryCardData matchHistoryCardData = new MatchHistoryCardData(1, "vi", kills, deaths, cs1, cs2,cs3, 1);
        //        presenter.setLoadedCardData(matchHistoryCardData, cardViewContract);
        //        verify(cardViewContract, times(1)).setGraph2(cs3);
    }

    @Test
    fun LoadCardData_SetsGraph3WithCreepScoreFirst10Minutes() {
        val matchHistoryInteractor = mock(MatchHistoryInteractor::class.java)
        val summonerRapidAccessObject = mock(SummonerRapidAccessObject::class.java)
        val cardViewContract = mock(MatchHistoryCardViewContract::class.java)

        //        MatchHistoryCardData matchHistoryCardData = new MatchHistoryCardData(1, "vi", kills, deaths, cs1, cs2,cs3, 1);
        //        presenter.setLoadedCardData(matchHistoryCardData, cardViewContract);
        //        verify(cardViewContract, times(1)).setGraph3(cs1);
    }

    @Test
    fun makeSureThatWeFetchTheCorrectMethodIfWeDoNotHaveAChampSet() {
        //        when(summonerRapidAccessObject.getChamp()).thenReturn(-1);
        //        presenter.setView(mock(TabContract.View.class));
        //        presenter.start();
        //
        //        verify(matchHistoryInteractor,times(1))
        //                .loadMatches(
        //                        "oce",
        //                        summonerRapidAccessObject.getSummonerId(),
        //                        summonerRapidAccessObject.getRole(),
        //                        0,
        //                        presenter);
    }

    @Test
    fun makeSureThatWeFetchTheCorrectMethodIfWeHaveAChampSet() {
        //        when(summonerRapidAccessObject.getChamp()).thenReturn(3);
        //        presenter.setView(mock(TabContract.View.class));
        //        presenter.start();
        //
        //        verify(matchHistoryInteractor,times(1))
        //                .loadMatches(
        //                        "oce",
        //                        summonerRapidAccessObject.getSummonerId(),
        //                        summonerRapidAccessObject.getRole(),
        //                        3,
        //                        0,
        //                        presenter);

    }

    @Test
    fun makeSureThatWeDoNotLoadNewInformationIfWeDoNotHaveMoreThan20ItemsInTheList() {
        `when`(summonerRapidAccessObject.champKey).thenReturn(-1)
        presenter.loadMoreDataForRole(20)
        verify(matchHistoryInteractor, times(1)).loadMatches(summonerId, roleString, 20, presenter)
    }

    @Test
    fun makeSureThatWeDoNotLoadNewInformationIfWeDoNotHaveMoreThan20ItemsInTheListAndWeHaveAChamp() {
        `when`(summonerRapidAccessObject.champKey).thenReturn(10)
        presenter.loadMoreDataForRole(20)
        verify(matchHistoryInteractor, times(1)).loadMatches(region, summonerId, roleString, 10, 20, presenter)
    }

    @Test
    fun makeSureThatWeDoNotLoadNewInformationIfWeDoNotHaveMoreThan40ItemsInTheList() {
        `when`(summonerRapidAccessObject.champKey).thenReturn(10)
        presenter.loadMoreDataForRole(21)
        verify(matchHistoryInteractor, times(0)).loadMatches(region, summonerId, roleString, 10, 21, presenter)

    }

    @Test
    fun makeSureThatWeDoLoadNewInformationIfWeHaveMoreThan20ItemsInTheList() {

    }

    @Test
    fun makeSureThatWeDoLoadNewInformationIfWeHaveMoreThan40ItemsInTheList() {

    }

    @Test
    fun LoadCardData_SetsCorrectChampIconUrl() {
        //        Assert.fail();
    }

    @Test
    fun LoadCardData_SetsCorrectKDAString() {
        //        Assert.fail();
    }

    private fun produceRandomHeadToHeadStat() : HeadToHeadStat {
        return HeadToHeadStat(random.nextFloat(), random.nextFloat())
    }

}
