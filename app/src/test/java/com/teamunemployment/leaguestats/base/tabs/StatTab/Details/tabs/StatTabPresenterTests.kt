package com.teamunemployment.leaguestats.base.tabs.StatTab.Details.tabs

import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.tabs.StatDetailsTabPresenter
import org.junit.Test
import org.mockito.Mockito.mock

class StatTabPresenterTests {

    val presenter = StatDetailsTabPresenter(mock(SummonerRapidAccessObject::class.java))

    @Test
    fun testThatWeCanLoadTheCorrectMethodWhenWeHaveNoChamp() {

    }

    @Test
    fun `test That We Can Average Head To Head Stat List Of Ten Items`() {
        val heroStats = ArrayList<Float>()
        heroStats.add(10f)
        heroStats.add(5f)
        heroStats.add(5f)
        heroStats.add(10f)
        heroStats.add(10f)
        heroStats.add(10f)
        heroStats.add(10f)
        heroStats.add(10f)
        heroStats.add(10f)
        heroStats.add(10f)

        val enemyStats = ArrayList<Float>()
        enemyStats.add(10f)
        enemyStats.add(5f)
        enemyStats.add(5f)
        enemyStats.add(10f)
        enemyStats.add(20f)
        enemyStats.add(10f)
        enemyStats.add(15f)
        enemyStats.add(5f)
        enemyStats.add(20f)
        enemyStats.add(25f)



        val arrayList = ArrayList<ArrayList<Float>>()

        arrayList.add(enemyStats)
        arrayList.add(heroStats)


        val averages = presenter.produceAverageFromRawData(arrayList)
        assert(averages[0] == 10f)
        assert(averages[1] == 7.5f)
        assert(averages[2] >6.66f && averages[2] < 6.7f)
        assert(averages[3] == 7.5f)
        assert(averages[4] == 10f)
        assert(averages[5] == 10f)
        assert(averages[6] == 10.714286f)
        assert(averages[7] == 10f)
        assert(averages[8] == 11.111111f)
        assert(averages[9] == 12.5f)

    }

    @Test
    fun `Test that I can average correctly with less than 5 items`() {

    }

    @Test
    fun `Test that i can average correctly with 1 item`() {

    }
}