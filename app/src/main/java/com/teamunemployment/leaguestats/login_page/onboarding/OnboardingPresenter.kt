package com.teamunemployment.leaguestats.login_page.onboarding

import com.teamunemployment.leaguestats.login_page.onboarding.model.SyncProgress
import com.google.android.material.snackbar.Snackbar
/**
 * Created by Josiah Kendall
 */
class OnboardingPresenter(private val onboardingInteractor: OnboardingInteractor) {

    private lateinit var view : OnboardingContract.View
    private val messages = OnboardingMessages()

    fun setView(view : OnboardingContract.View) {
        this.view = view
    }

    /**
     * Do any setup shit required for this screen. Currently that is just that it kicks off a sync
     */
    fun start(summonerId : String) {
        if (summonerId == (-1).toString()) {
            // TODO handle this
        }

        onboardingInteractor.requestSync(this, summonerId)
//        TODO("Fetch user details about name, rank etc and show it at the top of the screen")
    }

    /**
     * @param syncProgress The object holding the info we need.
     */
    fun handleSyncProgressUpdate(syncProgress: SyncProgress, summonerId: String) : Boolean {
        if (syncProgress.total == 0 && syncProgress.completed == 0) {
            return false
        }
        view.setTotalMatches(syncProgress.total)
        view.setSyncedMatches(syncProgress.completed)
        view.setProgressTextMessage(produceLoadingMessage(syncProgress.completed,syncProgress.total))

        if (syncProgress.total != syncProgress.completed) {
            return false
        }

        return true
    }

    private fun produceLoadingMessage(progress : Int, max : Int) : String{
        return "Downloading matches from Riot servers - $progress/$max"
    }

    /**
     * Handle the sync result. Launches the main screen if true, else it will show an error to the user.
     * @param success   The result of the sync. True means the user synced successfully, so progress
     *                  to the home screen.
     */
    fun handleSyncResult(success : Boolean, summonerId: String) {
        // launch main screen
        if (!success) {
            view.showMessage(messages.syncFailure())
            return
        }

        launchHome(summonerId)

    }

    fun launchHome(summonerId: String) {
        view.launchHomeScreen(summonerId)
    }

    /**
     * Show a little [Snackbar] with the given string. Has no buttons or anything of the like.
     *
     * @param message The text to show
     */
    fun showMessage(message: String) {
        view.showMessage(message)
    }

    /**
     * Show a mock progress. We don't actually get any progress reports from the server, but we know
     * the rough timescale and can give them an impression of the app loading
     */
    fun showMockProgress() {
        Thread(Runnable {
            for (i in 1..100) {
                Thread.sleep(250)
                handleSyncProgressUpdate(SyncProgress(100, i), "")
            }
        }).start()
    }
}