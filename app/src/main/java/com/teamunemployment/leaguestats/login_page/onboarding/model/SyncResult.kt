package com.teamunemployment.leaguestats.login_page.onboarding.model

class SyncResult(val matchesSynced : Int,
                 val successful : Boolean,
                 val resultCode : Int)