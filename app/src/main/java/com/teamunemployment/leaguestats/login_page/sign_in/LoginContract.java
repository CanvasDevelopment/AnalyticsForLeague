package com.teamunemployment.leaguestats.login_page.sign_in;

import android.widget.ArrayAdapter;

import com.teamunemployment.leaguestats.PresenterContract;
import com.teamunemployment.leaguestats.ViewContract;

/**
 * @author Josiah Kendall.
 */
public interface LoginContract {
    interface Presenter extends PresenterContract {
        void loginTestUser();
        void setView(LoginContract.LoginView loginView);
        void requestUserRegistration();
        void handleRegisterUserResult(Integer code, String summonerId, String region);
    }

    interface LoginView extends ViewContract {
        void launchOnboardingActivity(String summonerId);
        String getUserName();
        String getRegion();
        void showLoginProgressSpinner();
        void showLoginButton();
        void hideProgressSpinner();
        void hideLoginButton();
        void disableProceed();
        void enableProceed();
        void launchMainScreen(String summonerId);
    }

}
