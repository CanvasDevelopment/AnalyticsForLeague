package com.teamunemployment.leaguestats.login_page.onboarding

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.base.BaseActivityView
import kotlinx.android.synthetic.main.onboarding_view.*
import org.koin.android.ext.android.inject
import timber.log.Timber
import com.teamunemployment.leaguestats.login_page.onboarding.PaperOnboarding.PaperOnboardingPage
import com.teamunemployment.leaguestats.login_page.onboarding.PaperOnboarding.PaperOnboardingFragment


/**
 * Created by Josiah Kendall
 */
class OnboardingView : Activity(), OnboardingContract.View {

    val presenter by inject<OnboardingPresenter>()
    val srao by inject<SummonerRapidAccessObject>()

    override fun launchHomeScreen(summonerId : String) {
        srao.saveCurrentSummonerId(summonerId)
        val mainIntent = Intent(this, BaseActivityView::class.java)
//        mainIntent.putExtra("summoner_id", summonerId)
        startActivity(mainIntent)
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView( R.layout.onboarding_view)
        presenter.setView(this)

        val summonerId : String? = intent.getStringExtra("summoner_id")

        if (summonerId != null) {
            presenter.start(summonerId)
        }

        setUpOnBoarding()
    }

    private fun setUpOnBoarding() {
        val scr1 = PaperOnboardingPage("Match History",
                "Match history shows an aggregated performance indicator for each game stage. Click details to view individual stats",
                Color.parseColor("#1F1F1F"), R.drawable.match_history, R.drawable.ic_selected_oval, R.drawable.background)
        val scr2 = PaperOnboardingPage("Analyse",
                "View your performance, compared to your enemy lane, across various statistics",
                Color.parseColor("#1F1F1F"), R.drawable.stat_performance, R.drawable.ic_selected_oval, R.drawable.background)
        val scr3 = PaperOnboardingPage("Goals (Coming Soon)",
                "Set statistical goals, and track your progress.",
                Color.parseColor("#1F1F1F"), R.drawable.goals, R.drawable.ic_selected_oval,R.drawable.background)

        val elements = ArrayList<PaperOnboardingPage>()
        elements.add(scr1)
        elements.add(scr2)
        elements.add(scr3)

        val onBoardingFragment = PaperOnboardingFragment.newInstance(elements)
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.fragment_holder, onBoardingFragment)
        fragmentTransaction.commit()
    }

    override fun showMessage(message: String) {
        Timber.d(message)
    }

    override fun setTotalMatches(matches: Int) {
        sync_progress.max = matches
    }

    override fun setSyncedMatches(matches: Int) {
        sync_progress.progress = matches
    }

    override fun setProgressTextMessage(message: String) {
        runOnUiThread { progress_text.text = message  }

    }

    override fun showProcessingStarted() {
        progress_text.text = "Finalizing data ..."
    }

}