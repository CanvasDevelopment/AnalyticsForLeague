package com.teamunemployment.leaguestats.login_page.sign_in

import android.text.Editable
import android.text.TextWatcher
import com.teamunemployment.leaguestats.R
import timber.log.Timber
import kotlin.collections.ArrayList


/**
 * @author Josiah Kendall
 */
class LoginPresenter
constructor(private val arrayAdapterFactory: ArrayAdapterFactory,
            private val loginInteractor: LoginInteractor) : LoginContract.Presenter, TextWatcher {

    private lateinit var view: LoginContract.LoginView
    private val loginErrors = LoginErrorMessages()

    override fun start() {

    }

    fun pingServices() {
        loginInteractor.pingService()
    }

    override fun handleError(e: Throwable) {
        view.showMessage(e.message)
        view.showLoginButton()
        view.hideProgressSpinner()
    }

    override fun restart() {
        view.showLoginButton()
        view.hideProgressSpinner()
    }

    override fun resume() {
        val userId = loginInteractor.isThereALoggedInUser()
        if (userId != "-1") {
            view.launchMainScreen(userId)
        }

        pingServices()
    }

    override fun pause() {

    }

    override fun stop() {

    }

    override fun destroy() {

    }

    override fun loginTestUser() {
//        view.launchOnboardingActivity()
    }

    override fun setView(loginView: LoginContract.LoginView) {
        this.view = loginView
    }

    /**
     * Method to produce a region array list for our adapter that feeds the region data to the view.
     */
    fun produceRegionsArrayList(regionStrings : Array<String>) : List<Region>{
        val regions = ArrayList<Region>()

        for (string in regionStrings) {
            regions.add(Region(string, R.drawable.ic_fiber_manual_record_black_24dp))
        }
       return regions
    }

    override fun requestUserRegistration() {

        val userName = view.userName
        val region = view.region
        view.hideLoginButton()
        view.showLoginProgressSpinner()
        loginInteractor.registerAUser(userName, region, this)
    }

    /**
     * Set the correct registerAUser result to the view.
     * @param loginResult The result returned from the registerAUser attempt.
     *                      Codes :
     *                          404: The user does not exist
     *                          500: The server failed to log you in.
     *                          200: Ok
     *                          -1 : General error message.
     */
    override fun handleRegisterUserResult(code: Int, summonerId : String, region : String) {
        Timber.d("Registering user: $summonerId. Response code was $code")
        if (summonerId == "-1" || summonerId.isBlank()) {
            view.showMessage(loginErrors.`404`())
            enableLogIn()
            return
        }
        when(code) {
            404 -> {
                view.showMessage(loginErrors.`404`())
                enableLogIn()
            }
            500 -> view.showMessage(loginErrors.`500`())
            503 -> view.showMessage(loginErrors.`500`())
            -1 -> view.showMessage(loginErrors.default())
            else -> {
                view.launchOnboardingActivity(summonerId)
            }
        }
    }

    private fun enableLogIn() {
        view.showLoginButton()
        view.hideProgressSpinner()
        view.enableProceed()
    }

    override fun afterTextChanged(s: Editable?) {
        if (s == null) {
            return
        }
        handleTextChanged(s.toString())
    }

    fun handleTextChanged(s: String) {
        when(s.isEmpty()) {
            true -> {view.disableProceed()}
            false -> {view.enableProceed()}
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        // not required
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (s == null) {
            view.disableProceed()
            return
        }
        handleTextChanged(s.toString())
    }
}
