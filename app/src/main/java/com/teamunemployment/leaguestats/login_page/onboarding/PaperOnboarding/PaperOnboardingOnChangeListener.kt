package com.teamunemployment.leaguestats.login_page.onboarding.PaperOnboarding


interface PaperOnboardingOnChangeListener {

    fun onPageChanged(oldElementIndex: Int, newElementIndex: Int)

}