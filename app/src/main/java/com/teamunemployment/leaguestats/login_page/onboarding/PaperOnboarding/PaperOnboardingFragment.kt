package com.teamunemployment.leaguestats.login_page.onboarding.PaperOnboarding

import android.app.Fragment
import android.os.Bundle
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.Nullable
import com.teamunemployment.leaguestats.R


/**
 * Ready to use PaperOnboarding fragment
 */
class PaperOnboardingFragment : Fragment() {

    private var mOnChangeListener: PaperOnboardingOnChangeListener? = null
    private var mOnRightOutListener: PaperOnboardingOnRightOutListener? = null
    private var mOnLeftOutListener: PaperOnboardingOnLeftOutListener? = null
    var elements: ArrayList<PaperOnboardingPage>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            elements = arguments.get(ELEMENTS_PARAM) as ArrayList<PaperOnboardingPage>?
        }
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (inflater == null) {
            return null
        }

        val view = inflater.inflate(R.layout.onboarding_main_layout, container, false)

        // create engine for onboarding element
        val mPaperOnboardingEngine = PaperOnboardingEngine(view.findViewById(R.id.onboardingRootView), elements, getActivity().getApplicationContext())
        // set listeners
        if (mOnChangeListener != null) {
            mPaperOnboardingEngine.setOnChangeListener(mOnChangeListener!!)
        }
        if (mOnLeftOutListener != null) {
            mPaperOnboardingEngine.setOnLeftOutListener(mOnLeftOutListener!!)
        }

        if (mOnRightOutListener != null) {
            mPaperOnboardingEngine.setOnRightOutListener(mOnRightOutListener!!)
        }

        return view
    }

    fun setOnChangeListener(onChangeListener: PaperOnboardingOnChangeListener) {
        this.mOnChangeListener = onChangeListener
    }

    fun setOnRightOutListener(onRightOutListener: PaperOnboardingOnRightOutListener) {
        this.mOnRightOutListener = onRightOutListener
    }

    fun setOnLeftOutListener(onLeftOutListener: PaperOnboardingOnLeftOutListener) {
        this.mOnLeftOutListener = onLeftOutListener
    }

    companion object {

        private val ELEMENTS_PARAM = "elements"


        fun newInstance(elements: ArrayList<PaperOnboardingPage>): PaperOnboardingFragment {
            val fragment = PaperOnboardingFragment()
            val args = Bundle()
            args.putSerializable(ELEMENTS_PARAM, elements)
            fragment.setArguments(args)
            return fragment
        }
    }

}