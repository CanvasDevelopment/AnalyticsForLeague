package com.teamunemployment.leaguestats.login_page.onboarding

import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import com.teamunemployment.leaguestats.io.service_layer.DefaultService
import com.teamunemployment.leaguestats.io.service_layer.ServiceProducer
import com.teamunemployment.leaguestats.login_page.onboarding.model.SyncResult
import retrofit2.Call

/**
 * Created by Josiah Kendall
 */
class OnboardingInteractor(private val retrofitFactory: RetrofitFactory,
                           private val network: Network,
                           private val serviceProducer: ServiceProducer) {
    var syncRetries = 0
    private lateinit var onboardingService : OnboardingService

    /**
     *
     */
    fun requestSync(onboardingPresenter: OnboardingPresenter, summonerId : String) {
        // show progress
        onboardingPresenter.showMockProgress()

        // build default service
        val url = network.getProcessingUrl()
        onboardingService = retrofitFactory.produceRetrofitInterface(OnboardingService::class.java, url)

        val defaultService = serviceProducer.produceService<SyncResult>()

        /** generate the [Call] with which we can use to fetch the data **/
        val call = onboardingService.syncUser(summonerId)

        /** Trigger the fetch of the data using the [DefaultService], and dispatching to
         * our given presenter **/
        defaultService.requestData(
                call,
                SyncResult::class.java,

                {
                    onboardingPresenter.handleSyncResult(it.data.successful, summonerId)
                }
        )


    }
}