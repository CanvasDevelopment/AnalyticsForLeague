package com.teamunemployment.leaguestats.login_page.di

import androidx.room.Room
import com.teamunemployment.leaguestats.data.room.AppDatabase
import com.teamunemployment.leaguestats.login_page.onboarding.OnboardingInteractor
import com.teamunemployment.leaguestats.login_page.onboarding.OnboardingPresenter
import org.koin.android.module.AndroidModule

/**
 * Created by Josiah Kendall
 */
class OnboardingModule : AndroidModule() {

    override fun context() = applicationContext {

        context(name = "OnboardingModule") {
            provide { OnboardingPresenter(get()) }
            provide { OnboardingInteractor(get(),get(),get())}
            provide<AppDatabase>  { Room.databaseBuilder(get(), AppDatabase::class.java, "database1").fallbackToDestructiveMigration().build() }
        }
    }
}