package com.teamunemployment.leaguestats.login_page.sign_in

import android.content.SharedPreferences
import co.metalab.asyncawait.async
import com.teamunemployment.leaguestats.utils.Constant
import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.utils.get
import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.data.room.AppDatabase
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber


/**
 * @author Josiah Kendall
 */

class LoginInteractor
constructor(
        private val retrofitFactory: RetrofitFactory,
            database: AppDatabase,
            private val network: Network,
            private  val sharedPreferences: SharedPreferences) {

    private lateinit var loginIo: LoginIo
    private val loginRemoteRepo: LoginRemoteRepo =  retrofitFactory.produceRetrofitInterface(LoginRemoteRepo::class.java, network.getUrl())

    fun isThereALoggedInUser() : String {
        return if (sharedPreferences.getString(Constant.CURRENT_SUMMONER_ID, "-1") != null)
            sharedPreferences.getString(Constant.CURRENT_SUMMONER_ID, "-1")!!
        else "-1"
    }

    fun pingService() {
        val url = "https://processing-dot-leaguestats.appspot.com/_ah/processing/api/v1/"
        val pinga = retrofitFactory.produceRetrofitInterface(LoginRemoteRepo::class.java, url)

        async {
            val wakeUpProcessingCall: Call<Result<String>> = pinga.wakeUpProcessing()
            val wakeupResponse : Response<Result<String>> = await { wakeUpProcessingCall.execute() }

            val wakeUpApiCall: Call<Result<String>> = loginRemoteRepo.wakeUpApi()
            val response2 : Response<Result<String>> = await { wakeUpApiCall.execute() }
            // Process the data into a usable state
//            val result: Result<String> = response.get()
//            val summonerDetails = result.data
            // handle the result
        }.onError {
            Timber.e(Throwable(it))
            // Should also probably log this, rather than returning to the user
//            presenter.handleError(Throwable("Something went wrong - check your connection and try again"))

        }
    }


    /**
     * Sends a request for the summoner to the server, and returns the response to the server. It
     * also saves the result to the db
     * @param summonerName The name of the summoner to search for
     * @param region The region we need to send the request to - todo
     * @param presenter
     */
    fun registerAUser(summonerName: String, region : String, presenter : LoginContract.Presenter) {
        val url = "https://processing-dot-leaguestats.appspot.com/_ah/processing/api/v1/"
        val loginRemoteRepoLocal = retrofitFactory.produceRetrofitInterface(LoginRemoteRepo::class.java, url)
        loginIo = LoginIo(loginRemoteRepoLocal)
        async {
            // Send call to server to register user // todo check if she exists first

            val call: Call<Result<String>> = loginRemoteRepoLocal.registerDirect(summonerName.replace(" ", "%20"), region)
            val response : Response<Result<String>> = await { call.execute() }
            // Process the data into a usable state
            val result: Result<String> = response.get()
            val puuid = result.data
            // handle the result
            presenter.handleRegisterUserResult(result.resultCode, puuid, region)
            println("Found user result : ${result.resultCode} with id : $puuid")
            // Cache the data (if we worked out ok)
            if (result.resultCode == 200 || result.resultCode == 349) {
                sharedPreferences.edit().putString(Constant.CURRENT_SUMMONER_ID, puuid).apply()
                await {
//                    result.data.cache(region)
                }
            }
        }.onError {
            // Should also probably log this, rather than returning to the user
            presenter.handleError(Throwable("Something went wrong - check your connection and try again"))
        }
    }
}
