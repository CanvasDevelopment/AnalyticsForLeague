package com.teamunemployment.leaguestats.login_page.sign_in

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.EditorInfo
import com.ramotion.directselect.DSListView

import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.base.BaseActivityView
import com.teamunemployment.leaguestats.login_page.onboarding.OnboardingView

import kotlinx.android.synthetic.main.login_view.*
import org.koin.android.ext.android.inject
import java.lang.IllegalStateException

/**
 * @author Josiah Kendall
 *
 * Simple registerAUser screen for new users.
 */
class LoginView : AppCompatActivity(), LoginContract.LoginView {

    val presenter by inject<LoginPresenter>()

    lateinit var pickerView : DSListView<Region>

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_view)
        pickerView = findViewById(R.id.ds_picker)
        bindButtons()
        presenter.setView(this)
        presenter.start()
        user_name.imeOptions = EditorInfo.IME_ACTION_DONE
        setUpRegions()
        user_name.addTextChangedListener(presenter)

    }

    fun setUpRegions() {

        // Create adapter with our dataset
        val adapter = RegionAdapter(
                this, R.layout.region_item,
                presenter.produceRegionsArrayList(resources.getStringArray(R.array.regions_array)))

        // Set adapter to our DSListView
        pickerView.setAdapter(adapter)
    }

    override fun onResume() {
        super.onResume()
        presenter.resume()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        // Close the app
        val a = Intent(Intent.ACTION_MAIN)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }

    override fun showMessage(message: String) {
        Snackbar.make(login_root, message, Snackbar.LENGTH_LONG).show()
    }

    private fun bindButtons() {
        loginWithCredentials.setOnClickListener { presenter.requestUserRegistration() }
    }

    override fun launchOnboardingActivity(summonerId : String) {
        val mainIntent = Intent(this, OnboardingView::class.java)
        mainIntent.putExtra("summoner_id", summonerId)
        startActivity(mainIntent)
        overridePendingTransition( R.anim.slide_in_entry, R.anim.slide_out_entry)
    }

    override fun launchMainScreen(summonerId : String?) {
        val mainIntent = Intent(this, BaseActivityView::class.java)
        startActivity(mainIntent)
        overridePendingTransition( R.anim.slide_in_entry, R.anim.slide_out_entry)
    }

    override fun getUserName(): String {
        return user_name.text.toString()
    }

    override fun getRegion(): String {
        // need to get the code
        val values = resources.getStringArray(R.array.regions_array)
        when(pickerView.selectedItem.title) {
            values[0] -> return "OC1"
            values[1] -> return "NA1"
            values[2] -> return "EUW1"
            values[3] -> return "EUN1"
            values[4] -> return "JP1"
            values[5] -> return "KR"
            values[6] -> return "LA1"
            values[7] -> return "LA2"
            values[8] -> return "RU"
            values[9] -> return "BR1"
            values[10] -> return "TR1"
        }

        throw IllegalStateException("Failed to find region")
    }

    override fun showLoginProgressSpinner() {
        loginProgress.visibility = View.VISIBLE
    }

    override fun showLoginButton() {
        loginWithCredentials.visibility = View.VISIBLE
    }

    override fun hideProgressSpinner() {
        loginProgress.visibility = View.INVISIBLE
    }

    override fun hideLoginButton() {
        loginWithCredentials.visibility = View.INVISIBLE
    }

    override fun disableProceed() {
        loginWithCredentials.isEnabled = false
    }

    fun failLogin(message: String) {
        showMessage(message)
        hideProgressSpinner()
        showLoginButton()
    }

    override fun enableProceed() {
        loginWithCredentials.isEnabled = true
    }
}
