package com.teamunemployment.leaguestats.login_page.sign_in

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.ramotion.directselect.DSAbstractPickerBox
import com.teamunemployment.leaguestats.R

class RegionPickerBox @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : DSAbstractPickerBox<Region>(context, attrs, defStyleAttr) {

    private var text: TextView? = null
    private var icon: ImageView? = null
    private var cellRoot: View? = null

    init {
        init(context)
    }

    private fun init(context: Context) {
        val mInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mInflater.inflate(R.layout.region_box_picker, this, true)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        this.text = findViewById(R.id.custom_cell_text)
        this.icon = findViewById(R.id.custom_cell_image)
        this.cellRoot = findViewById(R.id.custom_cell_root)
    }

    override fun onSelect(selectedItem: Region, selectedIndex: Int) {
        this.text!!.text = selectedItem.title
        this.icon!!.setImageResource(selectedItem.icon)
    }

    override fun getCellRoot(): View? {
        return this.cellRoot
    }
}