package com.teamunemployment.leaguestats.login_page.onboarding

/**
 * Created by Josiah Kendall
 */
interface OnboardingContract {

    interface View {

        fun showMessage(message : String)
        fun launchHomeScreen(summonerId : String)
        fun setTotalMatches(matches : Int)
        fun setSyncedMatches(matches: Int)
        fun setProgressTextMessage(message : String)
        fun showProcessingStarted()
    }

    interface Presenter {

    }
}