package com.teamunemployment.leaguestats.login_page.sign_in

import com.teamunemployment.leaguestats.data.model.Result
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Josiah Kendall
 */
interface LoginRemoteRepo {

    @GET("summoner/v1/isRegistered/{summonerName}/{region}")
    fun getSummoner(@Path("summonerName") summonerName : String,
                    @Path("region") region : String) : Call<Result<Boolean>>

    @GET("summoner/v1/register/{summonerName}/{region}")
    fun register(@Path("summonerName") summonerName : String,
                 @Path("region") region : String) : Call<Result<SummonerDetails>>

    @GET("createNewUser/{summonerName}/{region}")
    fun registerDirect(@Path("summonerName") summonerName : String,
                 @Path("region") region : String) : Call<Result<String>>

    @GET("wakeUp")
    fun wakeUpProcessing() : Call<Result<String>>

    @GET("summoner/v1/wakeUp")
    fun wakeUpApi() : Call<Result<String>>

    @GET("summoner/v1/sync/{summonerId}")
    fun beginSync(@Path("summonerId") summonerId : String) : Call<Result<String>>
}