package com.teamunemployment.leaguestats

import android.app.Application
import androidx.room.Room
import com.teamunemployment.leaguestats.data.room.AppDatabase
import com.teamunemployment.leaguestats.data.room.champ.ChampModule
import com.teamunemployment.leaguestats.data.source.DatasourceModule
import com.teamunemployment.leaguestats.di.BaseModule

import com.teamunemployment.leaguestats.base.BaseActivityModule
import com.teamunemployment.leaguestats.base.champ_menu.ChampMenuModule
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen.MatchHistoryDetailsModule
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.MatchHistoryModule
import com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.StatDetailsModule
import com.teamunemployment.leaguestats.base.tabs.StatTab.di.StatModule
import com.teamunemployment.leaguestats.login_page.di.SignInModule
import com.teamunemployment.leaguestats.io.di.IoModule
import com.teamunemployment.leaguestats.login_page.di.OnboardingModule
import org.koin.android.ext.android.startAndroidContext
import org.koin.android.module.AndroidModule
import timber.log.Timber

/**
 * @author Josiah Kendall.
 *
 * Application class. For objects that should only exist once per application lifespan. i.e Dagger
 */

class App : Application() {


    companion object {
        lateinit var database : AppDatabase
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        // Initialise our room database
        database = Room.databaseBuilder(
                    this,
                    AppDatabase::class.java,
                    "analytics_for_league.db"
        )
        .build()

        // initialise koin
        startAndroidContext(this, modules())
    }


    private fun modules() : List<AndroidModule> {
        val list = ArrayList<AndroidModule>()
        list.add(SignInModule())
        list.add(IoModule())
        list.add(ChampModule())
        list.add(OnboardingModule())
        list.add(BaseActivityModule())
        list.add(ChampMenuModule())
        list.add(StatModule())
        list.add(MatchHistoryModule())
        list.add(BaseModule())
        list.add(MatchHistoryDetailsModule())
        list.add(DatasourceModule())
        list.add(StatDetailsModule())
        return list
    }


}
