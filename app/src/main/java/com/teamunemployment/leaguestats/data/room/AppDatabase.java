package com.teamunemployment.leaguestats.data.room;


import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.teamunemployment.leaguestats.data.HeadToHeadStatConverter;
import com.teamunemployment.leaguestats.data.model.Champ;
import com.teamunemployment.leaguestats.data.model.SimpleChamp;
import com.teamunemployment.leaguestats.data.room.MatchHistory.MatchHistoryDao;
import com.teamunemployment.leaguestats.data.room.champ.ChampDao;
import com.teamunemployment.leaguestats.data.room.champ.SimpleChampDao;
import com.teamunemployment.leaguestats.data.room.summoner.Summoner;
import com.teamunemployment.leaguestats.data.room.summoner.SummonerDao;
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchHistoryCardData;
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchIdentifier;
import com.teamunemployment.leaguestats.io.service_layer.cache.CacheDao;
import com.teamunemployment.leaguestats.io.service_layer.cache.Request;

@Database(entities = {
        Summoner.class,
        MatchIdentifier.class,
        MatchHistoryCardData.class,
        Champ.class,
        SimpleChamp.class,
        Request.class}, version = 10)
@TypeConverters(HeadToHeadStatConverter.class)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SummonerDao summonerDao();
    public abstract MatchHistoryDao matchHistoryDao();
    public abstract ChampDao champDao();
    public abstract SimpleChampDao simpleChampDao();
    public abstract CacheDao cacheDao();
}
