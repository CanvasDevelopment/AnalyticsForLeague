package com.teamunemployment.leaguestats.data.room.summoner

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Josiah Kendall
 */
@Entity
class Summoner(val summonerId : String,
                @PrimaryKey(autoGenerate = false) val puuid : String,
               val summonerName : String,
               val summonerDivisionLevel : Int,
               val summonerDivision : String,
               val region : String)