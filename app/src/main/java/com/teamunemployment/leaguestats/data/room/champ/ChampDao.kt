package com.teamunemployment.leaguestats.data.room.champ

import androidx.room.*
import com.teamunemployment.leaguestats.data.model.Champ

@Dao
interface ChampDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createChamp(champ: Champ)

    @Delete
    fun deleteChamp(champ : Champ)

    @Query("Select * FROM champ where key = :champId and summonerId = :summonerId")
    fun loadChamp(champId : String, summonerId : String) : Champ?

    @Query("SELECT * FROM champ where summonerId = :summonerId")
    fun loadAllOurChamps(summonerId : String) : List<Champ>

}