package com.teamunemployment.leaguestats.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Champ(val version :String,
            val id : String,
            val key : String,
            val name : String,
            val title : String,
            val blurb : String,
            val games : Int,
            val summonerId : String) {
    @PrimaryKey(autoGenerate = true)
    var _id : Int = 0
}