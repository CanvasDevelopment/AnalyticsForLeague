package com.teamunemployment.leaguestats.data.room.champ

import androidx.room.*
import com.teamunemployment.leaguestats.data.model.SimpleChamp

@Dao
interface SimpleChampDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createChamp(simpleChamp: SimpleChamp)

    @Delete
    fun deleteChamp(simpleChamp : SimpleChamp)

    @Query("Select * FROM SimpleChamp where key = :champId")
    fun loadChamp(champId : String) : SimpleChamp?

    @Query("Select * FROM SimpleChamp where id = :champId")
    fun loadChampById(champId : String) : SimpleChamp?

}