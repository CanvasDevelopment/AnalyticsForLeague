package com.teamunemployment.leaguestats.data.model

class Positions {
    companion object Positions {
        const val TOP = 0
        const val MID = 1
        const val JUNGLE = 2
        const val ADC = 3
        const val SUPPORT = 4
    }
}
