package com.teamunemployment.leaguestats.data.model

import android.content.SharedPreferences
import com.teamunemployment.leaguestats.utils.Constant.Companion.CHAMP_KEY
import com.teamunemployment.leaguestats.utils.Constant.Companion.CURRENT_SUMMONER_ID
import com.teamunemployment.leaguestats.utils.Constant.Companion.ROLE_KEY

/**
 * @author Josiah Kendall
 *
 * Helper class for dealing with summoner shit that needs to be accessed rapidly but also have the
 * state persisted.
 */
class SummonerRapidAccessObject(private val sharedPreferences : SharedPreferences) {

    // We do this to ensure it is not null
    var summonerId : String = if (sharedPreferences.getString(CURRENT_SUMMONER_ID, "-1") != null) sharedPreferences.getString(CURRENT_SUMMONER_ID, "-1")!! else "-1"
    var role : Int = sharedPreferences.getInt(ROLE_KEY, 0)
    var champKey : Int = sharedPreferences.getInt(CHAMP_KEY, -1)

    fun getRegion() : String {
        return "OCE"
    }

    fun updateRole(role : Int) {
        this.role = role
        sharedPreferences.edit()
                .putInt(ROLE_KEY, role)
                .apply()
    }

    fun updateChampId(champKey : Int) {
        this.champKey = champKey
        sharedPreferences
                .edit()
                .putInt(CHAMP_KEY, champKey)
                .apply()
    }

    fun saveCurrentSummonerId(summonerId : String) {
        this.summonerId = summonerId
        sharedPreferences.edit()
                .putString(CURRENT_SUMMONER_ID, summonerId)
                .apply()
    }

    fun clearData() {
        sharedPreferences.edit().clear().apply()
    }
}