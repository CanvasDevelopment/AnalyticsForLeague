package com.teamunemployment.leaguestats.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class SimpleChamp(val version : String,
                  val id : String,
                  val key : String,
                  val name : String,
                  val title : String,
                  val blurb : String) {
    @PrimaryKey(autoGenerate = true)
    var _id : Int = 0

    fun toChamp(summonerId : String) : Champ {
        return Champ(version,id,key,name,title,blurb,0,summonerId)
    }
}