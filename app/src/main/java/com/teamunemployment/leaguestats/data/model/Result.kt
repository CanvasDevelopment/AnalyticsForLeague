package com.teamunemployment.leaguestats.data.model

/**
 * Created by Josiah Kendall
 */
data class Result<T>(
            val resultCode : Int,
            val data : T)