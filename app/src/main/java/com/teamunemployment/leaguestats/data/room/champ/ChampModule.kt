package com.teamunemployment.leaguestats.data.room.champ

import com.teamunemployment.leaguestats.data.room.AppDatabase
import org.koin.android.module.AndroidModule

class ChampModule : AndroidModule() {
    override fun context()= applicationContext {
        context(name = "ChampModule") {
            provide { get<AppDatabase>().champDao() }
            provide { get<AppDatabase>().simpleChampDao() }
        }
    }
}