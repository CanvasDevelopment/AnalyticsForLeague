package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.View

import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.MatchHistoryPresenter
import com.squareup.picasso.Picasso
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.utils.Role
import com.teamunemployment.leaguestats.utils.produceBarData
import com.teamunemployment.leaguestats.utils.setDefaultStyle
import kotlinx.android.synthetic.main.four_stage_stat_view.view.*
import kotlinx.android.synthetic.main.match_history_card.view.*
import kotlinx.android.synthetic.main.match_history_details_view.*
import kotlinx.android.synthetic.main.three_stage_stat_view.view.earlyGameGraph
import kotlinx.android.synthetic.main.three_stage_stat_view.view.lateGameGraph
import kotlinx.android.synthetic.main.three_stage_stat_view.view.midGameGraph

/**
 * @author Josiah Kendall.
 */

class MatchHistoryCardView(private val cardBase: View,
                           private val context: Context,
                           private val matchHistoryPresenter: MatchHistoryPresenter,
                           private val network: Network)
    : RecyclerView.ViewHolder(cardBase),
        MatchHistoryCardViewContract {

    var _detailsUrl : String = ""
    var _win : Boolean = false

    override fun setVictory() {
        // I know that this is weird but it is done for testing purposes - I don't want to be calling
        // runOnUIThread { } in the presenter logic as that will fuck up our testing
        matchHistoryPresenter.runOnUIThread {
            cardBase.result.text = context.getString(R.string.victory)
            cardBase.result.setTextColor(context.resources.getColor(R.color.lightgreen))
        }
    }

    override fun setDefeat() {
        matchHistoryPresenter.runOnUIThread {
            cardBase.result.text = context.getString(R.string.defeat)
            cardBase.result.setTextColor(context.resources.getColor(R.color.orange))
        }
    }

    override fun setChampPlaceHolders() {
        matchHistoryPresenter.runOnUIThread {
            val loading = "Loading"
            cardBase.totalGraph.setNoDataText(loading)
            cardBase.earlyGameGraph.setNoDataText(loading)
            cardBase.midGameGraph.setNoDataText(loading)
            cardBase.lateGameGraph.setNoDataText(loading)
        }
    }

    override fun getDetailsUrl(): String {
        return _detailsUrl
    }

    override fun getWin(): Boolean {
        return _win
    }

    override fun setWin(won: Boolean) {
        this._win = won
    }

    override fun setDetailsUrl(detailsUrl: String) {
        this._detailsUrl = detailsUrl
    }

    override fun setSummaryChart(headToHeadStat: HeadToHeadStat) {
        matchHistoryPresenter.runOnUIThread {
            val result = headToHeadStat.produceBarData(context)
            cardBase.cardDetailsButton.isEnabled = true
            cardBase.totalGraph.data = result
            cardBase.totalGraph.setDefaultStyle()
        }
    }

    override fun setGraph1(headToHeadStat: HeadToHeadStat) {
        // todo grey stuff  val greyDisplay = headToHeadStat.enemyStatValue == 0f || headToHeadStat.heroStatValue == 0f
        matchHistoryPresenter.runOnUIThread {
            val result = headToHeadStat.produceBarData(context)
            cardBase.earlyGameGraph.data = result
            cardBase.earlyGameGraph.setDefaultStyle()
        }
    }

    override fun setGraph2(headToHeadStat: HeadToHeadStat) {
        matchHistoryPresenter.runOnUIThread {
            val result = headToHeadStat.produceBarData(context)
            cardBase.midGameGraph.data = result
            cardBase.midGameGraph.setDefaultStyle()
        }
    }

    override fun setGraph3(headToHeadStat: HeadToHeadStat) {
        matchHistoryPresenter.runOnUIThread {
            val result = headToHeadStat.produceBarData(context)
            cardBase.lateGameGraph.data = result
            cardBase.lateGameGraph.setDefaultStyle()
        }
    }

    override fun setRole(roleIdentifier: Int) {
        when(roleIdentifier) {
            Role.TOP -> cardBase.roleIcon.setImageResource(R.drawable.top_with_padding)
            Role.JUNGLE -> cardBase.roleIcon.setImageResource(R.drawable.jungle_with_padding)
            Role.MID -> cardBase.roleIcon.setImageResource(R.drawable.mid_with_padding)
            Role.ADC -> cardBase.roleIcon.setImageResource(R.drawable.adc_with_padding)
            Role.SUPPORT -> cardBase.roleIcon.setImageResource(R.drawable.support_with_padding)
        }
    }

    override fun setGraph4(HeadToHeadStat: HeadToHeadStat) {
        //  setUpPieChart(barChart4, HeadToHeadStat);
    }

    override fun setHeroChampIcon(title: String) {

        val champString = title
                .replace(" ", "")
                .replace("'", "")
                .replace(".", "")

        matchHistoryPresenter.runOnUIThread {
            Picasso.get().load("${network.getRawUrl("")}/champion/$champString.png").into(cardBase.heroChampIcon)
        }
    }

    override fun setEnemyChampIcon(title: String) {
        val champString = title
                .replace(" ", "")
                .replace("'", "")
                .replace(".", "")

        matchHistoryPresenter.runOnUIThread {
            Picasso.get().load("${network.getRawUrl("")}/champion/$champString.png").into(cardBase.enemyChampIcon)
        }
    }

    override fun setTimeStamp(timeStamp: String) {
        matchHistoryPresenter.runOnUIThread { cardBase.timeAgo.text = timeStamp }
    }

    override fun setChamp(champName: String) {
        matchHistoryPresenter.runOnUIThread {
//            cardBase.result.text = (cardBase.result.text.toString() + champName)
        }
    }

    /**
     * Clear the card for the next one
     */
    fun clear() {
        matchHistoryPresenter.runOnUIThread {
            cardBase.result.text = ""
            cardBase.timeAgo.text = "..."
            cardBase.lateGameGraph.clear()
            cardBase.midGameGraph.clear()
            cardBase.earlyGameGraph.clear()
            cardBase.totalGraph.clear()
        }
    }
}
