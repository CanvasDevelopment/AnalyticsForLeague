package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics

import com.teamunemployment.leaguestats.base.tabs.TabContract
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.utils.Constant
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.base.BaseActivityView
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen.DetailsView
import kotlinx.android.synthetic.main.base.*

import kotlinx.android.synthetic.main.tab_view_fragment.*
import org.koin.android.ext.android.inject

/**
 * @author Josiah Kendall
 */
class MatchHistoryTabView : Fragment(), TabContract.View {

    private var rootView : View? = null

    private val firebaseAnalytics : FirebaseAnalytics by inject()
    val presenter: MatchHistoryPresenter by inject()
    val summonerRapidAccessObject : SummonerRapidAccessObject by inject()
    private val screenName = "MatchHistory"

    lateinit var baseActivityView : BaseActivityView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.rootView = inflater.inflate(R.layout.tab_view_fragment, container, false)
        baseActivityView = activity as BaseActivityView
        presenter.setView(this)
        firebaseAnalytics.setCurrentScreen(activity as BaseActivityView, screenName, null /* class override */)
        return rootView
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) logAnalyticsEvent()
    }

    private fun logAnalyticsEvent() {
        if (activity != null) {
            firebaseAnalytics.setCurrentScreen(activity as BaseActivityView, screenName, null /* class override */)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val baseView = activity as BaseActivityView
        baseView.setCorrectRoleAndChampView()
    }

    override fun showMessage(message: String) {
         Snackbar.make(baseActivityView.base_root, message, Snackbar.LENGTH_LONG).show()
    }

    override fun setRole(role: Int) {
        presenter.setRole(role)
        presenter.start()
    }

    override fun refresh() {
        presenter.start()
    }

    override fun setAdapter(adapter : TabContract.TabAdapter) {
        val matchHistoryAdapter = adapter as MatchHistoryRecycleViewAdapter
        val layoutManager = LinearLayoutManager(this.context)
        recyclerView.layoutManager = layoutManager as RecyclerView.LayoutManager?
        recyclerView.isNestedScrollingEnabled = true
        recyclerView.adapter = matchHistoryAdapter
    }

    override fun setLoadingVisible(visible: Boolean) {

    }

    override fun launchDetailsActivity(detailsUrl : String, win : Boolean) {
        val detailsIntent = Intent(this.context, DetailsView::class.java)
        detailsIntent.putExtra(Constant.DetailsPage.DETAILS_URL_KEY,detailsUrl)
        detailsIntent.putExtra(Constant.DetailsPage.RESULT, win)
        startActivity(detailsIntent)
        this.activity!!.overridePendingTransition( R.anim.slide_in_entry, R.anim.slide_out_entry)
    }

    fun showPlaceholder() {
        placeholder.visibility = View.VISIBLE
    }

    fun hidePlaceholder() {
        placeholder.visibility = View.GONE
    }
}
