package com.teamunemployment.leaguestats.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


import com.teamunemployment.leaguestats.base.tabs.StatTab.AnalyseTabView
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.MatchHistoryTabView
import com.teamunemployment.leaguestats.base.tabs.TabContract
import com.teamunemployment.leaguestats.utils.Constant

/**
 * @author Josiah Kendall.
 */
class TabAdapter(val fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private var currentTab: TabContract.View? = null
    private val role = 1

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).

        var resultFragment: Fragment

        if (position == 0) {
            resultFragment = MatchHistoryTabView()
        } else {
            resultFragment = AnalyseTabView()
            val args = Bundle()
            args.putInt(Constant.ROLE_KEY, 1)
            resultFragment.arguments = args
        }

        currentTab = resultFragment as TabContract.View?

        return resultFragment
    }

    override fun getCount(): Int {
        // Show 3 total pages.
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "History"
            1 -> return "Analyse"
        }
        return null
    }
}


