package com.teamunemployment.leaguestats.base

import android.util.Log
import co.metalab.asyncawait.async
import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.utils.SyncManager
import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.data.room.AppDatabase
import com.teamunemployment.leaguestats.data.room.summoner.SummonerDao
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import com.teamunemployment.leaguestats.login_page.onboarding.OnboardingService
import com.teamunemployment.leaguestats.login_page.onboarding.model.SyncProgress


import retrofit2.Call
import retrofit2.Response
import timber.log.Timber


/**
 * @author Josiah Kendall
 * Interactor for fetching
 */

class BaseActivityPersistenceInteractor
constructor(private val retrofitFactory : RetrofitFactory, // Our repositories.
            private val database : AppDatabase,
            private val network : Network,
            private val summonerDao: SummonerDao,
            private val summonerRapidAccessObject: SummonerRapidAccessObject,
            private val syncManager: SyncManager) : SyncManager.SyncContract {

    private lateinit var presenter: BaseActivityPresenter;

    private  val baseActivityService: BaseActivityService

    init {
        val url = network.getUrl()
        baseActivityService = retrofitFactory.produceRetrofitInterface(BaseActivityService::class.java, url)

    }
    fun fetchWinRateForRole(summonerId : String, role : Int, region : String, presenter: BaseActivityContract.Presenter) {
        async {
            val call : Call<Result<HeadToHeadStat>> = baseActivityService.getWinRateForRole(summonerId, role)
            val response : Response<Result<HeadToHeadStat>> = await { call.execute() }

            val result : Result<HeadToHeadStat> = response.get()
            val code = result.resultCode // do something here?
            presenter.onWinRateLoaded(result.data)
        }.onError {
            presenter.showMessage("Offline - check you internet connection")
        }
    }

    fun fetchWinRateForRole(summonerId : String, role : Int, region : String, champKey : String, presenter: BaseActivityContract.Presenter) {
        async {
            val call : Call<Result<HeadToHeadStat>> = baseActivityService.getWinRateForRole(summonerId, role, champKey)
            val response : Response<Result<HeadToHeadStat>> = await { call.execute() }

            val result : Result<HeadToHeadStat> = response.get()
            val code = result.resultCode // do something here?
            presenter.onWinRateLoaded(result.data)
        }.onError {
            presenter.showMessage("Offline - check you internet connection")
        }
    }

    private fun Response<Result<HeadToHeadStat>>.get() : Result<HeadToHeadStat> {
        if (body() != null) {
            return body()!!
        }

        return Result(404, HeadToHeadStat(-1f, -1f))
    }

    // Todo fix this shit
    fun loadSummonerName(baseActivityPresenter: BaseActivityPresenter) {
        this.presenter = baseActivityPresenter
        async {
            val summoner = await{summonerDao.loadSummoner(summonerRapidAccessObject.summonerId)}
            if (summoner == null) {
                // todod make summoner details on client side and server side match
                val call =  baseActivityService.fetchSummoner(summonerRapidAccessObject.summonerId)
//                call.request().url()
                val summonerDetails = await {call.execute()}
                if (summonerDetails.body()!!.resultCode == 200) {
                    baseActivityPresenter.setSummonerNameResult(summonerDetails.body()!!.data.name)
                }
            } else {
                baseActivityPresenter.setSummonerNameResult(summoner!!.summonerName)
            }
        }.onError {
            // issues loading summoner name
            Timber.e(it)
        }
    }

    fun launchRefresh(baseActivityPresenter: BaseActivityPresenter) {
        // todo set presenter
        this.presenter = baseActivityPresenter
        // just send a request and handle the return.
        // send request to

        val summonerId = summonerRapidAccessObject.summonerId

        async {
            val url = network.getProcessingUrl()
            // fetch summoner details
            val onboardingService = retrofitFactory.produceRetrofitInterface(OnboardingService::class.java, url)

            val sync = await{onboardingService.syncUser(summonerId).execute()}

            if (!sync.isSuccessful) {
                baseActivityPresenter.showMessage("Error occurred trying to sync user data - try again")
            } else {
                Log.e("SYNC", "Finished sync successfully")
                onCompletion()
            }
        }.onError {
            Log.e("SYNC", "Failed to sync : ${it.message}")
            onError("Sync failed - Please check your internet connection")
        }

//        syncManager.requestSync(this,summonerRapidAccessObject.summonerId)
    }

    override fun onProgressUpdate(data: SyncProgress): Boolean {
        if (data.completed == data.total) {
            return true
        }

        return false
    }

    override fun onCompletion() {
        presenter.finishRefresh()
    }

    override fun onError(message: String) {
        presenter.handleSyncError(message)
    }
}
