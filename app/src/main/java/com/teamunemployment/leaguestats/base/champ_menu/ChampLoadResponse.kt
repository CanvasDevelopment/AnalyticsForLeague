package com.teamunemployment.leaguestats.base.champ_menu

import com.teamunemployment.leaguestats.data.model.Champ

interface ChampLoadResponse {

    fun success(champs: List<Champ>)
    fun failure()
}