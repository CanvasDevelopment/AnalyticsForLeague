package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.LENGTH_LONG
import androidx.appcompat.app.AppCompatActivity
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen.model.GameStageView
import kotlinx.android.synthetic.main.match_history_details_view.*
import org.koin.android.ext.android.inject
import android.view.MotionEvent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.data.BarData
import com.google.firebase.analytics.FirebaseAnalytics
import com.squareup.picasso.Picasso
import com.teamunemployment.leaguestats.utils.Constant
import com.teamunemployment.leaguestats.utils.Role
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject

/**
 * @author Josiah Kendall
 */
class DetailsView : AppCompatActivity() {
    val presenter by inject<DetailsPresenter>()
    val firebaseAnalytics by inject<FirebaseAnalytics>()
    val summonerRapidAccessObject by inject<SummonerRapidAccessObject>()
    private var x1: Float = 0.toFloat()
    private var x2: Float = 0.toFloat()
    private val MIN_DISTANCE = 150
    private lateinit var earlyGameView : GameStageView
    private lateinit var midGameView : GameStageView
    private lateinit var lateGameView : GameStageView

    private val screenName = "MATCH_DETAILS"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // get match id from args
        val detailsUrl = intent.getStringExtra(Constant.DetailsPage.DETAILS_URL_KEY)
        val win = intent.getBooleanExtra(Constant.DetailsPage.RESULT, false)
        setContentView(R.layout.match_history_details_view)
        firebaseAnalytics.setCurrentScreen(this, screenName, null)
        presenter.setView(this)
        presenter.start(detailsUrl)
        setWinResult(win)
        setClickHandlers()
    }

    private fun setWinResult(win : Boolean) {
        if (win)  {
            matchHistoryDetailsToolbarTitle.text = "Victory"
            matchHistoryDetailsToolbarTitle.setTextColor(resources.getColor(R.color.lightgreen))
        }

        else {
            matchHistoryDetailsToolbarTitle.text = "Defeat"
            matchHistoryDetailsToolbarTitle.setTextColor(resources.getColor(R.color.orange))
        }
    }

    fun initialiseAdapter(adapter: MatchDetailsStatListAdapter) {
        val layoutManager = LinearLayoutManager(this)
        statsView.layoutManager = layoutManager as RecyclerView.LayoutManager?
        statsView.isNestedScrollingEnabled = true
        statsView.adapter = adapter
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> x1 = event.x
            MotionEvent.ACTION_UP -> {
                x2 = event.x
                val deltaX = x2 - x1
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    finish()
                }
            }
        }

        return super.onTouchEvent(event)
    }

    private fun setClickHandlers() {
        matchDetailsBackButton.setOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition( R.anim.slide_out_exit, R.anim.slide_in_exit)
    }

    /**
     * Show a message to the user in the form of a [Snackbar]
     *
     * @param message The message to display
     */
    fun showMessage(message : String) {
        runOnUiThread {  Snackbar.make(base_root, message, LENGTH_LONG) }
    }

    /**
     * Set the toolbar title
     */
    fun setToolbar(title : String) {
        // load result
        runOnUiThread {  matchHistoryDetailsToolbarTitle.text = title }
    }

    fun setCreepsEarlyGameChart(chartData: BarData, chartTitle : String) {
        earlyGameView.setCreeps(chartData, "",chartTitle)
    }

    fun setCreepsMidGameChart(chartData:BarData, chartTitle : String) {
        midGameView.setCreeps(chartData, "", chartTitle)
    }

    fun setCreepsLateGameChart(chartData: BarData, chartTitle : String) {
        lateGameView.setCreeps(chartData, "",chartTitle)
    }

    fun setDamageTakenEarlyGameChart(chartData: BarData, chartTitle : String) {
        earlyGameView.setDamageTaken(chartData, "",chartTitle)
    }

    fun setDamageDealtEarlyGameChart(chartData: BarData, chartTitle : String) {
        earlyGameView.setDamageDealt(chartData, "", chartTitle)
    }

    fun setGoldEarlyGameChart(chartData: BarData, chartTitle : String) {
        earlyGameView.setGold(chartData, "",chartTitle)
    }

    fun setXpEarlyGameChart(chartData: BarData, chartTitle : String) {
        earlyGameView.setXp(chartData, "", chartTitle)
    }

    fun setDamageTakenMidGameChart(chartData: BarData, chartTitle : String) {
        midGameView.setDamageTaken(chartData, "", chartTitle)
    }

    fun setDamageDealtMidGameChart(chartData: BarData, chartTitle : String) {
        midGameView.setDamageDealt(chartData, "", chartTitle)
    }

    fun setGoldMidGameChart(chartData: BarData, chartTitle : String) {
        midGameView.setGold(chartData, "", chartTitle)
    }

    fun setXpMidGameChart(chartData: BarData, chartTitle : String) {
        midGameView.setXp(chartData, "", chartTitle)
    }

    fun setDamageTakenLateGameChart(chartData: BarData, chartTitle : String) {
        lateGameView.setDamageTaken(chartData, "", chartTitle)
    }

    fun setDamageDealtLateGameChart(chartData: BarData, chartTitle : String) {
        lateGameView.setDamageDealt(chartData, "", chartTitle)
    }

    fun setGoldLateGameChart(chartData: BarData, chartTitle : String) {
        lateGameView.setGold(chartData, "", chartTitle)
    }

    fun setXpLateGameChart(chartData: BarData, chartTitle : String) {
        lateGameView.setXp(chartData, "", chartTitle)
    }

    fun setHeroChamp(champUrl : String) {
        runOnUiThread {  Picasso.get().load(champUrl).into(heroChamp) }
    }

    fun setEnemyChamp(champUrl : String) {
        runOnUiThread {  Picasso.get().load(champUrl).into(enemyChamp) }
    }

    fun setRole(roleIdentifier: Int) {
        when(roleIdentifier) {
            Role.TOP -> role.setImageResource(R.drawable.top_with_padding)
            Role.JUNGLE -> role.setImageResource(R.drawable.jungle_with_padding)
            Role.MID -> role.setImageResource(R.drawable.mid_with_padding)
            Role.ADC -> role.setImageResource(R.drawable.adc_with_padding)
            Role.SUPPORT -> role.setImageResource(R.drawable.support_with_padding)
        }
    }

    fun setGameDuration(gameDuration: String) {
        runOnUiThread { this.gameDuration.text = gameDuration }
    }
}