package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen.model

import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat

class Stat(
        val title : String,
        val headToHeadStats : ArrayList<HeadToHeadStat>)