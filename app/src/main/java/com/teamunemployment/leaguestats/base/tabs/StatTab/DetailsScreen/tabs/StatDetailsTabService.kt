package com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.tabs

import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.model.StatDetailsData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface StatDetailsTabService {

    @GET("analysis/v1/statDetails/{summonerId}/{games}/{position}/{statType}/{gameStage}")
    fun fetchMatches(@Path("summonerId") summonerId : String,
                     @Path("games") games : Int,
                     @Path("position") position : Int,
                     @Path("statType") statType : Int,
                     @Path("gameStage")gameStage : String) : Call<Result<StatDetailsData>>

    @GET("analysis/v1/statDetails/{summonerId}/{games}/{lane}/{statName}/{champKey}")
    fun fetchMatches(@Path("summonerId") summonerId : String,
                     @Path("games") games : Int,
                     @Path("lane") lane : String,
                     @Path("statName") statName : String,
                     @Path("champKey") champKey : String) : Call<Result<StatDetailsData>>
}