package com.teamunemployment.leaguestats.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import com.github.mikephil.charting.formatter.DefaultValueFormatter

import com.squareup.picasso.Picasso
import com.teamunemployment.leaguestats.base.champ_menu.ChampMenuContract
import com.teamunemployment.leaguestats.base.champ_menu.ChampMenuListAdapter
import com.teamunemployment.leaguestats.base.champ_menu.ChampMenuPresenter
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.utils.Role
import com.teamunemployment.leaguestats.utils.produceBarData
import com.teamunemployment.leaguestats.utils.setDefaultStyle
import com.teamunemployment.leaguestats.data.model.Champ
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.data.room.summoner.SummonerDao
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.base.tabs.TabContract
import com.teamunemployment.leaguestats.login_page.sign_in.LoginView


import java.util.ArrayList

import kotlinx.android.synthetic.main.base.*
import kotlinx.android.synthetic.main.search_test.*
import org.koin.android.ext.android.inject
import com.google.firebase.analytics.FirebaseAnalytics
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar


/**
 * @author Josiah Kendall.
 *
 * This is the base class of the main application page. Holds the bottom bar and the fragment viewpager.
 */
class BaseActivityView : AppCompatActivity(), BaseActivityContract.View, ChampMenuContract.View, TextWatcher {

    private var tabAdapter: TabAdapter? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val presenter by inject<BaseActivityPresenter>()
    private val searchPresenter by inject<ChampMenuPresenter>()
    private val searchListAdapter = ChampMenuListAdapter(searchPresenter)
    private val summonerRapidAccessObject by inject<SummonerRapidAccessObject>()
    private val summonerDao by inject<SummonerDao>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base)
        // Set views for our presenter

        presenter.setView(this)
        searchPresenter.setSearchView(this)

        // set listener for our search
        searchInput.addTextChangedListener(this)
        setUpTabs()
        setUpClickHandlers()
        setUpToolbar()
        ensureChampsUpToDate()
    }

    private fun ensureChampsUpToDate() {
        // send request to server to check version
        // if current version is newer
        searchPresenter.refreshChamps()
    }

    private fun setUpToolbar() {
        setSupportActionBar(toolbar)
        presenter.loadSummonerName()
    }

    override fun onBackPressed() {
        presenter.backPressed()
    }

    override fun onResume() {
        super.onResume()
    }

    fun closeApp() {
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(startMain)
    }

    fun setCorrectRoleAndChampView() {
            searchPresenter.setCurrentChamp(summonerRapidAccessObject.champKey)
            navigation.selectedItemId = getCorrectButton()
    }

    fun getCorrectButton() : Int {
        val role = Role()
        when (summonerRapidAccessObject.role) {
            role.TOP -> return R.id.action_top
            role.MID -> return R.id.action_mid
            role.JUNGLE -> return R.id.action_jungle
            role.SUP -> return R.id.action_support
            role.ADC -> return R.id.action_adc
        }
        summonerRapidAccessObject.updateRole(role.TOP)
        throw IllegalStateException("Incorrect role set")
    }

    private fun setUpClickHandlers() {
        champFab.setOnClickListener { searchPresenter.handleSearchFabClick() }
        navigation.setOnNavigationItemSelectedListener {
            presenter.handleChangeInRole(it.itemId)
        }

        clearSearchButton.setOnClickListener {
            searchPresenter.clearSearchText()
            refresh()
        }

        overlay.setOnClickListener {
            searchPresenter.closeSearchView()
        }
    }

    private fun setUpTabs() {
        tabAdapter = TabAdapter(supportFragmentManager)
        viewPager.adapter = tabAdapter
        tabs.setupWithViewPager(viewPager)
    }

    override fun showMessage(message: String) {
        Snackbar.make(base_root,message, Snackbar.LENGTH_SHORT).show()
    }

    override fun setCorrectTabFragment(tab: Int) {
        summonerRapidAccessObject.updateRole(tab)

        refresh()
    }

    override fun showOverlay() {
        overlay!!.visibility = View.VISIBLE
    }

    override fun hideOverlay() {
        overlay!!.visibility = View.GONE
    }

    override fun setChampFabIconAsACross() {
         champFab.setImageResource(R.drawable.close_filter)
    }

    override fun setChampFabIconAsNone() {
        champFab.setImageResource(R.drawable.filter_with_padding)
    }

    override fun setChampFabIconAsSelectedChamp(champIconUrl: String) {
        Picasso.get().load(champIconUrl).into(champFab)
    }

    override fun showChampList() {
        champSearchList!!.visibility = View.VISIBLE
    }

    override fun hideChampList() {
        champSearchList!!.visibility = View.GONE
    }

    override fun showSearchBar() {
        searchBox!!.visibility = View.VISIBLE
    }

    override fun hideSearchBar() {
        searchBox!!.visibility = View.GONE
    }

    override fun setChampList(champs: ArrayList<Champ>) {
        searchListAdapter.setData(champs)
        val horizontalLayoutManager = LinearLayoutManager(this)
        horizontalLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        champSearchList.layoutManager = horizontalLayoutManager
        champSearchList.adapter = searchListAdapter
    }

    override fun ensureKeyboardIsHidden() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun clearSearchField() {

        if (searchInput!!.text != null) {
            searchInput!!.text.clear()
        }

        searchListAdapter.clearFilter()
    }

    override fun showClearSearchTextButton() {
        clearSearchButton!!.visibility = View.VISIBLE
    }

    override fun hideClearSearchTextButton() {
        clearSearchButton!!.visibility = View.INVISIBLE
    }

    override fun filter(searchText: String) {
        searchListAdapter.filter(searchText)
    }


    /**
     * Required for our text watcher for the search. Do not remove
     * @param charSequence
     * @param i
     * @param i1
     * @param i2
     */
    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

    /**
     * Handle a text input into our search box. Update on each key
     * @param charSequence
     * @param i
     * @param i1
     * @param i2
     */
    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        searchPresenter.searchForChamp(charSequence.toString())
    }

    override fun afterTextChanged(editable: Editable) {

    }

    /**
     * Triggers an update to the view, as our champ has been updated. This means that we need to
     * refresh and update our data
     */
    override fun refresh() {
        runOnUiThread {
            var index = 0
            while (index < tabAdapter!!.count) {
                val fragment = tabAdapter!!.getItem(index) as TabContract.View
                fragment.refresh()
                index += 1
            }

            presenter.updateWinRate()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId) {
            R.id.changeSummoner -> {
                // clear current summoner from preferences
                val detailsIntent = Intent(this, LoginView::class.java)
                summonerRapidAccessObject.clearData()
                startActivity(detailsIntent)
                overridePendingTransition( R.anim.slide_out_exit, R.anim.slide_in_exit)
            }

            R.id.refresh -> {
                presenter.handleRefresh()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun setRefreshButtonEnabled(enabled : Boolean) {
        val view = toolbar.findViewById<View>(R.id.refresh)
        view.isEnabled = enabled
    }

    fun setRefreshAnimating(animate : Boolean) {
        val refreshButton =  toolbar.findViewById<View>(R.id.refresh)
        if (animate) {
           refreshButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_indef))
        } else {
            if (refreshButton.animation != null) {
                refreshButton.animation.cancel()
            }
        }
    }

    fun logAnalyticsEvent(eventId : String, eventName : String ) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, eventId)
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, eventName)
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
    }

    fun updateWinRateChart(winRate: HeadToHeadStat) {
        winsAndLossesChart.data = winRate.produceBarData(this)
        winsAndLossesChart.setDefaultStyle()
        val formatter = DefaultValueFormatter(0)
        winsAndLossesChart.data.setValueFormatter(formatter)
    }

    fun setSummonerName(name: String) {
        user_name.text = name
    }

    fun setJungleRoleIcon() {
        avatar.setImageResource(R.drawable.jungle_with_padding)
    }

    fun setTopRoleIcon() {
        avatar.setImageResource(R.drawable.top_with_padding)

    }

    fun setMidRoleIcon() {
        avatar.setImageResource(R.drawable.mid_with_padding)
    }

    fun setADCRoleIcon() {
        avatar.setImageResource(R.drawable.adc_with_padding)

    }

    fun setSupportRoleIcon() {
        avatar.setImageResource(R.drawable.support_with_padding)

    }
}
