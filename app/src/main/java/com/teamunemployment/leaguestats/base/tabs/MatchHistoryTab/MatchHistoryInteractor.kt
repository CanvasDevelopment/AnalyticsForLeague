package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab

import android.content.Context
import co.metalab.asyncawait.async
import com.google.gson.reflect.TypeToken
import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.utils.Role
import com.teamunemployment.leaguestats.data.room.MatchHistory.MatchHistoryDao
import com.teamunemployment.leaguestats.data.room.champ.ChampDao

import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.MatchHistoryCardViewContract
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchHistoryCardData
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchIdentifier
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import com.teamunemployment.leaguestats.io.service_layer.ServiceProducer
import kotlin.collections.ArrayList

/**
 * TODO refactor this class to not have hardcoded observers.
 * @author Josiah Kendall
 */
class MatchHistoryInteractor(private val context: Context,
                             private val retrofitFactory: RetrofitFactory,
                             private val network: Network,
                             private val matchHistoryDao: MatchHistoryDao,
                             private val champDao: ChampDao,
                             private val serviceProducer: ServiceProducer) {


    private val TAG = "MatchHistoryInteractor"
    private val url = network.getUrl()
    private val matchHistoryService = retrofitFactory.produceRetrofitInterface(MatchHistoryService::class.java, url)

    private val roleUtils = Role()

    /**
     * Load the match details to show on a card.
     * @param id The id of the match as per saved in our database. may or may not be the same as the Riot match id.
     * @param presenter The presenter to return the value to when we are done loading.
     */
    fun loadMatchDetails(id: Long,
                         role : Int,
                         presenter : MatchHistoryPresenter,
                         cardViewContract : MatchHistoryCardViewContract,
                         region: String,
                         summonerId : String,
                         timestamp : Long) {

        // Set up stuff
        val defaultService= serviceProducer.produceService<MatchHistoryCardData>()
         val call = matchHistoryService.fetchMatchSummary(role,id,summonerId)

        // Send request to server.
        defaultService.requestData(
                                call,
                                MatchHistoryCardData::class.java,
                {
                    // our successCallback object
                    presenter.runOnUIThread {
                        presenter.setLoadedCardData(
                            it.data,
                            cardViewContract,
                            it.data.dateTime)
                    }

                }
        )
    }

    private fun produceResult(win : Boolean) : String {
        return if (win) {
            "Win as "
        } else "Loss as "
    }

    fun loadMatches(summonerId: String,
                    role: Int,
                    startingPoint: Int,
                    presenter: MatchHistoryPresenter) {

        val defaultService = serviceProducer.produceService<ArrayList<MatchIdentifier>>()
        val call = matchHistoryService.fetchMatches(role, startingPoint,summonerId)
        defaultService.requestData(
                call,
                object : TypeToken<ArrayList<MatchIdentifier>>(){}.type,
                {
                    // Success callback
                    presenter.handleDataResponse(it)
                }
        )
    }

    fun loadMatches(region: String,
                    summonerId : String,
                    role: Int,
                    champId : Int,
                    startingPoint: Int,
                    presenter: MatchHistoryPresenter) {
        val defaultService = serviceProducer.produceService<ArrayList<MatchIdentifier>>()
           val call = matchHistoryService.fetchMatchesWithChamp(role, champId, startingPoint,summonerId)
        // request data
        defaultService.requestData(call,object : TypeToken<ArrayList<MatchIdentifier>>(){}.type,{presenter.handleDataResponse(it)})
    }

    fun produceTimestamp(timestamp: Long): String {
        val oneSecond : Long = 1000
        val oneMinute : Long = 60000
        val oneHour : Long = oneMinute * 60
        val oneDay : Long = oneHour * 24
        val oneWeek : Long = oneDay * 7

        val currentTimeMillis = System.currentTimeMillis()
        val diff : Long = currentTimeMillis - timestamp
        if (diff < oneMinute) {
            val numberOfSeconds : Int = (diff / oneSecond).toInt()
            if (numberOfSeconds > 1) {
                return "$numberOfSeconds seconds ago"
            }
            return "$numberOfSeconds second ago"
        }

        if (diff >= oneMinute && diff < oneHour) {
            // convert to minutes case
            val numberOfMinutes : Int = (diff / oneMinute).toInt()
            if (numberOfMinutes > 1) {
                return "$numberOfMinutes minutes ago"
            }
            return "$numberOfMinutes minute ago"

        }
        else if (diff <oneDay) {
            val numberOfHours : Int = (diff / oneHour).toInt()
            if (numberOfHours > 1) {
                return "$numberOfHours hours ago"
            }
            return "$numberOfHours hour ago"
        } else if (diff < oneWeek) {
            val numberOfDays : Int = (diff / oneDay).toInt()
            if (numberOfDays > 1) {
                return "$numberOfDays days ago"
            }
            return "$numberOfDays day ago"
        } else {
            val numberOfWeeks : Int = (diff / oneWeek).toInt()
            if (numberOfWeeks > 1) {
                return "$numberOfWeeks weeks ago"
            }
            return "$numberOfWeeks week ago"
        }
    }

    fun loadChampName(champId: Long,
                      cardViewContract: MatchHistoryCardViewContract,
                      summonerId : String,
                      win : Boolean) {
        async {
            val champ = await { champDao.loadChamp(champId.toString(), summonerId) }
            if (champ != null) {
                cardViewContract.setChamp(champ.title)
            }
        }
    }
}
