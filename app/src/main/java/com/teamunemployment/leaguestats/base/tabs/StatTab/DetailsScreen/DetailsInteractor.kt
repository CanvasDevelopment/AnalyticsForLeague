package com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen

import co.metalab.asyncawait.async
import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.base.tabs.StatTab.AnalysisService
import com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.model.StatDetailsDataModel
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import com.teamunemployment.leaguestats.mock.MockAnalysisServiceResponses
import com.teamunemployment.leaguestats.mock.MockHttpResponseInterceptor

/**
 * Created by Josiah Kendall
 */
class DetailsInteractor(private val retrofitFactory: RetrofitFactory,
                        private val network: Network,
                        private val summonerRapidAccessObject: SummonerRapidAccessObject) {

    fun fetchDetails(url: String, detailsPresenter: DetailsPresenter) {
        val mockResponses = MockAnalysisServiceResponses()
        val mockInterceptor = MockHttpResponseInterceptor(mockResponses.getStatDetails(), 200)
        async {
            val analysisService = retrofitFactory.produceMockRetrofitInterface(AnalysisService::class.java, mockInterceptor)
            val result : Result<StatDetailsDataModel> = await{analysisService.getStatDetails(url).execute().body()!!}

            // todo
//            result.cache()
            detailsPresenter.onDetailsLoaded(result.data)
        }.onError {
            detailsPresenter.showMessage("Offline - check you internet connection")
        }
    }

}