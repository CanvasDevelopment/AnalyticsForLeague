package com.teamunemployment.leaguestats.base.tabs.StatTab.recycler

import android.view.View
import com.teamunemployment.leaguestats.utils.produceBarData
import com.teamunemployment.leaguestats.utils.setDefaultStyle
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.base.tabs.StatTab.AnalysePresenter
import kotlinx.android.synthetic.main.three_stage_stat_view.view.*

/**
 * Created by Josiah Kendall
 */
class FullStatCardView(view: View, presenter: AnalysePresenter) : StatCardView(view, presenter)  {

    fun setGraph1(stat : HeadToHeadStat) {
        presenter.runOnUiThread{
            val barData = stat.produceBarData(presenter.getContext())
            view.earlyGameGraph.data = barData
            view.earlyGameGraph.setDefaultStyle()
        }

    }

    fun setGraph2(stat : HeadToHeadStat) {
        presenter.runOnUiThread {
            val barData = stat.produceBarData(presenter.getContext())
            view.midGameGraph.data = barData
            view.midGameGraph.setDefaultStyle()
        }
    }

    fun setGraph3(stat : HeadToHeadStat) {
        presenter.runOnUiThread {
            val barData = stat.produceBarData(presenter.getContext())
            view.lateGameGraph.data = barData
            view.lateGameGraph.setDefaultStyle()
        }
    }
}