package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards;

import com.github.mikephil.charting.charts.Chart;

/**
 * @author Josiah Kendall.
 */
public interface ChartCardViewContract {
    void AddChart(Chart chart);
}
