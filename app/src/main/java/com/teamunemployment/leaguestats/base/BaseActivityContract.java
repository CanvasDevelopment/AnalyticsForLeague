package com.teamunemployment.leaguestats.base;

import com.teamunemployment.leaguestats.PresenterContract;
import com.teamunemployment.leaguestats.ViewContract;
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat;

/**
 * @author Josiah Kendall
 */

public interface BaseActivityContract {

    interface Presenter extends PresenterContract {
        void showMessage(String message);
        void handleTabPress(int tab);
        void onWinRateLoaded(HeadToHeadStat winRate);
    }

    interface View extends ViewContract {
        void setCorrectTabFragment(int tab);
    }
}
