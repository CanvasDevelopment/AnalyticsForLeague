package com.teamunemployment.leaguestats.base.champ_menu.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by Josiah Kendall
 */

public class OffSetViewHolder  extends RecyclerView.ViewHolder{
    public OffSetViewHolder(View itemView) {
        super(itemView);
    }
}
