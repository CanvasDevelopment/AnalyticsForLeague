package com.teamunemployment.leaguestats.base.tabs.StatTab

import android.content.Intent
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics

import com.teamunemployment.leaguestats.data.model.OldChamp
import com.teamunemployment.leaguestats.base.tabs.TabContract
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.utils.Constant
import com.teamunemployment.leaguestats.utils.Constant.Companion.STAT_NAME
import com.teamunemployment.leaguestats.utils.Constant.Companion.STAT_TYPE
import com.teamunemployment.leaguestats.base.BaseActivityView
import com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.DetailsView
import com.teamunemployment.leaguestats.base.tabs.StatTab.recycler.AnalyseAdapter
import kotlinx.android.synthetic.main.base.*
import kotlinx.android.synthetic.main.tab_view_fragment.*
import kotlinx.android.synthetic.main.tab_view_fragment.view.*
import org.koin.android.ext.android.inject
import timber.log.Timber

/**
 * Created by Josiah Kendall
 */

class AnalyseTabView : Fragment(), AnalyseTabContract.View {

    private val firebaseAnalytics : FirebaseAnalytics by inject()
    private val presenter: AnalysePresenter by inject()
    private lateinit var baseActivity : BaseActivityView
    private val screenName = "StatsTab"

    var statType = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val rootView = inflater.inflate(R.layout.tab_view_fragment, container, false)
        baseActivity = activity as BaseActivityView
        val args = arguments
        val role = args!!.getInt(Constant.ROLE_KEY, -1)

        // Set our view. This starts the loading of value.
        presenter.setView(this)
        presenter.start()
        hidePlaceholder(rootView)
        return rootView
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) logAnalyticsEvent()
    }

    private fun logAnalyticsEvent() {
        firebaseAnalytics.setCurrentScreen(baseActivity, screenName, null /* class override */)
    }

    private fun hidePlaceholder(root : View) {
        root.placeholder.visibility = View.GONE
    }

    override fun setPlaceHolderVisible() {
//        errorMessageView.visibility = View.VISIBLE
    }

    override fun setPlaceHolderInvisible() {
//        errorMessageView.visibility = View.GONE
    }

    override fun setPlaceHolderString(message: String) {
//        errorMessageView.text = message
    }

    override fun setChamp(oldChamp: OldChamp) {
        presenter.setChamp(oldChamp)
        presenter.start()
    }

    override fun setRole(role: Int) {
        presenter.setRole(role)
    }



    override fun launchDetailsActivity(detailsUrl : String, win : Boolean) {

        val detailsIntent = Intent(this.context, DetailsView::class.java)
        detailsIntent.putExtra(STAT_NAME, detailsUrl)
        detailsIntent.putExtra(STAT_TYPE, statType)
        startActivity(detailsIntent)
        this.activity!!.overridePendingTransition(R.anim.slide_in_entry, R.anim.slide_out_entry)
    }

    override fun setAdapter(adapter: TabContract.TabAdapter) {
        activity!!.runOnUiThread {
            val analyseAdapter = adapter as AnalyseAdapter
            val linearLayoutManager = LinearLayoutManager(context)
            recyclerView.layoutManager = linearLayoutManager
            recyclerView.adapter = analyseAdapter
        }
    }

    override fun setLoadingVisible(visible: Boolean) {

    }

    override fun showMessage(message: String) {
        Timber.d(message)
         Snackbar.make(baseActivity.base_root, message, Snackbar.LENGTH_LONG).show()
    }

    override fun refresh() {
        presenter.start()
    }
}