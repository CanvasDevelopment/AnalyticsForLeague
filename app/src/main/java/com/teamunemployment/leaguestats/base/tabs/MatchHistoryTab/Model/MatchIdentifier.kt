package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class MatchIdentifier(val matchId: Long,
                      val role: Int,
                      val champKey: Int,
                      val timestamp: Long,
                      val summonerId: String) {

    @PrimaryKey(autoGenerate = true)
    var id : Int = 0

    override fun equals(other: Any?): Boolean {
        if (other is MatchIdentifier) {
            return matchId == other.matchId &&
                    role == other.role &&
                    timestamp == other.timestamp &&
                    summonerId == other.summonerId &&
                    id == other.id
        }

        return false
    }

    fun default() : MatchIdentifier {
        return MatchIdentifier(-1,-1,-1,-1,"-1")
    }
}