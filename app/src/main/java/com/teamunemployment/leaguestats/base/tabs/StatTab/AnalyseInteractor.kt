package com.teamunemployment.leaguestats.base.tabs.StatTab

import com.google.gson.reflect.TypeToken
import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.base.tabs.StatTab.Model.*
import com.teamunemployment.leaguestats.base.tabs.StatTab.recycler.StatCardView
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import com.teamunemployment.leaguestats.io.service_layer.ServiceProducer
import retrofit2.Call

import java.util.ArrayList

/**
 * Created by Josiah Kendall
 */

class AnalyseInteractor(private val retrofitFactory : RetrofitFactory,
                        private val network : Network,
                        private val serviceProducer : ServiceProducer) {

    private val url = network.getUrl()

    private val playerAnalysisRemoteRepo : PlayerAnalysisRemoteRepo = retrofitFactory.produceRetrofitInterface(PlayerAnalysisRemoteRepo::class.java, url)

    private val defaultService = serviceProducer.produceService<ArrayList<StatSummary>>()

    fun loadIndividualStat(url: String, viewHolder : StatCardView, presenter : AnalysePresenter) {

        val defaultService = serviceProducer.produceService<ArrayList<HeadToHeadStat>>()
        val call : Call<Result<ArrayList<HeadToHeadStat>>> = playerAnalysisRemoteRepo.fetchIndividualStat(url)

        val type = object : TypeToken<ArrayList<HeadToHeadStat>>(){}.type

        defaultService.requestData(call,type, { presenter.handleCardResult(it.data, viewHolder)})
    }

    fun loadStatList(position : Int, presenter : AnalysePresenter, region : String, summonerId: String, champId : Int) {

        val call : Call<Result<ArrayList<StatSummary>>> = playerAnalysisRemoteRepo.fetchStatList(summonerId,position,champId,20)

        val type = object : TypeToken<ArrayList<StatSummary>>(){}.type

        defaultService.requestData(call,type, {presenter.setStatList(it.data)})
    }

    fun loadStatList(role : Int, presenter : AnalysePresenter, region: String, summonerId : String) {
        val defaultService = serviceProducer.produceService<ArrayList<StatSummary>>()
        val call : Call<Result<ArrayList<StatSummary>>> = playerAnalysisRemoteRepo.fetchStatList(summonerId,role,20)

        val type = object : TypeToken<ArrayList<StatSummary>>(){}.type

        defaultService.requestData(call,type, {presenter.setStatList(it.data)})
    }
}
