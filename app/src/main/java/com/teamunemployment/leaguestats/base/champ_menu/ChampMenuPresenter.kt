package com.teamunemployment.leaguestats.base.champ_menu

import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.data.model.Champ

import java.util.ArrayList

/**
 * Created by Josiah Kendall
 *
 * This is the presenter for the search in our main view. It is broken up into its own seperate class
 * to prevent the main presenter growing too large.
 */

class ChampMenuPresenter
constructor(private val champMenuInteractor : ChampMenuInteractor,
            private val network : Network) : ChampLoadResponse {

    private var filter = ""
    private var searchButtonState = 0
    private lateinit var champMenuView: ChampMenuContract.View
    internal val champs = ArrayList<Champ>()
    internal val champsOrigin = ArrayList<Champ>()


    /**
     * Set the search view for use to present to.
     * @param champMenuView The view object that we are presenting to.
     */
    fun setSearchView(champMenuView: ChampMenuContract.View) {
        this.champMenuView = champMenuView
    }

    /**
     * Set the champ that the user clicked on.
     * @param champ The champ a user clicked on.
     */
    fun handleChampClick(champ: Champ?) {
        // Champurl is empty if we clicked on the "Clear" icon. Therefore we need to wipe the champ settings.
        champMenuInteractor.setCurrentChamp(champ)
        closeSearchView()
        champMenuView.refresh()
    }

    fun setCurrentChamp(champKey: Int) {
        champMenuInteractor.setCurrentChamp(champKey,  {
            closeSearchView()
            champMenuView.refresh()
        })

    }

    fun handleSearchFabClick() {
        if (searchButtonState == OPEN) {
            closeSearchView()
        } else {
            openSearchView()
        }
    }

    /**
     * Search for a champion
     * @param searchText
     */
    fun searchForChamp(searchText: String?) {
        if (searchText != null && searchText.isNotEmpty()) {
            champMenuView.showClearSearchTextButton()
            champMenuView.filter(searchText)
        } else {
            champMenuView.hideClearSearchTextButton()
            champMenuView.clearSearchField()
        }
    }

    private fun openSearchView() {
        champMenuView.showOverlay()
        champMenuView.setChampFabIconAsACross()
        champMenuView.showChampList()
        champMenuView.showSearchBar()
        // fetch our champs
        champMenuInteractor.getChamps(this)
        searchButtonState = OPEN
    }

    fun refreshChamps() {
        champMenuInteractor.updateChamps()
    }

    fun filterChamps(filter : String) {
        this.filter = filter
        champs.clear()
        val offSetChamp = Champ("","","","","","",0,"0L")
        champs.add(0, offSetChamp)
        champs.addAll(champsOrigin.filter {
            s -> s.title.toLowerCase().startsWith(filter.toLowerCase())
        })

    }

    fun closeSearchView() {
        champMenuView.hideOverlay()
        val champ = champMenuInteractor.getCurrentSetChamp()
        if (champ != null) {
            // todo extract and test this
            val champString = champ.title
                .replace(" ", "")
                .replace("'", "")
                .replace(".", "")
                .capitalize()
            val url = "${network.getRawUrl("")}/champion/$champString.png"
            champMenuView.setChampFabIconAsSelectedChamp(url)
        } else {
            champMenuView.setChampFabIconAsNone()
        }
        champMenuView.showChampList()
        champMenuView.showSearchBar()
        champMenuView.hideChampList()
        champMenuView.hideSearchBar()
        champMenuView.ensureKeyboardIsHidden()
        searchButtonState = ClOSED
    }

    fun setChampRequestResponse(champs : ArrayList<Champ>) {
//        val offSetChamp = Champ("","","","","","",0,0L)
//        champs.add(0, offSetChamp)
//        val champ = Champ("","","","","","",0,0L)
//        champs.add(1, champ)
        champMenuView.setChampList(champs)
    }

    fun clearSearchText() {
        champMenuView.clearSearchField()
    }

    companion object {
        private val OPEN = 1
        private val ClOSED = 0
    }

    override fun failure() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun success(champs: List<Champ>) {
        val champArray = ArrayList(champs)
        val offSetChamp = Champ("","","remove","","","",0,"0L")
        champArray.add(0, offSetChamp)
        val offSetChamp1 = Champ("","-1","","","","",0,"0L")
        champArray.add(1, offSetChamp1)

        champMenuView.setChampList(champArray)
    }

    fun getUrl(champString: String): String {
        return "${network.getRawUrl("")}/champion/$champString.png"
    }

    fun clearChamp() {
        champMenuView.setChampFabIconAsNone()
        champMenuInteractor.clearCurrentChamp()
    }

    fun clearFilter() {
        this.filter = ""
        champs.clear()
        champs.addAll(champsOrigin)
    }

    fun setChampData(champs: ArrayList<Champ>) {
        this.champs.clear()
        this.champs.addAll(champs)
        this.champsOrigin.clear()
        this.champsOrigin.addAll(champs)
    }
}
