package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen.model

class MatchDetails(val heroChampId : Int,
                   val enemyChampId : Int,
                   val gameDuration : Long,
                   val stats : ArrayList<Stat>)
