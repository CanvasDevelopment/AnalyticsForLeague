package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model

import com.github.mikephil.charting.data.BarData

class BarChartComparisonResult(val barData : BarData) {
}