package com.teamunemployment.leaguestats.base.champ_menu

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

import com.teamunemployment.leaguestats.base.champ_menu.Model.ChampSearchCardView
import com.teamunemployment.leaguestats.base.champ_menu.Model.OffSetViewHolder
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.utils.Constant
import com.teamunemployment.leaguestats.utils.Constant.ViewType.OFFSET_VIEW
import com.teamunemployment.leaguestats.data.model.Champ

import java.util.ArrayList

/**
 * Created by Josiah Kendall
 */

class ChampMenuListAdapter(private val champMenuPresenter: ChampMenuPresenter) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private var filter : String? = null

    override fun getItemViewType(position: Int): Int {

        return if (position == 0) {
            Constant.ViewType.OFFSET_VIEW
        } else Constant.ViewType.STANDARD_VIEW
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == Constant.ViewType.OFFSET_VIEW) {
            // set up offset
            val v = LayoutInflater.from(parent.context).inflate(R.layout.offset_champ_view, parent, false)
            return OffSetViewHolder(v)
        }

        val v = LayoutInflater.from(parent.context).inflate(R.layout.champ_search_card, parent, false)
        return ChampSearchCardView(v, champMenuPresenter, parent.context)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is OffSetViewHolder) {
            return
        }

        val champ = champMenuPresenter.champs[position]
        val champSearchCardView = holder as ChampSearchCardView
        if (champ.title.isEmpty()) {
            champSearchCardView.setChamp(null)
            return
        }

        if (getItemViewType(position) != OFFSET_VIEW) {
            champSearchCardView.setChamp(champ)
        }
    }

    fun setData(champs: ArrayList<Champ>) {
        champMenuPresenter.setChampData(champs)
    }

    /**
     * Apply a string filter to the list of champs. This will filter the champs in the
     * [champs] list, while leaving the [champsOrigin] untouched.
     */
    fun filter(filter : String) {
        champMenuPresenter.filterChamps(filter)
        notifyDataSetChanged()
    }


    fun clearFilter() {
        champMenuPresenter.clearFilter()
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return champMenuPresenter.champs.size
    }

    fun getChamps() : ArrayList<Champ> {
        return champMenuPresenter.champs
    }
}
