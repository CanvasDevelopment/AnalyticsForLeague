package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen.model

import android.widget.TextView
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.teamunemployment.leaguestats.utils.setDefaultStyle

/**
 * Created by Josiah Kendall
 */
class GameStageView(private val title : TextView,
                    private val chart1 : BarChart,
                    private val chart1Title : TextView,
                    private val chart2: BarChart,
                    private val chart2Title : TextView,
                    private val chart3: BarChart,
                    private val chart3Title : TextView,
                    private val chart4: BarChart,
                    private val chart4Title : TextView,
                    private val chart5: BarChart,
                    private val chart5Title : TextView,
                    private val chart6: BarChart,
                    private val chart6Title : TextView) {

    fun setCreeps(data : BarData, centerText : String, gameStageString : String) {
        chart1.data = data
        chart1Title.text = gameStageString
        chart1.setDefaultStyle()
    }

    fun setDamageDealt(data : BarData, centerText : String, gameStageString : String) {
        chart2.data = data
        chart2Title.text = gameStageString
        chart2.setDefaultStyle()
    }

    fun setDamageTaken(data : BarData, centerText : String, gameStageString : String ) {
        chart3.data = data
        chart3.setDefaultStyle()
        chart3Title.text = gameStageString
    }

    fun setXp(data : BarData, centerText : String, gameStageString : String) {
        chart4.data = data
        chart4.setDefaultStyle()
        chart4Title.text = gameStageString

    }

    fun setGold(data : BarData, centerText : String, gameStageString : String) {
        chart5.data = data
        chart5.setDefaultStyle()
        chart5Title.text = gameStageString

    }

    fun unused(data : BarData,centerText : String, gameStageString : String) {
        chart6.data = data
        chart6.setDefaultStyle()
        chart6Title.text = gameStageString

    }



    fun setViewTitle(title : String) {
        this.title.text = title
    }
}