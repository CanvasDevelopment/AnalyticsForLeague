package com.teamunemployment.leaguestats.base.tabs.StatTab.di

import com.teamunemployment.leaguestats.utils.RoleUtils
import com.teamunemployment.leaguestats.base.tabs.StatTab.recycler.AnalyseAdapter
import com.teamunemployment.leaguestats.base.tabs.StatTab.AnalyseInteractor
import com.teamunemployment.leaguestats.base.tabs.StatTab.AnalysePresenter
import org.koin.android.module.AndroidModule

/**
 * @author Josiah Kendall
 */
class StatModule : AndroidModule() {
    override fun context() = applicationContext {

        context(name = "StatModule") {
            provide { AnalysePresenter(get(),get(), get(), get(), get()) }
            provide { RoleUtils() }
            provide { AnalyseInteractor(get(), get(), get()) }
            provide { AnalyseAdapter(get()) }
        }
    }
}