package com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.utils.*
import com.teamunemployment.leaguestats.utils.Constant.Companion.STAT_NAME
import com.teamunemployment.leaguestats.utils.Constant.Companion.STAT_TYPE
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.tabs.StatDetailsTabAdapter
import kotlinx.android.synthetic.main.stat_details_view.*
import org.koin.android.ext.android.inject

/**
 * Created by Josiah Kendall
 */
class DetailsView : AppCompatActivity() {

    val presenter by inject<DetailsPresenter>()
    private val firebaseAnalytics by inject<FirebaseAnalytics>()
    private var tabAdapter: StatDetailsTabAdapter? = null
    private val screenName = "STAT_DETAILS"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.stat_details_view)
        presenter.setView(this)
        firebaseAnalytics.setCurrentScreen(this, screenName, null)
        val statName = intent.getStringExtra(STAT_NAME)
        val statType = intent.getIntExtra(STAT_TYPE, 0)
        setUpTabs(statName.split(" ")[0], statType)
        statDetailsTitle.text = statName

    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.slide_out_exit, R.anim.slide_in_exit)
    }

    /**
     * Set the "historical" line chart with
     */
    fun setHistoricalCharts() {

    }

    private fun setUpTabs(statName : String, statType : Int) {
        tabAdapter = StatDetailsTabAdapter(supportFragmentManager)
        tabAdapter!!.statName = statName
        tabAdapter!!.statType = statType
        statDetailsViewPager.adapter = tabAdapter
        statDetailsViewPager.offscreenPageLimit = 2 // we want all to load at once, its not much data.
        statDetailsTabs.setupWithViewPager(statDetailsViewPager)
    }

    /**
     * Set the opponent game stage comparison charts
     * @param stats An Array list of the [HeadToHeadStat] objects for each game stage
     */
    fun setVsOpponentGameStageCharts(stats : ArrayList<HeadToHeadStat>) {
        val earlyGameData = stats.earlyGame().producePieChartData(this)
        val midGameData = stats.midGame().producePieChartData(this)
        val lateGameData = stats.lateGame().producePieChartData(this)

//        vsOpponentStats.setGraphs(earlyGameData, midGameData, lateGameData)
    }

    /**
     * Set the division game stage comparison charts
     * @param stats An Array list of the [HeadToHeadStat] objects for each game stage
     */
    fun setVsDivisionGameStageCharts(stats: ArrayList<HeadToHeadStat>) {
        val earlyGameData = stats.earlyGame().producePieChartData(this)
        val midGameData = stats.midGame().producePieChartData(this)
        val lateGameData = stats.lateGame().producePieChartData(this)

//        vsDivisionStats.setGraphs(earlyGameData, midGameData, lateGameData)
    }


    /**
     * Show a message to the user in the form of a [Snackbar]
     *
     * @param message The message to display
     */
    fun showMessage(message : String) {
        Snackbar.make(stat_details_root, message, Snackbar.LENGTH_LONG)
    }
}