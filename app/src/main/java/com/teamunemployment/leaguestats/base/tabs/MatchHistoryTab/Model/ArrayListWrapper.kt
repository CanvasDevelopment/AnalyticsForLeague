package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model

/**
 * @author Josiah Kendall
 */
class ArrayListWrapper<T>(val items : ArrayList<T>)