package com.teamunemployment.leaguestats.base.tabs.StatTab.recycler

import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat

/**
 * Created by Josiah Kendall
 */
interface StatCardContract {
    fun onStatLoaded(stats : ArrayList<HeadToHeadStat>)
    fun setTitle(title : String)
}