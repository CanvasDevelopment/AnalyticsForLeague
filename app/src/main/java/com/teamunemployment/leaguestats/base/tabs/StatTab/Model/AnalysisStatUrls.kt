package com.teamunemployment.leaguestats.base.tabs.StatTab.Model

/**
 * Created by Josiah Kendall
 */
class AnalysisStatUrls(val card : String,
                       val detail : String,
                       val title: String)