package com.teamunemployment.leaguestats.base.tabs.StatTab

import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.base.tabs.StatTab.Model.StatSummary
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Josiah Kendall
 */
interface PlayerAnalysisRemoteRepo {

    @GET("analysis/v1/getStatList/{summonerId}/{position}/{champ}/{games}")
    fun fetchStatList(@Path("summonerId")summonerId : String,
                      @Path("position") position : Int,
                      @Path("champ") champ: Int,
                      @Path("games")games : Int) : Call<Result<ArrayList<StatSummary>>>

    @GET("analysis/v1/getStatList/{summonerId}/{position}/{games}")
    fun fetchStatList(@Path("summonerId")summonerId : String,
                      @Path("position") position : Int,
                      @Path("games")games : Int) : Call<Result<ArrayList<StatSummary>>>

    @GET("analysis/v1/{url}")
    fun fetchIndividualStat(@Path("url", encoded = true) url: String) : Call<Result<ArrayList<HeadToHeadStat>>>
}