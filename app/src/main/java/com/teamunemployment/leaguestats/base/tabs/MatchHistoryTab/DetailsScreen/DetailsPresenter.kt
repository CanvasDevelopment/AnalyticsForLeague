package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen

import android.content.Context
import android.view.ViewGroup
import com.teamunemployment.leaguestats.utils.*
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen.model.GameStageView
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen.model.MatchDetails

/**
 * @author Josiah Kendall
 */
class DetailsPresenter(private val detailsInteractor: DetailsInteractor,
                       private val context : Context,
                       private val viewProducer: ViewProducer,
                       private val network: Network,
                       private val adapter : MatchDetailsStatListAdapter) {

    private val EARLY_GAME = 0
    private val MID_GAME = 1
    private val LATE_GAME = 2

    private val detailsErrorMessages = DetailsErrorMessages()
    private lateinit var view : DetailsView

    fun setView(view : DetailsView) {
        this.view = view
    }

    /**
     * Trigger the loading of the data, which will be presented to the view once it is ready
     * @param matchId       The unique id of the match that we are fetching the data for.
     * @param summonerId    The unique Id of the summoner who is the hero in this match
     * @param role          The role of the summoner that we are interested in.
     */
    fun start(detailsUrl : String) {
        // process the image and get the ro
        val params = detailsUrl.split("/")
        view.setRole(params[0].toInt())
        val matchId = params[1]
        val summonerId = params[2]
        detailsInteractor.setPresenter(this)
        detailsInteractor.fetchDetailsForMatch(detailsUrl, this)
        detailsInteractor.fetchChampInfo(matchId, summonerId)
    }

    /**
     * Handle the response from the server.
     *
     * @param code                  This is the response code from the server. If this is not a 200, we fetch the
     *                              appropriate error message and display it to the user.
     * @param result                This is the actual data. If the [code] is a 200, then we should have some juicy
     *                              data that we can display about the user and their performance in this match
     *                              through a variety of pie charts
     * @param headToHeadPerformance The calculated performance stat [HeadToHeadStat] for each the
     *                              hero and the villain
     */
    fun handleDetailsResponse(code : Int, result : MatchDetails) {
        when(code) {
            200 -> handleHealthyResponse(result)
            404 -> view.showMessage(detailsErrorMessages.`404`())
            500 -> view.showMessage(detailsErrorMessages.`500`())
        }
    }

    /**
     * Handle a 200 response. This method pulls apart the data and applies the correct data to the
     * correct charts, using the extension method [producePieChartData] to create the pie chart data.
     * @param result The data that we need to display on screen.
     */
    private fun handleHealthyResponse(result: MatchDetails) {
        // set presenter here
        adapter.clearData()
        view.initialiseAdapter(adapter)

        view.setGameDuration(produceGameDurationString(result.gameDuration))
        adapter.addData(result.stats)
        adapter.notifyDataSetChanged()
    }

    // todo tidy and test this.
    private fun produceGameDurationString(duration : Long) : String {

        val durationSimple = round(duration.toFloat() / 60.toFloat(), 2)
        val minsandSeconds = durationSimple.toString().split(".")
        val seconds = round(minsandSeconds[1]
                                    .toFloat() * 0.6f, 0)

        if (seconds < 10) {
            return "${minsandSeconds[0]}m ${seconds.toString().subSequence(0,1)}s"
        }

        return "${minsandSeconds[0]}m ${seconds.toString().subSequence(0,2)}s"
    }

    private fun round(value: Float, precision: Int): Double {
        val scale = Math.pow(10.0, precision.toDouble()).toInt()
        return Math.round(value * scale).toDouble() / scale
    }

    /**
     * Create the game stage view that we want. This returns a gameStageView object that we can use to
     * set data to the actual view.
     * @param gameStage An int reflecting the game stage that we want. Must be [EARLY_GAME], [MID_GAME]
     *                  or [LATE_GAME].
     * @param parent    The parent that we want to add this view too.
     */
    fun produceGameStageView(gameStage : Int, parent : ViewGroup): GameStageView {
        val gameStageView = viewProducer.produceGameStageView(parent)

        when(gameStage) {
            EARLY_GAME -> gameStageView.setViewTitle("Early Game")
            MID_GAME-> gameStageView.setViewTitle("Mid Game")
            LATE_GAME-> gameStageView.setViewTitle("Late Game")
        }

        return gameStageView
    }

//    fun setChamps(champId: Long, enemyChampId: Long) {
//        network.getRawUrl()
//        view.setHeroChamp(champId)
//        view.setEnemyChampId(enemyChampId)
//    }

    fun setHeroChamp(title: String) {

        val champString = title
                .replace(" ", "")
                .replace("'", "")
                .replace(".", "")

        view.setHeroChamp("${network.getRawUrl("")}/champion/$champString.png")
    }

    fun setEnemyChamp(title : String) {
        val champString = title
                .replace(" ", "")
                .replace("'", "")
                .replace(".", "")
        view.setEnemyChamp("${network.getRawUrl("")}/champion/$champString.png")
    }

    fun showMessage(message : String) {
        view.showMessage(message)
    }

    fun setResultString(won: Boolean, title: String) {
        val resultsString = if (won) {
            "Win as $title"
        } else {
            "Loss as $title"
        }


        view.setToolbar(resultsString)
    }

    fun initiateToolbar(enemyChamp: Int, heroChamp: Int, role: Int) {
        view.setRole(role)
    }

}