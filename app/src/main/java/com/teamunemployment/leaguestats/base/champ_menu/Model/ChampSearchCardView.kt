package com.teamunemployment.leaguestats.base.champ_menu.Model

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.View

import com.squareup.picasso.Picasso
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.data.model.Champ
import com.teamunemployment.leaguestats.base.champ_menu.ChampMenuPresenter
import kotlinx.android.synthetic.main.champ_search_card.view.*

/**
 * Created by Josiah Kendall
 */

class ChampSearchCardView(private val view: View,
                          private val champMenuPresenter: ChampMenuPresenter,
                          private val context: Context) : RecyclerView.ViewHolder(view), View.OnClickListener {
    private var champ: Champ? = null

    init {
        view.setOnClickListener(this)
    }

    /**
     * Set the champ that this card/item relates to.
     * @param champ
     */
    fun setChamp(champ: Champ?) {
        this.champ = champ

        val picasso = Picasso.get()
        picasso.setIndicatorsEnabled(true)
        picasso.isLoggingEnabled = true

        if (champ == null) {
            view.champImage.setBackgroundResource(R.drawable.close_filter_padded)
            return
        }

        // does not show
        val champString = champ.title
                .replace(" ", "")
                .replace("'", "")
                .replace(".", "")
                .capitalize()
        picasso.load(champMenuPresenter.getUrl(champString))
                .placeholder(R.drawable.ic_person_white_24dp)
                .tag("PICASSO")
                .into(view.champImage)
    }

    override fun onClick(view: View) {
        //champMenuPresenter.
        // if placeholder

        champMenuPresenter.handleChampClick(champ)
    }
}
