package com.teamunemployment.leaguestats.base.charting.bar

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler

/**
 * A smart formatter for our stat values. If the value is over 1000 it abbreviates it (e.g 4200 becomes 4.2k)
 * IF it is less than 1000, it uses the default [DefaultValueFormatter.getFormattedValue]
 *
 */
class SmartFormatter(decimal : Int) : DefaultValueFormatter(decimal) {

    override fun getFormattedValue(value: Float, entry: Entry?, dataSetIndex : Int, viewPortHandler: ViewPortHandler?): String {
        if (value > 1000) {
            return "${round((value / 1000F),1)}k"
        }

        return super.getFormattedValue(value, entry, dataSetIndex, viewPortHandler)
    }

    fun round(value: Float, precision: Int): Double {
        val scale = Math.pow(10.0, precision.toDouble()).toInt()
        return Math.round(value * scale).toDouble() / scale
    }
}