package com.teamunemployment.leaguestats.base.champ_menu;

import com.teamunemployment.leaguestats.data.model.Champ;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Created by Josiah Kendall
 */

public interface ChampMenuContract {

    interface View {
        void showOverlay();
        void hideOverlay();
        void setChampFabIconAsACross();
        void setChampFabIconAsNone();
        void setChampFabIconAsSelectedChamp(String champIconUrl);
        void showChampList();
        void hideChampList();
        void showSearchBar();
        void hideSearchBar();
        void setChampList(ArrayList<Champ> champs);
        void ensureKeyboardIsHidden();
        void clearSearchField();
        void showClearSearchTextButton();
        void hideClearSearchTextButton();

        void filter(@NotNull String searchText);

        void refresh();
    }
}
