package com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.tabs


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class StatDetailsTabAdapter(val fm: FragmentManager)  : FragmentPagerAdapter(fm) {

    private var currentTab: StatDetailsTab? = null
    var statName = ""
    var statType = 0

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        val resultFragment  = StatDetailsTab()
        resultFragment.statType = statType
        when (position) {
            0 -> resultFragment.gameStage = "earlyGame" // + current stat
            1 -> resultFragment.gameStage = "midGame" // + current stat
            2 -> resultFragment.gameStage = "lateGame" // + current stat
        }

        // set up statType todo
        // just return a fragment
        currentTab = resultFragment

        return resultFragment
    }

    override fun getCount(): Int {
        // Show 3 total pages.
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "Early Game"
            1 -> return "Mid Game"
            2 -> return "Late Game"
        }
        return null
    }


}