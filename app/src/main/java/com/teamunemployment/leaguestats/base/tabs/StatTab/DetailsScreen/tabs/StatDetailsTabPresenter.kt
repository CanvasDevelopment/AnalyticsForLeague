package com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.tabs

import android.graphics.Color
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.teamunemployment.leaguestats.utils.Role
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat

class StatDetailsTabPresenter(private val summonerRapidAccessObject: SummonerRapidAccessObject) {

    private lateinit var view : StatDetailsTab
    private val roleHelper = Role()

    fun setView(view : StatDetailsTab) {
        this.view = view
    }

    fun start(statInteractor : StatDetailsTabInteractor, view : StatDetailsTab) {
        val champkey = summonerRapidAccessObject.champKey
        val summonerId = summonerRapidAccessObject.summonerId
        val role = summonerRapidAccessObject.role

        if (summonerRapidAccessObject.champKey != -1) {
//            statInteractor.loadStats(summonerId,50, roleHelper.produceSingleLaneStringForRoleInt(role), view, champkey,this)
        } else {
            statInteractor.loadStats(summonerId,50, role, view, this)
        }
    }

    /**
     * Handle the raw line data that comes back from the server. Currently only creates a hero line,
     * but could be easily used to create a enemy line too.
     *
     * @param rawData The raw history data of the stat that we are fetching. This comes as a [HeadToHeadStat]
     *                with both the hero and enemy data.
     *
     */
    fun handleRawLineData(rawData: ArrayList<ArrayList<Float>>, tab : StatDetailsTab) {
        // now I need to create a line of hero rawData
        val entries = ArrayList<Entry>()
        var count = 0f

        // Create a list of entries based off of our raw hero data
        for (dataObject in produceAverageFromRawData(rawData)) {
            entries.add(Entry(count, dataObject))
            count += 1
        }

        val lineDataSet = LineDataSet(entries,  "")
        lineDataSet.valueTextColor = Color.WHITE
//        lineDataSet.setDrawCircles(facblse)
        lineDataSet.color = Color.WHITE
        lineDataSet.setDrawValues(false)
        lineDataSet.setDrawCircles(false)

        tab.setLineChart(LineData(lineDataSet))
    }


    /**
     * Produce an average of head to head stats from a list.
     *
     * @return a list of [HeadToHeadStat], that contain the averages for the enemy and the hero
     */
    fun produceAverageFromRawData(rawData: ArrayList<ArrayList<Float>>) : ArrayList<Float> {
        // This is the array we calculate the moving average from
        val movingAverage = ArrayList<Float>()

        // This is the array of averages that we will return.
        val averageArray = ArrayList<Float>()

        for (stat in rawData[0]) {
            // if the list is 10, push one off the start.
            if (movingAverage.size == 10) {
                movingAverage.removeAt(0)
            }

            // add our stat to the moving average
            if (stat > 0) {
                movingAverage.add(stat)
            }
            averageArray.add(movingAverage.avg())
        }

        return averageArray
    }


    fun ArrayList<HeadToHeadStat>.avg() : HeadToHeadStat {
        var enemyTotal = 0f
        var heroTotal = 0f
        forEach {
            enemyTotal += it.enemyStatValue
            heroTotal += it.heroStatValue
        }

        return HeadToHeadStat(enemyTotal/size, heroTotal / size)
    }

    fun ArrayList<Float>.avg() : Float {
        var total = 0f
        forEach {
            total += it
        }

        return total / size
    }

    fun handleAverage(avg : Float) {
        view.setAvg(avg)
    }

    fun handleMinimum(min : Float) {
        view.setMin(min)
    }

    fun handleMaximum(max : Float) {
        view.setMax(max)
    }

    fun showMessage(message : String) {
        view.showMessage(message)
    }

}