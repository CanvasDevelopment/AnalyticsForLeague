package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen.model.Stat

class MatchDetailsStatListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val stats = ArrayList<Stat>()

    fun addData(data : ArrayList<Stat>) {
        stats.addAll(data)
    }

    fun clearData() {
        stats.clear()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.stat_breakdown_card, parent, false)
        return MatchDetailsStatCard(v, parent.context)
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
    }

    override fun getItemCount(): Int {
        return stats.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        // grab out the data
        // set the early game, mid, late and overall. And set our title
        val card = holder as MatchDetailsStatCard
        // get stat
        val stat = stats[position]

        card.setTitle(stat.title.toLowerCase().capitalize())
        card.setOverall(stat.headToHeadStats.accumulate(), "OVERALL")
        card.setEarlyGame(stat.headToHeadStats[0], "EARLY GAME")
        card.setMidGame(stat.headToHeadStats[1], "MID GAME")
        card.setLateGame(stat.headToHeadStats.subList(2,4).accumulate(), "LATE GAME")

        // load stat
    }

    private fun List<HeadToHeadStat>.accumulate() : HeadToHeadStat{
        var enemy = 0f
        var hero = 0f
        for (h2h in this) {
            enemy += h2h.enemyStatValue
            hero += h2h.heroStatValue
        }

        return HeadToHeadStat(enemy,hero)
    }


}