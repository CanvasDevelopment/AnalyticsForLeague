package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.teamunemployment.leaguestats.data.HeadToHeadStatConverter
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
/**
 * @author Josiah Kendall§
 */
@Entity
class MatchHistoryCardData(@PrimaryKey(autoGenerate = false) val matchId: Long,
                           val champId: Long,
                           val enemyChampId: Long,
                           @TypeConverters(HeadToHeadStatConverter::class) val earlyGame: HeadToHeadStat,
                           @TypeConverters(HeadToHeadStatConverter::class) val midGame: HeadToHeadStat,
                           @TypeConverters(HeadToHeadStatConverter::class) val lateGame: HeadToHeadStat,
                           val won : Boolean,
                           val detailsUrl : String,
                           val summonerId : String,
                           val heroChampName : String,
                           val dateTime : Long) {

    override fun equals(other: Any?): Boolean {
        if (other is MatchHistoryCardData) {
            if (other.matchId == matchId &&
                    other.champId == champId &&
                    other.enemyChampId == enemyChampId &&
                    other.earlyGame == earlyGame &&
                    other.midGame == midGame &&
                    other.lateGame == lateGame &&
                    other.won == won &&
                    other.detailsUrl == detailsUrl &&
                    other.summonerId == summonerId) {
                return true
            }
        }
        return false
    }
}
