package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab

import android.util.Log
import co.metalab.asyncawait.async
import com.github.mikephil.charting.data.PieDataSet

import com.teamunemployment.leaguestats.data.model.MatchSummary
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.MatchHistoryCardViewContract
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchHistoryCardData
import com.teamunemployment.leaguestats.base.tabs.TabContract

import java.util.ArrayList

import com.github.mikephil.charting.data.PieEntry
import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.utils.Role
import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.data.room.champ.SimpleChampDao
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchIdentifier
import com.teamunemployment.leaguestats.io.service_layer.ServicePresenter
import com.teamunemployment.leaguestats.utils.champ.ChampPresenter
import timber.log.Timber


/**
 * @author Josiah Kendall.
 */
class MatchHistoryPresenter
constructor(private val matchHistoryInteractor: MatchHistoryInteractor,
            private val summonerRapidAccessDataObject : SummonerRapidAccessObject,
            private val matchHistoryRecycleViewAdapter : MatchHistoryRecycleViewAdapter,
            private val champPresenter: ChampPresenter) : MatchHistoryTabContract.Presenter, ServicePresenter {

    override fun <T> handleDataResponse(result: Result<T>) {

        if (result.resultCode in 200..299) {
            // we are successfull
            if (result.data is ArrayList<*>) {
                onMatchListLoadedSuccessfully(result.data as ArrayList<MatchIdentifier>, 200)
            } else if (result.data is MatchHistoryCardData) {
//                setLoadedCardData(result.data,)
            }
        }
    }

    override fun showMessage(message: String) {
        view.showMessage(message)
    }

    private lateinit var view: TabContract.View

    override fun start() {
        matchHistoryRecycleViewAdapter.matchHistoryPresenter = this
        matchHistoryRecycleViewAdapter.clearData()
        view.setAdapter(matchHistoryRecycleViewAdapter)

        if (summonerRapidAccessDataObject.champKey == -1) {
            Timber.d("Loading with no champ and role : ${summonerRapidAccessDataObject.role}")
            loadDataForRole(summonerRapidAccessDataObject.role, summonerRapidAccessDataObject.summonerId)
        } else {
            Timber.d("Loading with champ: ${summonerRapidAccessDataObject.champKey} and role : ${summonerRapidAccessDataObject.role}")
            loadDataForRole(
                    summonerRapidAccessDataObject.role,
                    summonerRapidAccessDataObject.summonerId,
                    summonerRapidAccessDataObject.champKey)
        }
    }

    override fun handleError(e: Throwable) {
        Log.d("MatchHistory", "Error: " + e.message)
    }

    override fun restart() {

    }

    override fun resume() {

    }

    override fun pause() {

    }

    override fun stop() {

    }

    override fun destroy() {

    }

    override fun loadDataForRole(role: Int, summonerId : String) {
        matchHistoryInteractor.loadMatches(summonerId, role, matchHistoryRecycleViewAdapter.itemCount, this)
    }

    fun loadDataForRole(role : Int, summonerId : String, champKey : Int) {
        matchHistoryInteractor.loadMatches(
                "oce",
                summonerId,
                role,
                champKey,
                matchHistoryRecycleViewAdapter.itemCount/*default*/,
                this)
    }

    override fun loadMatchSummary(matchId: Long) {

    }

    override fun setView(view: TabContract.View) {
        this.view = view
    }

    override fun onMatchSummaryLoadedSuccessfully(matchSummary: MatchSummary) {

    }

    override fun onError(e: Throwable) {
        //        Log.e("MatchHistory", "Error: " + e.getMessage());
        view.showMessage("An error occurred. Please try again.")
    }

    override fun onMatchListLoadedSuccessfully(result: ArrayList<MatchIdentifier>, code : Int) {
        if (code in 200..299) {
            matchHistoryRecycleViewAdapter.addData(result)
            // todo sort this out - either use match history tab or the view.
            val matchHistoryView = view as MatchHistoryTabView
            if (matchHistoryRecycleViewAdapter.itemCount == 0) {
                matchHistoryView.showPlaceholder()
            } else {
                matchHistoryView.hidePlaceholder()
            }
        } else {
            view.showMessage("Whoops, something went wrong :(")
        }
    }

    // todo remove this
    @Deprecated("use SummonerRapidAccessObject class now")
    override fun setRole(role: Int) {
//        this.role = role
    }

    /**
     * Load the cardUrl value.
     * @param id The match id for the value we want to load.
     * @param cardViewContract The cardUrl object to present the value back to.
     */
    override fun loadCardData(matchIdentifier : MatchIdentifier, cardViewContract : MatchHistoryCardViewContract, region : String) {
        // if we get -1 here we should probably remove this card
        matchHistoryInteractor.loadMatchDetails(matchIdentifier.matchId, matchIdentifier.role, this, cardViewContract, region,summonerRapidAccessDataObject.summonerId, matchIdentifier.timestamp )
    }

    fun loadMoreDataForRole(listSize : Int) {
        // Check if we have a whole number, so that we can know that we still have more data to fetch
        if((listSize.toFloat()/20) == (listSize / 20).toFloat()){
            if (summonerRapidAccessDataObject.champKey == -1) {
                matchHistoryInteractor.loadMatches(summonerRapidAccessDataObject.summonerId,
                        summonerRapidAccessDataObject.role,
                        listSize,
                        this)
            } else {
                matchHistoryInteractor.loadMatches(summonerRapidAccessDataObject.getRegion(),
                        summonerRapidAccessDataObject.summonerId,
                        summonerRapidAccessDataObject.role,
                        summonerRapidAccessDataObject.champKey,
                        listSize,
                        this)
            }
        }
        // if list size is less than a multiple of 20, don't load anything

    }

    /**
     * todo tests for this now we have removed UI thread calls.
     * Set our loaded cardUrl value back to the presenter for presenting to the cardUrl.
     * =====================
     * Note the cardViewContract being handed around. That is because we want to
     * maintain the same request/callback pattern that we use everywhere else for ease.
     * The reason we have to keep the instance is that we cannot reliably maintain a reference to that cardUrl
     * object any other way. The other option is to take the observables out of the interactor and process
     * them here in the presenter. I feel like this is the lesser of two evils.
     * @param matchHistoryCardData The cardUrl value to present.
     * @param cardViewContract The cardUrl view object to present to.
     */
    override fun setLoadedCardData(matchHistoryCardData: MatchHistoryCardData,
                                   cardViewContract: MatchHistoryCardViewContract,
                                   timestamp: Long) {
        val timestampString = matchHistoryInteractor.produceTimestamp(timestamp)


        val enemyStat = matchHistoryCardData.earlyGame.enemyStatValue +
                matchHistoryCardData.midGame.enemyStatValue +
                matchHistoryCardData.lateGame.enemyStatValue
        val heroStat= matchHistoryCardData.earlyGame.heroStatValue +
                matchHistoryCardData.midGame.heroStatValue+
                matchHistoryCardData.lateGame.heroStatValue

        val summaryPerformance = HeadToHeadStat(enemyStat, heroStat)

        // Set our result
        if (matchHistoryCardData.won) cardViewContract.setVictory() else cardViewContract.setDefeat()

        cardViewContract.setTimeStamp(timestampString)

        cardViewContract.setGraph1(matchHistoryCardData.earlyGame)
        cardViewContract.setGraph2(matchHistoryCardData.midGame)
        cardViewContract.setGraph3(matchHistoryCardData.lateGame)
        cardViewContract.setDetailsUrl(matchHistoryCardData.detailsUrl)
        cardViewContract.setWin(matchHistoryCardData.won)
        cardViewContract.setSummaryChart(summaryPerformance)

        val params = matchHistoryCardData.detailsUrl.split("/")
        cardViewContract.setRole(params[0].toInt())

        // Set our champs
        champPresenter.presentChamp(
                matchHistoryCardData.champId.toInt()
        ) {cardViewContract.setHeroChampIcon(it)}

        champPresenter.presentChamp(
                matchHistoryCardData.enemyChampId.toInt()
        ) {cardViewContract.setEnemyChampIcon(it)}


    }

    fun fetchPieDataSet(HeadToHeadStat: HeadToHeadStat): PieDataSet {
        val entries = ArrayList<PieEntry>()

        entries.add(PieEntry(HeadToHeadStat.heroStatValue, "Hero"))
        entries.add(PieEntry(HeadToHeadStat.enemyStatValue, "Enemy"))

        return PieDataSet(entries, "")
    }

    fun onDetailsButtonClick(detailsUrl : String, win : Boolean) {
        // launch new view with sliding animation
        view.launchDetailsActivity(detailsUrl, win)
    }

    fun runOnUIThread(function: () -> Unit) {
        view.activity.runOnUiThread(function)
    }
}
