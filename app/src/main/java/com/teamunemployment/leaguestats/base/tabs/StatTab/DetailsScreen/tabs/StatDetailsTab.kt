package com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.tabs

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.LineData
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.DetailsView
import kotlinx.android.synthetic.main.stat_details_tab.*
import org.koin.android.ext.android.inject

class StatDetailsTab : Fragment() {

    private var rootView: View? = null

    val presenter: StatDetailsTabPresenter by inject()
    val summonerRapidAccessObject: SummonerRapidAccessObject by inject()
    val statDetailsTabInteractor : StatDetailsTabInteractor by inject()
    lateinit var baseActivity : DetailsView
    // game stage should be a combo of the stat and the gamestage
    var statType = 0
    var gameStage = "earlyGame"

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater : LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.rootView = inflater.inflate(R.layout.stat_details_tab, container, false)

        baseActivity = activity as DetailsView
        presenter.setView(this)
        presenter.start(statDetailsTabInteractor, this)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    fun setLineChart(lineData : LineData) {
        statHistory.setTouchEnabled(false)
        statHistory.axisLeft.mAxisMinimum = 1f
        statHistory.axisRight.mAxisMinimum = 1f
        statHistory.axisRight.textColor = Color.WHITE
        statHistory.axisLeft.textColor = Color.WHITE
        statHistory.axisRight.setDrawTopYLabelEntry(false)
        statHistory.xAxis.setDrawGridLines(false)
        statHistory.axisLeft.spaceBottom
        statHistory.xAxis.setDrawLabels(false)
        statHistory.axisLeft.spaceBottom = 80f
        statHistory.axisLeft.spaceTop= 80f
        statHistory.axisRight.spaceBottom = 80f
        statHistory.axisRight.spaceTop= 80f
        statHistory.axisRight.setDrawGridLines(false)
        statHistory.axisLeft.setDrawGridLines(false)
        statHistory.xAxis.textColor = Color.WHITE
        statHistory.xAxis.xOffset = lineData.xMax
        val description = Description()
        description.text = ""
        description.isEnabled = false

        statHistory.description = description
        statHistory.data = lineData
        statHistory.invalidate()
    }

    fun setMax(max : Float) {
        statMax.text = "Max: $max"
    }

    fun setMin(min : Float) {
        statMin.text = "Min: $min"
    }

    fun setAvg(avg : Float) {
        statAverage.text = "Avg: $avg"
    }

    fun showMessage(message : String) {
        baseActivity.showMessage(message)
    }
}