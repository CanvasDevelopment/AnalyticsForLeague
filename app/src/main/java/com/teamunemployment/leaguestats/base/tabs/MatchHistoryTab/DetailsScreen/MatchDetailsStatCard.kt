package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.DetailsScreen

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.teamunemployment.leaguestats.utils.produceBarData
import com.teamunemployment.leaguestats.utils.setDefaultStyle
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import kotlinx.android.synthetic.main.stat_breakdown_card.view.*
import kotlinx.android.synthetic.main.stat_with_title.view.*

class MatchDetailsStatCard(val cardBase : View,
                           val context : Context) : RecyclerView.ViewHolder(cardBase) {


    /**
     * Clear out the view, so that we have no remanants of old data in them. This is to be called
     * in the [RecyclerView.Adapter.onViewRecycled] method.
     */
    fun clear() {

    }

    fun setTitle(title : String) {
        cardBase.statTitle.text = title
    }

    fun setOverall(headToHeadStat: HeadToHeadStat, title : String) {
        // set up the stat
        val result = headToHeadStat.produceBarData(context)

        cardBase.overallStat.statChart.data = result
        cardBase.overallStat.statChart.setDefaultStyle(0)
        cardBase.overallStat.individualStatTitle.text = title
    }

    fun setEarlyGame(headToHeadStat: HeadToHeadStat, title : String) {

        val result = headToHeadStat.produceBarData(context)

        cardBase.earlyGame.statChart.data = result
        cardBase.earlyGame.statChart.setDefaultStyle(0)
        cardBase.earlyGame.individualStatTitle.text = title
    }

    fun setMidGame(headToHeadStat: HeadToHeadStat, title : String) {

        val result = headToHeadStat.produceBarData(context)

        cardBase.midGame.statChart.data = result
        cardBase.midGame.statChart.setDefaultStyle(0)
        cardBase.midGame.individualStatTitle.text = title
    }

    fun setLateGame(headToHeadStat: HeadToHeadStat, title : String) {

        val result = headToHeadStat.produceBarData(context)
        cardBase.lateGame.statChart.data = result
        cardBase.lateGame.statChart.setDefaultStyle(0)
        cardBase.lateGame.individualStatTitle.text = title
    }


}