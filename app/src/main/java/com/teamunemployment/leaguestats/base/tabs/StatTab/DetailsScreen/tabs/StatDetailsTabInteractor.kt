package com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.tabs

import co.metalab.asyncawait.async
import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.model.StatDetailsData
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory

class StatDetailsTabInteractor(val retrofitFactory: RetrofitFactory,
                               val network: Network) {
    private val region = "oce"
    private val statDetailsTabService: StatDetailsTabService = retrofitFactory.produceRetrofitInterface(StatDetailsTabService::class.java, network.getUrl())

    fun loadStats(summonerId : String,
             games : Int,
             lane : Int,
             view : StatDetailsTab,
             presenter : StatDetailsTabPresenter) {

        async {
            val result : Result<StatDetailsData> = await{ statDetailsTabService.fetchMatches(summonerId,
                    games,
                    lane,
                    view.statType,
                    view.gameStage).execute().body()!!}
            if (result.resultCode == 200) {
                view.setAvg(result.data.statAvg)
                view.setMax(result.data.statMax)
                view.setMin(result.data.statMin)
                presenter.handleRawLineData(result.data.statHistory, view)
            }
        }.onError {
            presenter.showMessage("Offline - check you internet connection")
        }
    }

    fun loadStats(summonerId : String,
                  games : Int,
                  lane : String,
                  view : StatDetailsTab,
                  champKey : Int,
                  presenter : StatDetailsTabPresenter) {

        async {
//            val result : Result<StatDetailsData> = await{statDetailsTabService.fetchMatches(summonerId,
//                    games,
//                    lane,
//                    view.statType,
//                    champKey.toString()).execute().body()!!}
//            if (result.resultCode == 200) {
//                view.setAvg(result.data.statAvg)
//                view.setMax(result.data.statMax)
//                view.setMin(result.data.statMin)
//                presenter.handleRawLineData(result.data.statHistory, view)
//            }

        }.onError {
            presenter.showMessage("Offline - check you internet connection")
        }
    }

}