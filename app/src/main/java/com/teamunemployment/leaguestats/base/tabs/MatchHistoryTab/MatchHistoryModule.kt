package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab

import org.koin.android.module.AndroidModule

/**
 * @author Josiah Kendall
 */
class MatchHistoryModule : AndroidModule() {
    override fun context() = applicationContext {

        // TODO
        context(name = "MatchHistoryModule") {
            provide { MatchHistoryPresenter(get(), get(), get(), get()) }
            provide { MatchHistoryInteractor(get(), get(), get(), get(), get(), get()) }
            provide { MatchHistoryRecycleViewAdapter(get())}
        }
    }
}