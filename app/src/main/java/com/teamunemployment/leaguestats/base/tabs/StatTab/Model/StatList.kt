package com.teamunemployment.leaguestats.base.tabs.StatTab.Model

/**
 * Created by Josiah Kendall
 */
class StatList(val stats : ArrayList<AnalysisStatUrls>,
               val totalNumberOfGames : Int)