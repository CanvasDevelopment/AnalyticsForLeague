package com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.model

import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat

/**
 * Created by Josiah Kendall
 */
data class StatDetailsDataModel(val historical : ArrayList<ArrayList<Int>>,
                                val vsOpponent : ArrayList<HeadToHeadStat>,
                                val vsDivision : ArrayList<HeadToHeadStat>)