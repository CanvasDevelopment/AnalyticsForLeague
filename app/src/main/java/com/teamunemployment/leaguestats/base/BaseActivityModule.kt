package com.teamunemployment.leaguestats.base

import com.google.firebase.analytics.FirebaseAnalytics
import com.teamunemployment.leaguestats.utils.SyncManager
import com.teamunemployment.leaguestats.data.room.AppDatabase
import org.koin.android.module.AndroidModule

/**
 * @author Josiah Kendall
 */
class BaseActivityModule : AndroidModule() {
    override fun context() = applicationContext {

        context(name = "BaseActivityModule") {
            provide { BaseActivityPresenter(get(), get()) }
            provide { BaseActivityPersistenceInteractor(get(), get(), get(), get(), get(), get()) }
            provide { SyncManager(get(), get())}
            provide { get<AppDatabase>().summonerDao() }
            provide {FirebaseAnalytics.getInstance(get())}
        }
    }
}