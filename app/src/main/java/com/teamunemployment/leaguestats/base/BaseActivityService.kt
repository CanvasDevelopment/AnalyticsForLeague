package com.teamunemployment.leaguestats.base

import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.login_page.sign_in.SummonerDetails
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author Josiah Kendall
 */
interface BaseActivityService {

    @GET("analysis/v1/fetchSummonerWinRate/{summonerId}/{position}")
    fun getWinRateForRole(@Path("summonerId") summonerId : String, @Path("position") position : Int) : Call<Result<HeadToHeadStat>>

    @GET("analysis/v1/fetchSummonerWinRate/{summonerId}/{position}/{champKey}")
    fun getWinRateForRole(@Path("summonerId") summonerId : String,
                          @Path("position") position : Int,
                          @Path("champKey") champKey : String) : Call<Result<HeadToHeadStat>>

    @GET("summoner/v1/summonerDetails/{summonerId}")
    fun fetchSummoner(@Path("summonerId") summonerId: String) : Call<Result<SummonerDetails>>

}