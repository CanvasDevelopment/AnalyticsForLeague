package com.teamunemployment.leaguestats.base.champ_menu

import co.metalab.asyncawait.async
import com.teamunemployment.leaguestats.utils.Network
import com.teamunemployment.leaguestats.data.model.Champ
import com.teamunemployment.leaguestats.data.model.SimpleChamp
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.data.remote.ChampService
import com.teamunemployment.leaguestats.data.room.champ.ChampDao
import com.teamunemployment.leaguestats.data.room.champ.SimpleChampDao
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import timber.log.Timber

/**
 * Created by Josiah Kendall
 */

class ChampMenuInteractor(private val champDao : ChampDao,
                          private val retrofitFactory: RetrofitFactory,
                          private val summonerRapidAccessObject : SummonerRapidAccessObject,
                          private val network: Network,
                          private val simpleChampDao: SimpleChampDao) {

    private var currentFilteredChamp: Champ? = null
    private val champService = retrofitFactory.produceRetrofitInterface(ChampService::class.java, network.getUrl())

    fun getChamps(champLoadResponse: ChampLoadResponse) {
        // use default service

        // fetch the champs from the database
        async {
            val localData = await { champDao.loadAllOurChamps(summonerRapidAccessObject.summonerId) }
            champLoadResponse.success(localData)
            val remoteData = await{champService.fetchAllChamps(summonerRapidAccessObject.summonerId).execute().body()!!}
            await { saveAllChamps(remoteData.data) }
            // need to do this in the onboarding stage, or even earlier
            if (localData.size != remoteData.data.size  && remoteData.data.isNotEmpty()) {
                println("Local data size is: ${localData.size} and remote data size is ${remoteData.data.size}")
                champLoadResponse.success(remoteData.data)
            }
        }.onError {
//            presenter.showMessage("Offline - check you internet connection")
            println("Champs : Error occurred fetching champs : ${it.message}")
            champLoadResponse.failure()
            // not doing anything here atm
        }
    }


    /**
     *
     * Update the champs. Note that this is all the champs, not just the ones in our match history
     */
    fun updateChamps() {
        async{
            val simpleChampData = await{champService.fetchAllChamps().execute().body()!!}
            await {
                Timber.d("Saving champlist : ${simpleChampData.data}")
                saveAllSimpleChamps(simpleChampData.data) }
        }.onError {
            // whoops
            Timber.d("Failed to download and save champs")
        }
    }

    private fun saveAllChamps(champs : ArrayList<Champ>) {
        for (champ in champs) {
            champDao.createChamp(champ)
        }
    }

    private fun saveAllSimpleChamps(champs : ArrayList<SimpleChamp>) {
        for (simpleChamp in champs) {
            champDao.createChamp(simpleChamp.toChamp(summonerRapidAccessObject.summonerId))
            simpleChampDao.createChamp(simpleChamp)
        }
    }

    fun getCurrentSetChamp(): Champ? {
        return currentFilteredChamp
    }

    fun setCurrentChamp(champ: Champ?) {
        if (champ != null) {
            summonerRapidAccessObject.updateChampId(champ.key.toInt())
        } else {
            summonerRapidAccessObject.updateChampId(-1)
        }
        this.currentFilteredChamp = champ
    }

    fun setCurrentChamp(champKey : Int, callback: () -> Unit) {
        if (champKey == -1) {
            this.currentFilteredChamp = null
        } else {
            async {
                val champ = await { champDao.loadChamp(champKey.toString(), summonerRapidAccessObject.summonerId) }
                currentFilteredChamp = champ
                callback()
            }.onError {
                // not doing anything here yet
            }
        }
    }

    fun clearCurrentChamp() {
        this.currentFilteredChamp = null
    }
}
