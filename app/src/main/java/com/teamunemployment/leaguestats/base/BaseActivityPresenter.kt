package com.teamunemployment.leaguestats.base

import android.util.Log

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.ncapdevi.fragnav.FragNavController
import com.teamunemployment.leaguestats.R
import com.teamunemployment.leaguestats.utils.Role
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat


/**
 * @author Josiah Kendall
 */

class BaseActivityPresenter
constructor(private val baseActivityPersistenceInteractor: BaseActivityPersistenceInteractor,
            private val summonerRapidAccessObject: SummonerRapidAccessObject): BaseActivityContract.Presenter {

    private lateinit var view : BaseActivityView

    private var bottomBar: AHBottomNavigation? = null

    private var backLastPressed = 0L

    fun setView(view: BaseActivityView) {
        this.view = view
    }

    override fun start() {

    }

    override fun showMessage(message: String) {
        view.showMessage(message)
    }

    override fun handleError(e: Throwable) {
        Log.e("BaseActivityPresenter", "Error : " + e.message)
    }

    override fun restart() {

    }

    override fun resume() {

    }

    override fun pause() {

    }

    override fun stop() {

    }

    override fun destroy() {

    }


    override fun handleTabPress(tab: Int) {

        val summonerId : String = "-1" // TODO
        when (tab) {
            TOP -> {
                baseActivityPersistenceInteractor.fetchWinRateForRole("-1"/*todo*/, TOP, "oce", this)
                view.setCorrectTabFragment(FragNavController.TAB1)
            }
            JUNGLE -> {
                baseActivityPersistenceInteractor.fetchWinRateForRole("-1", JUNGLE, "oce", this) // todo
                view.setCorrectTabFragment(FragNavController.TAB2)
            }
            MID -> {
                baseActivityPersistenceInteractor.fetchWinRateForRole("-1", MID, "oce", this)
                view.setCorrectTabFragment(FragNavController.TAB3)
            }
            MARKSMAN -> {
                baseActivityPersistenceInteractor.fetchWinRateForRole("-1", MARKSMAN, "oce", this)
                view.setCorrectTabFragment(FragNavController.TAB4)
            }
            SUPPORT -> {
                baseActivityPersistenceInteractor.fetchWinRateForRole("-1", SUPPORT, "oce", this)
                view.setCorrectTabFragment(FragNavController.TAB5)
            }
        }
    }

    override fun onWinRateLoaded(winRate: HeadToHeadStat) {
        view.updateWinRateChart(winRate)
    }

    fun handleChangeInRole(itemId: Int): Boolean {
        val role = Role()
        return when(itemId) {
            R.id.action_top -> {
                view.setCorrectTabFragment(role.TOP)
                view.setTopRoleIcon()
                true
            }

            R.id.action_mid -> {
                view.setCorrectTabFragment(role.MID)
                view.setMidRoleIcon()
                true
            }

            R.id.action_jungle -> {
                view.setCorrectTabFragment(role.JUNGLE)
                view.setJungleRoleIcon()
                true
            }

            R.id.action_support -> {
                view.setCorrectTabFragment(role.SUP)
                view.setSupportRoleIcon()
                true
            }

            R.id.action_adc -> {
                view.setCorrectTabFragment(role.ADC)
                view.setADCRoleIcon()
                true
            }

            else -> {
                throw IllegalStateException("Incorrect role")
            }

        }
    }

    private val role = Role()
    fun updateWinRate() {
        val champKey = summonerRapidAccessObject.champKey
        if (champKey == -1) {
            baseActivityPersistenceInteractor.fetchWinRateForRole(
                    summonerRapidAccessObject.summonerId,
                    summonerRapidAccessObject.role,
                    summonerRapidAccessObject.getRegion(),
                    this)
        } else {
            baseActivityPersistenceInteractor.fetchWinRateForRole(
                    summonerRapidAccessObject.summonerId,
                    summonerRapidAccessObject.role,
                    summonerRapidAccessObject.getRegion(),
                    champKey.toString(),
                    this)
        }
    }

    fun loadSummonerName() {
        baseActivityPersistenceInteractor.loadSummonerName(this)
    }

    fun setSummonerNameResult(name : String) {
        view.setSummonerName(name)
    }

    fun handleRefresh() {
        view.setRefreshButtonEnabled(false)
        view.setRefreshAnimating(true)
        view.showMessage("Updating user stats")
        // launch process
        baseActivityPersistenceInteractor.launchRefresh(this)
    }

    fun finishRefresh() {
        view.setRefreshAnimating(false)
        view.setRefreshButtonEnabled(true)
        view.showMessage("Update finished")
        view.refresh()
        handleTabPress(JUNGLE)
    }

    fun handleSyncError(message: String) {
        view.showMessage("An error occurred while loading")
        view.setRefreshButtonEnabled(true)
        view.setRefreshAnimating(false)
    }

    fun backPressed() {
        if (backLastPressed == 0L || backLastPressed < System.currentTimeMillis() - 2000) {
            view.showMessage(BACK_MASSAGE)
            backLastPressed = System.currentTimeMillis()
        } else {
            view.closeApp()
        }
    }


    companion object {
        const val TOP = 0
        const val JUNGLE = 1
        const val MID = 2
        const val MARKSMAN = 3
        const val SUPPORT = 4
        const val BACK_MASSAGE: String = "Press back twice to exit."
    }
}
