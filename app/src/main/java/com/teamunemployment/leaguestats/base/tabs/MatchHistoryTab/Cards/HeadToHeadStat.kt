package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards

import androidx.room.Entity

/**
 * @author Josiah Kendall
 */
@Entity
class HeadToHeadStat(val enemyStatValue : Float,
                     val heroStatValue : Float) {
    override fun equals(other: Any?): Boolean {

        if (other is HeadToHeadStat) {
            return enemyStatValue == other.enemyStatValue &&
                    heroStatValue == other.heroStatValue
        }

        return false
    }
}
