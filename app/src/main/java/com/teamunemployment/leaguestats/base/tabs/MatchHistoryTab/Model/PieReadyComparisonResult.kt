package com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model

import com.github.mikephil.charting.data.PieData

/**
 * Created by Josiah Kendall
 */
class PieReadyComparisonResult(val pieData: PieData, val centreText: String)
