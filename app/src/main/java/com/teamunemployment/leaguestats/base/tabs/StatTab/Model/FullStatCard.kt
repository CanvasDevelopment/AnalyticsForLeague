package com.teamunemployment.leaguestats.base.tabs.StatTab.Model

/**
 * Created by Josiah Kendall
 */
class FullStatCard(val heroEarlygame : Float,
                   val villanEarlyGame : Float,
                   val heroMiddleGame : Float,
                   val villanMidGame : Float,
                   val heroLateGame : Float,
                   val villanLateGame : Float)