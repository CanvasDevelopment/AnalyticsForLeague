package com.teamunemployment.leaguestats.base.tabs;

import android.app.Activity;

import com.teamunemployment.leaguestats.ViewContract;

/**
 * @author Josiah Kendall
 */

public interface TabContract {

    interface View extends ViewContract {
        void setRole(int role);
        void setAdapter(TabAdapter adapter);
        void setLoadingVisible(boolean visible);
        void launchDetailsActivity(String statSummaryUrl, Boolean win);
        void refresh();
        Activity getActivity();
    }

    /**
     * We need a setValue() method for each of the tabs, because each of the tabs have a different value
     * package delivered to them from the server. // TODO rewrite value delivery to use a wrapper and one setValue() method on the contract.
     */
    interface TabAdapter {

    }
}
