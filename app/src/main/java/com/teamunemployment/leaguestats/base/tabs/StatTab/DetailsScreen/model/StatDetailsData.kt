package com.teamunemployment.leaguestats.base.tabs.StatTab.DetailsScreen.model

class StatDetailsData(val statHistory : ArrayList<ArrayList<Float>>,
                      val statMax : Float,
                      val statMin : Float,
                      val statAvg : Float)