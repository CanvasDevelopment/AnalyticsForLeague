package com.teamunemployment.leaguestats.di

import android.preference.PreferenceManager
import com.teamunemployment.leaguestats.utils.Role
import com.teamunemployment.leaguestats.data.model.SummonerRapidAccessObject
import com.teamunemployment.leaguestats.utils.champ.ChampPresenter
import org.koin.android.module.AndroidModule

/**
 * @author Josiah Kendall
 */
class BaseModule : AndroidModule() {
    override fun context()= applicationContext {
        context(name = "BaseModule") {
            provide { PreferenceManager.getDefaultSharedPreferences(context) }

            provide { SummonerRapidAccessObject(get()) }
            provide { ChampPresenter(get())}
            provide { Role() }
        }
    }
}


