package com.teamunemployment.leaguestats.extensions

/**
 * Created by Josiah Kendall
 */
internal inline fun <reified T : Any> objectOf() = T::class.java

internal fun getId() : Long = Math.random().toLong()