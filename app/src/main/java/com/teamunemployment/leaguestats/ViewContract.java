package com.teamunemployment.leaguestats;

/**
 * @author Josiah Kendall
 */
public interface ViewContract {
    void showMessage(String message);
}

