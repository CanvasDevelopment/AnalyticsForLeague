package com.teamunemployment.leaguestats.utils.champ

import co.metalab.asyncawait.async
import com.teamunemployment.leaguestats.data.room.champ.SimpleChampDao

class ChampPresenter(private val simpleChampDao: SimpleChampDao) {

    /**
     * Present a champ to a given view. This will call a callback (which should be your view set image
     * method).
     *
     * This allows for more testability of champs as having the async database fetch is a problem.
     *
     * @param champId
     */
    fun presentChamp(champId : Int, viewCallback : (title : String) -> Unit) {
        async {
            val champ = await{ simpleChampDao.loadChamp(champId.toString())}
            if (champ != null) viewCallback(champ.title)
        }

    }

}