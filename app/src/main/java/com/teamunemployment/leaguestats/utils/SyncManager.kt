package com.teamunemployment.leaguestats.utils

import co.metalab.asyncawait.async
import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import com.teamunemployment.leaguestats.login_page.onboarding.OnboardingService
import com.teamunemployment.leaguestats.login_page.onboarding.model.SyncProgress
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber

class SyncManager(private val network : Network,
                  private val retrofitFactory: RetrofitFactory) {

    var syncRetries = 0
    private lateinit var onboardingService : OnboardingService


    public interface SyncContract {
        fun onProgressUpdate(data : SyncProgress) : Boolean
        fun onCompletion()
        fun onError(message : String)
    }

    fun requestSync(contract: SyncContract, summonerId: String) {
        async {
            val url = network.getUrl()
            // fetch summoner details
            onboardingService = retrofitFactory.produceRetrofitInterface(OnboardingService::class.java, url)

            val call: Call<Result<String>> = onboardingService.syncUserMatchList(summonerId)
            val response: Response<Result<String>> = await { call.execute() }

            val result: Result<String> = response.get()
            Timber.d("requested sync of user match list. Result : ${result.resultCode}")
            val code = result.resultCode

            if (code == 200) {
                // progress to stage 2
                await {
                    var finished = false
                    while (!finished) {
                        Thread.sleep(3000)

                        val updateSyncProgressCall: Call<Result<SyncProgress>> = onboardingService.syncProgressReport(summonerId)
                        val response: Response<Result<SyncProgress>> = updateSyncProgressCall.execute()

                        val result: Result<SyncProgress> = response.getSyncProgress()
                        Timber.d("Updating progress : Completed ${result.data.completed} / ${result.data.total}")

                        if (result.resultCode == 200) {
                            syncRetries = 0
                            // handle sync progress update
                            finished = contract.onProgressUpdate(result.data)
                            Timber.d("finished : $finished")
                            if (finished) {
                                // request processing
                                val refineProgressCall: Call<Result<String>> = onboardingService.refineStats(summonerId)
                                val response: Response<Result<String>> = refineProgressCall.execute()
                                val result: Result<String> = response.get()
                                Timber.d("Refined stats. Recieved response code: ${result.resultCode}")

                                if (result.resultCode == 200) {
                                    // launch new screen
                                    contract.onCompletion()
                                }
                            }
                        }
                    }
                }
            }

            if (code == 200) {
                contract.onCompletion()
            } else {
                contract.onError("Error : code $code")
            }
        }.onError {
            if (syncRetries < 4) {
                syncRetries += 1
                requestSync(contract, summonerId)
            } else {
                syncRetries = 0
                contract.onError("Offline - check you internet connection")
                // go back to login screen.
//                onboardingPresenter.
            }
        }
    }
}