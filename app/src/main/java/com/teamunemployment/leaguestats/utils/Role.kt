package com.teamunemployment.leaguestats.utils

import java.lang.IllegalStateException

class Role {

    companion object Roles {
        const val TOP = 0
        const val JUNGLE = 1
        const val MID = 2
        const val ADC = 3
        const val SUPPORT = 4
    }


    // ROLES
    val SOLO = "SOLO"
    val NONE = "NONE"
    val DUO_CARRY = "DUO_CARRY"
    val DUO_SUPPORT = "DUO_SUPPORT"

    // LANES
    val TOP_LANE = "TOP"
    val MID_LANE = "MID"
    val BOTTOM = "BOTTOM"
    val JUNGLE_LANE = "JUNGLE"

    // integer rep of role/lane combo
    val ADC = 3
    val SUP = 4
    val MID = 2
    val JUNGLE = 1
    val TOP = 0
    val UNSUPPORTED = -1

    fun produceCorrectRoleIntegerGivenRoleAndLane(role : String, lane :String) : Int{

        when(role) {
            SOLO -> return handleSolo(lane)
            DUO_CARRY -> return handleCarry(lane)
            DUO_SUPPORT -> return handleSupport(lane)
            NONE -> return handleJungle(lane)
        }
        return UNSUPPORTED
    }

    fun produceCorrectRoleStringForRoleInt(role : Int) : String {
        when(role) {
            TOP-> return SOLO
            MID -> return SOLO
            SUP -> return DUO_SUPPORT
            ADC -> return DUO_CARRY
            JUNGLE -> return NONE
        }

        throw IllegalStateException("$role is not a valid integer for this method. Refer to the method documentation")

    }


    /**
     * Produce a string representation of the users lane based on the role key
        ADC = 4
        SUP = 5
        MID = 3
        JUNGLE = 2
        TOP = 1
     */
    fun produceCorrectLaneStringForRoleInt(role : Int) : String {
        when(role) {
            TOP-> return TOP_LANE
            MID -> return MID_LANE
            SUP -> return BOTTOM
            ADC -> return BOTTOM
            JUNGLE -> return JUNGLE_LANE
        }
        // maybe log this as a nissue? should never happen?
        throw IllegalStateException("$role is not a valid integer for this method. Refer to the method documentation")
    }

    fun produceSingleLaneStringForRoleInt(role : Int) : String {
        when (role) {
            TOP-> return "Top"
            MID -> return "Mid"
            SUP -> return "Support"
            ADC -> return "ADC"
            JUNGLE -> return "Jungle"
        }
        throw IllegalStateException("$role is not a valid role key. Must be $TOP,$MID, $SUP,$ADC,$JUNGLE")
    }

    private fun handleSolo(lane : String) : Int{
        when (lane) {
            TOP_LANE -> return TOP
            MID_LANE -> return MID
        }
        return UNSUPPORTED
    }

    private fun handleCarry(lane : String) : Int {
        if (lane == BOTTOM) {
            return ADC
        }
        return UNSUPPORTED
    }

    private fun handleSupport(lane: String) : Int {
        if (lane == BOTTOM) {
            return SUP
        }
        return UNSUPPORTED
    }

    private fun handleJungle(lane: String) : Int {
        if (lane == JUNGLE_LANE) {
            return JUNGLE
        }
        return UNSUPPORTED
    }
}