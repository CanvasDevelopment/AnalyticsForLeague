package com.teamunemployment.leaguestats.utils.extensions

import android.view.View
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.PieReadyComparisonResult

/**
 * Created by Josiah Kendall
 */
/**
 * Set the game stage graphs on a view object
 */
fun View.setGraphs(earlyGameData : PieReadyComparisonResult,
                   midGameData : PieReadyComparisonResult,
                   lateGameData : PieReadyComparisonResult) {

//    earlyGameGraph.setDefaultStyle(earlyGameData.centreText)
//    midGameGraph.setDefaultStyle(midGameData.centreText)
//    lateGameGraph.setDefaultStyle(lateGameData.centreText)
//    earlyGameGraph.data = earlyGameData.pieData
//    midGameGraph.data = midGameData.pieData
//    lateGameGraph.data = lateGameData.pieData

}