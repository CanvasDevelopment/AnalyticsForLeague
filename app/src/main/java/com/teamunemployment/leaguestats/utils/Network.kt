package com.teamunemployment.leaguestats.utils

import android.content.Context
import android.net.ConnectivityManager
import com.teamunemployment.leaguestats.BuildConfig
import com.teamunemployment.leaguestats.data.room.AppDatabase

/**
 * @author Josiah Kendall
 */

class Network(private val database: AppDatabase) {

    private val summonerDao = database.summonerDao()
    private val localDebugUrl = "http://192.168.0.100:8080"
    private val productionUrl = "https://leaguestats.appspot.com"
    private val processingProduction ="https://processing-dot-leaguestats.appspot.com/_ah/processing/"

    fun isConnectingToInternet(context : Context) : Boolean {

        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    /**
     * Get the url for the given region. Note that if you are currently running a debug build, it
     * will return
     */
    fun getUrl(): String {
        return "$productionUrl/_ah/api/"
//        if (BuildConfig.DEBUG) {
//
//        }
    }

    fun getProcessingUrl() : String {
        return processingProduction
    }

    fun getRawUrl(region: String) : String {
        if (BuildConfig.DEBUG) {
            return productionUrl
        }
        return productionUrl
    }

    fun getUrl(summonerId : String) : String {
        // Todo make this logic async
        val summoner = summonerDao.loadSummoner(summonerId)
        if (summoner != null) {
            return getUrl()
        }

        return ""
    }
}
