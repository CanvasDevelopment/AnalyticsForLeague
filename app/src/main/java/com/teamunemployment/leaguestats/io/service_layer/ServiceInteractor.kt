package com.teamunemployment.leaguestats.io.service_layer

import android.util.Log
import co.metalab.asyncawait.async
import com.google.gson.Gson
import com.teamunemployment.leaguestats.data.model.Result
import com.teamunemployment.leaguestats.io.service_layer.cache.CacheDao
import com.teamunemployment.leaguestats.io.service_layer.cache.Request
import retrofit2.Call
import java.lang.reflect.Type
import kotlin.coroutines.coroutineContext

/**
 * @author Josiah Kendall
 *
 * The object of this class is to handle all the async stuff in its own small simple class, so everything
 * else can be tested appropriately.
 */
class ServiceInteractor(val cacheDao: CacheDao) {
    val gson = Gson()
    private val tag = "ServiceInteractor"

    /**
     * Save our result to the database under the given url. This simply creates a [Request] object
     * and uses the [CacheDao] to save it
     *
     * @param result the [Result] object to save
     * @param url Used as the key for which to find cached results for this url in the future.
     */
    fun <T>cacheData(result : Result<T>, url : String) {
        val request = Request(url, gson.toJson(result.data))
        async {
            await {cacheDao.createRequest(request)}
        }
    }

    /**
     * Fetch the cached data out of the database. Note Will only call [service.handleCacheResponse] if
     * there is content to display.
     *
     * @param url This is the key to fetch the cached data with
     * @param service the service that we are to return any data we find to.
     */
    fun <T>fetchCachedData(url : String, service: DefaultService<T>, c : Type) : Result<T>? {
        Log.d("CacheTest", "Fetching data")
        val request =   cacheDao.loadRequest(url)
        Log.d("CacheTest", "fetched data")
        if (request != null) {
            val t = createGenericObject<T>(request, c)
            return Result(200, t)

        }
        return null
    }

    /**
     * Broken out into this method to make it more testable
     *
     * Simply creates an object that matches our class generic, based on the type given.
     * The reason we need to pass the type is because we cannot use [T] is because we then need it
     * to be an inline function with a reified variable, which we cannot support here (based on the
     * code that calls it in the default service) therefore we need to pass in the class type.
     *
     * @param request The request object from the database that we are going to turn into our generic
     * obejct
     * @param type The type that we are creating.
     */
    fun <T> createGenericObject(request: Request, type : Type) : T {
        return  gson.fromJson<T>(request.data, type)
    }

    /**
     * Executes the given [call] object in a coroutine, before dispatching a result to the server.
     * Currently error handling is very simplistic, but this can be improved in time.
     *
     * @param call The [Call] object that will execute our request.
     * @param defaultService The service object to project our results onto.
     */
    fun <T>fetchFromServer(call : Call<Result<T>>, defaultService: DefaultService<T>) : Result<T>? {

        val response =  call.execute()
        if (response.isSuccessful) {
            return response.body()!!
        } else {
            defaultService.errorLoadingFromServer()
        }
        return null
    }
}