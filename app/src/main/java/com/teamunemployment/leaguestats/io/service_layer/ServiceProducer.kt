package com.teamunemployment.leaguestats.io.service_layer

import com.google.firebase.analytics.FirebaseAnalytics
import java.lang.reflect.Type

/**
 * Produce [DefaultService]'s. Used as a factory class so that we can inject it.
 */
class ServiceProducer(private val serviceInteractor: ServiceInteractor,
                      private val firebaseAnalytics: FirebaseAnalytics) {

    fun <T>produceService() : DefaultService<T> {
        return DefaultService<T>(serviceInteractor,firebaseAnalytics)
    }
}