package com.teamunemployment.leaguestats.io.service_layer.cache

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * @author Josiah Kendall
 */

@Dao
interface CacheDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createRequest(request: Request)

    @Query("Select * FROM request where urlKey = :url")
    fun loadRequest(url : String) : Request?
}