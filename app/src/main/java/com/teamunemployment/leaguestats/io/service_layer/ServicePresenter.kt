package com.teamunemployment.leaguestats.io.service_layer

import com.teamunemployment.leaguestats.data.model.Result

interface ServicePresenter {

    fun <T> handleDataResponse(result : Result<T>)
}