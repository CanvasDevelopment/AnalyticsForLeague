package com.teamunemployment.leaguestats.io.di

import com.teamunemployment.leaguestats.data.room.AppDatabase
import com.teamunemployment.leaguestats.io.networking.RetrofitFactory
import com.teamunemployment.leaguestats.io.service_layer.ServiceInteractor
import com.teamunemployment.leaguestats.io.service_layer.ServiceProducer
import org.koin.android.module.AndroidModule

/**
 * Created by Josiah Kendall
 */
class IoModule : AndroidModule() {
    override fun context()= applicationContext {
        context(name = "IoModule") {
            provide { RetrofitFactory() }
            provide {get<AppDatabase>().matchHistoryDao()}
            provide{ ServiceInteractor(get<AppDatabase>().cacheDao()) }
            provide{ ServiceProducer(get(), get()) }
        }
    }

}