package com.teamunemployment.leaguestats.io.service_layer.cache

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Request(@PrimaryKey val urlKey : String, val data : String)