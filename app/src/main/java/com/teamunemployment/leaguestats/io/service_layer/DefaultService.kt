package com.teamunemployment.leaguestats.io.service_layer

import android.os.Bundle
import co.metalab.asyncawait.async
import com.google.firebase.analytics.FirebaseAnalytics
import com.teamunemployment.leaguestats.data.model.Result
import retrofit2.Call
import timber.log.Timber
import java.io.IOException
import java.lang.IllegalStateException
import java.lang.reflect.Type


/**
 * @author Josiah Kendall
 *
 * Create a service that we can use to request data from the database and the server.
 *
 * This class handles the requesting of data from both data sources. It will dispatch requests to
 * both the cache and the server, but will only update the server if the cached data is stale.
 *
 * Note when using - you will need to create a new default service for every data type we need to fetch.
 * The reason for this is because we need to store the [cachedData] to check if we have the exact
 * same data in the cache as we do on the server.
 *
 * We could fix this by moving the class interactor to hold the async behaviour, as opposed to using
 * the service interactor to hold it (which we do now).
 */
class DefaultService<T>(val serviceInteractor: ServiceInteractor,
                        val firebaseAnalytics: FirebaseAnalytics) {

    private val tag = "DefaultService"
    private var cachedData : Result<T>? = null
    var url : String = ""
    private lateinit var callback: (result: Result<T>) -> Unit
    private lateinit var call :  Call<Result<T>>
    private val context = this


    fun setTestCallback(callback : (result : Result<T>) -> Unit) {
        this.callback = callback
    }

    fun setTestUrl(url : String) {
        this.url = url
    }

    /**
     * Request data from both the server and the cache. This fetches from the
     * cache and the network simultaneously.
     *
     * The server only displays of there is no cache data or they are different. Then the server is
     * displayed and updated in the cache.
     */
    fun requestData(
            call :  Call<Result<T>>,
            t : Type,
            successCallback : (result : Result<T>) -> Unit,
            failureCallback : (code : Int) -> Unit = {},
            onlyFresh : Boolean = false) {

        this.url = call.request().url().toString()
        this.call = call
        this.callback = successCallback

        async {
            val localCachedData = await { serviceInteractor.fetchCachedData(url, context, t) }
            if (localCachedData != null && !onlyFresh) {
                handleCacheResponse(localCachedData)
            }

            await {
                val serverData = serviceInteractor.fetchFromServer(call, context)
                if (serverData != null) {
                    handleServerResponse(serverData)
                } else {
                    failureCallback(500)
                }
            }

        }

        /**
         * When we error, we want to know if it occurred because of a failed sync from the server.
         * We want to log all server side errors. If it occurs because of an [IOException], we are not
         * as concerned as this is likely environmental issues (poor network connection). We still
         * want to log them as we
        */
        .onError {

            /** We dispatch [IllegalStateException] if we do not receive a 200..300 request from the
             * server. The error code is in the message. */
            if (it is IllegalStateException) {
                val errorCode = it.message
                if (errorCode!= null && errorCode.length < 4) {
                    failureCallback(errorCode.toInt())
                }
            }

            /** Something went wrong with the network connection or the cache request */
            else if (it is IOException) {
                failureCallback(-1)
                Timber.d(it.cause, "Either cache or network request failed")
            }

           // TODO log here.

            Timber.d(it.cause, "Failed to successfully load data from either the server or cache")
        }
    }

    /** Tell the hub about a crash we have caught. **/
    fun logErrorToServer(throwable: Throwable) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "-1")
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "$throwable.")
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
    }

    /**
     * If the response is not ok, we throw an error. Else call the [callback]
     * parameter with the result.
     *
     * @param response The result from our fetch to the server.
     */
    fun handleServerResponse(response: Result<T>) {
        if (cachedData != null && cachedData!!.data == response.data) {
            return
        }

        if (response.resultCode in 200 .. 300) {
            callback(response)
            serviceInteractor.cacheData(response, url)
        } else {
            throw IllegalStateException("Incorrect response code: ${response.resultCode}")
        }

    }



    fun handleCacheResponse(response: Result<T>) {
        callback(response)
        this.cachedData = response
    }

    fun errorLoadingFromServer() {
        if (cachedData == null) {
            // show error
        }
    }
}