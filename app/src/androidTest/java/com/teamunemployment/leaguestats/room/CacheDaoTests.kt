package com.teamunemployment.leaguestats.room

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import com.google.gson.Gson
import com.teamunemployment.leaguestats.data.room.AppDatabase
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Cards.HeadToHeadStat
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchHistoryCardData
import com.teamunemployment.leaguestats.io.service_layer.cache.Request
import org.junit.Assert
import org.junit.Test
import java.util.*

class CacheDaoTests {

    val random = Random()
    val context = InstrumentationRegistry.getTargetContext()
    val database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
    val cacheDao = database.cacheDao()
    val summonerId = random.nextLong().toString()

    @Test
    fun Ensurewecansaveandloadacachefdatausingtheurlasakey() {
        val gson = Gson()
        val url =  "http://processing-dot-leaguestats.appspot.com/_ah/api/v1/getSummoner/this%20is%20atest"
        val matchHistoryData = produceMatchHistoryCardData(1L)
        val request = Request(url,gson.toJson(matchHistoryData))
        cacheDao.createRequest(request)
        val returnedResult = gson.fromJson<MatchHistoryCardData>(cacheDao.loadRequest(url)!!.data, MatchHistoryCardData::class.java)
        Assert.assertEquals(returnedResult, matchHistoryData)
        // fetch form cache
        // check they are the same
    }

    private fun produceMatchHistoryCardData(matchId : Long) : MatchHistoryCardData  {
        return MatchHistoryCardData(
                matchId,random.nextLong(),
                random.nextLong(),
                produceRandomHeadToHeadStat(),
                produceRandomHeadToHeadStat(),
                produceRandomHeadToHeadStat(),
                random.nextBoolean(),
                random.nextInt().toString(),
                summonerId,
                random.nextInt().toString(),
                random.nextLong()
        )
    }


    private fun produceRandomHeadToHeadStat() : HeadToHeadStat {
        return HeadToHeadStat(random.nextFloat(), random.nextFloat())
    }
}