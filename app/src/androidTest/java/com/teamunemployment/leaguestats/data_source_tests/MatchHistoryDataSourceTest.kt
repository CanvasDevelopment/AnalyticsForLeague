package com.teamunemployment.leaguestats.data_source_tests

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import com.teamunemployment.leaguestats.data.room.AppDatabase
import com.teamunemployment.leaguestats.data.source.MatchHistoryDataSource
import com.teamunemployment.leaguestats.base.tabs.MatchHistoryTab.Model.MatchIdentifier
import org.junit.Test
import java.util.*

class MatchHistoryDataSourceTest {

    val context = InstrumentationRegistry.getTargetContext()

    private val database : AppDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
    private val random = Random()
    private val summonerId = random.nextLong().toString()
    private val matchHistoryDao = database.matchHistoryDao()
    private val matchHistoryDataSource = MatchHistoryDataSource(matchHistoryDao)


    @Test
    fun testThatWeCanLoadFromOurDatasourceCorrectly() {
        // save a bunch of champ

        for (i in 1..100) {
            val matchIdentifier = produceMatchIdentifier(i.toLong())
            matchHistoryDao.createMatchHistoryIdentifier(matchIdentifier)
        }

        val matches = matchHistoryDataSource.fetchListOfChamps(summonerId)
        assert(matches.size == 100)

    }

    private fun produceMatchIdentifier(matchId: Long) : MatchIdentifier {
        return MatchIdentifier(matchId,
                random.nextInt().toString(),
                random.nextInt().toString(),
                random.nextInt(),
                random.nextLong(),
                summonerId)
    }
}