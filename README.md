# Overview

LeagueStats is an android application for players of the MOBA League of Legends.
It shows a user how their performance compares to their opponents. 
It shows them areas of the game they excel at and areas that they struggle at compared
to their peers.

It is currently in a beta track on google play, and is under heavy development.

# Designs

Match History:

![alt text](docs/design/MatchHistory.jpg "Match History")

Dashboard:

![alt text](docs/design/Dashboard.jpg "User Dashboard")

